# Auth

Microservice for authentication functionality

## Project

- Spring boot 2.2.4
- Java JDK 13 (currently 13.0.2)

## Dependencies

- Lombok
- spring-boot-starter-security