package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/OauthRefreshToken.java
 */
@Entity
@Table(name="oauth_refresh_token")
@Getter @Setter
public class OauthRefreshToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="token_id")
    private String tokenId;

    @Column(name="token")
    private byte[] token;

    @Column(name="authentication")
    private byte[] authentication;
}
