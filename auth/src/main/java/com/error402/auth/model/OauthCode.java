package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/OauthCode.java
 */
@Entity
@Table(name="oauth_code")
@Getter @Setter
public class OauthCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="code")
    private String code;

    @Column(name="authentication")
    private byte[] authentication;
}
