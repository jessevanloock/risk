package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/OauthAccessToken.java
 */
@Entity
@Table(name="oauth_access_token")
@Getter @Setter
public class OauthAccessToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="token_id")
    private String tokenId;

    @Column(name="token")
    private byte[] token;

    @Column(name="authentication_id")
    private String authenticationId;

    @Column(name="user_name")
    private String userName;

    @Column(name="client_id")
    private String clientId;

    @Column(name="authentication")
    private byte[] authentication;

    @Column(name="refresh_token")
    private String refreshToken;
}
