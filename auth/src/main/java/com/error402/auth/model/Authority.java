package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/Authority.java
 */

@Entity
@Table(name="authority", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}, name="AUTHORITY_UNIQUE_NAME"))
@Getter @Setter
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(length = 20)
    private String name;
}
