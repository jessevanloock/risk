package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/OauthClientToken.java
 */
@Entity
@Table(name="oauth_client_token")
@Getter @Setter
public class OauthClientToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name="token_id")
    private String tokenId;

    @Column(name="token")
    private byte[] token;

    @Column(name="authentication_id")
    private String authenticationId;

    @Column(name="user_name")
    private String userName;

    @Column(name="client_id")
    private String clientId;
}
