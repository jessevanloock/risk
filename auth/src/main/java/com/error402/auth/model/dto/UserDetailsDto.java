package com.error402.auth.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserDetailsDto {

    // Original values for authorization
    private String username;
    private String password;
    private String UUID;
    private String loginType;

    // Values to be updated
    private String newUsername;
    private String newPassword;
}
