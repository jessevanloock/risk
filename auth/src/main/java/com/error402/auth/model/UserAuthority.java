package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/UserAuthority.java
 */
@Entity
@Table(name="user_authority", uniqueConstraints = @UniqueConstraint(columnNames = {"custom_user_details_id", "authority_id"}, name="CUSTOM_USER_DETAILS_AUTHORITY_UNIQUE_CUSTOM_USER_DETAILS_ID_AND_AUTHORITY_ID"))
@Getter @Setter
public class UserAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CUSTOM_USER_DETAILS_ID", foreignKey = @ForeignKey(name = "FK_CUSTOM_USER_DETAILS_AUTHORITY_CUSTOM_USER_DETAILS_ID"))
    private CustomUserDetails customUserDetails;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "AUTHORITY_ID", foreignKey = @ForeignKey(name = "FK_CUSTOM_USER_DETAILS_AUTHORITY_AUTHORITY_ID"))
    private Authority authority;
}
