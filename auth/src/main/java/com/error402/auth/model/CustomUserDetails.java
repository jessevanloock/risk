package com.error402.auth.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.EnumType.STRING;

/**
 * https://github.com/coderSinol/oAuth2/blob/master/src/main/java/com/sinol/oauth/model/User.java
 */
@Entity
@Table(name = "custom_user_details", uniqueConstraints = @UniqueConstraint(columnNames = {"userName"}, name = "CUSTOM_USER_DETAILS_UNIQUE_USERNAME"))
@Getter
@Setter
public class CustomUserDetails implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(length = 50)
    private String username;

    @Column
    private String password;

    @Column
    private String UUID;

    @Column
    @Enumerated(STRING)
    private AccountType accountType;

    @Column(name = "account_non_expired")
    private Boolean accountNonExpired;

    @Column(name = "account_non_locked")
    private Boolean accountNonLocked;

    @Column(name = "credentials_non_expired")
    private Boolean credentialsNonExpired;

    @Column
    private Boolean enabled;

    @OneToMany(mappedBy = "customUserDetails", targetEntity = UserAuthority.class, cascade = {
            CascadeType.ALL}, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<UserAuthority> userAuthorities = new HashSet<>();

    public CustomUserDetails() {
    }

    public CustomUserDetails(AccountType accountType, String userName, String password) {
        this.accountType = accountType;
        this.username = userName;
        this.password = password;
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true; // Email validation should set this to true
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }


    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public enum AccountType {
        GOOGLE, DEFAULT
    }
}
