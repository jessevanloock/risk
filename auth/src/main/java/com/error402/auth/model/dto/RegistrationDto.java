package com.error402.auth.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RegistrationDto {
    private String username;
    private String password;
}
