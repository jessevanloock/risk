package com.error402.auth.dao;

import com.error402.auth.model.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Integer> {
}
