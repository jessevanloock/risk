package com.error402.auth.dao;

import com.error402.auth.model.CustomUserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<CustomUserDetails, Integer> {

    @Query("SELECT DISTINCT u FROM CustomUserDetails u WHERE u.username = :username")
    CustomUserDetails findByUsername(@Param("username") String username);


    @Query("SELECT DISTINCT u FROM CustomUserDetails u WHERE u.UUID = :UUID")
    CustomUserDetails findByUUID(@Param("UUID") String UUID);
}
