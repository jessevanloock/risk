package com.error402.auth.dao;

import com.error402.auth.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
    Authority findDistinctByName(String name);
}
