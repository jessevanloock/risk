package com.error402.auth.controller;

import com.error402.auth.config.ProfileConfiguration;
import com.error402.auth.exceptions.UUIDNotFoundException;
import com.error402.auth.exceptions.ValidationException;
import com.error402.auth.model.dto.UserDetailsDto;
import com.error402.auth.service.AuthUserDetailsService;
import com.error402.auth.util.AuthorizationHelper;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@CrossOrigin(origins = {"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com", "http://localhost:8080"})
@RestController
@RequestMapping("user")
public class ApiController {

    @Autowired
    private ProfileConfiguration profileConfiguration;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private AuthUserDetailsService userService;

    @Autowired
    private DefaultTokenServices tokenServices;

    @PostMapping
    @RequestMapping("create-new-user")
    public ResponseEntity createNewUser(@RequestBody UserDetailsDto dto)
            throws ValidationException {
        userService.createUser(dto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PostMapping
    @RequestMapping("logout")
    public ResponseEntity logout(@RequestHeader("Authorization") String accessToken) {
        tokenServices.revokeToken(accessToken.substring(7));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    @RequestMapping("google")
    public Object google(@RequestParam("idtoken") String idTokenString, HttpServletRequest request) throws GeneralSecurityException, IOException, ValidationException {

        if (request.getHeaders("X-Requested-With") == null) {
            throw new RuntimeException("Header does not contain X-Requested-With");
        }

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory()).setAudience(Collections.singletonList(profileConfiguration.getGoogle())).build();
        GoogleIdToken idToken = verifier.verify(idTokenString);
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();


            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);


            // Get profile information from payload
            String email = payload.getEmail();
            boolean emailVerified = payload.getEmailVerified();
            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
            String locale = (String) payload.get("locale");
            String familyName = (String) payload.get("family_name");
            String givenName = (String) payload.get("given_name");

            UserDetailsDto dto = new UserDetailsDto();
            dto.setLoginType("Google");
            dto.setUUID(userId);
            dto.setPassword(AuthorizationHelper.randomString());
            dto.setUsername(givenName);


            UserDetails userDetails = null;
            try {
                userDetails = userService.loadUserByUUID(dto.getUUID());
            } catch (UUIDNotFoundException e) {
                userDetails = userService.createGoogleUser(dto);
                //Return access token for new account.
            }


            OAuth2Request oauthRequest = AuthorizationHelper.extractAuthorizationRequest(userDetails);

            return tokenServices.createAccessToken(new OAuth2Authentication(oauthRequest, new UsernamePasswordAuthenticationToken(dto.getUsername(), userDetails.getPassword())));


            // Else we create the user and return the access token.

        } else {
            System.out.println("Invalid ID token.");

            return "Invalid Token";
        }
    }


    @PutMapping()
    public ResponseEntity updateUser(@RequestHeader("Authorization") String token, @RequestBody UserDetailsDto dto)
            throws ValidationException {
        validateRequest(token, dto.getUsername());
        userService.updateUser(dto);
        tokenServices.revokeToken(token.substring(7)); // Prevent this access token to be refreshed (only the refresh token gets revoked, access token has to expire)
        return new ResponseEntity<>("CustomUserDetails updated", HttpStatus.OK);
    }


    private void validateRequest(String token, String username)
            throws ValidationException {

        if (username != null) {
            try {
                OAuth2AccessToken accessToken = tokenServices.readAccessToken(token.substring(7));
                if (!username.equals(accessToken.getAdditionalInformation().get("user_name").toString()))
                    throw new ValidationException("User is not allowed to do this");
                if (accessToken.isExpired()) {
                    throw new ValidationException("Token has expired");
                }
            } catch (InvalidTokenException ex) {
                throw new ValidationException("Token is invalid");
            }
        } else throw new ValidationException("Username is missing");
    }
}
