package com.error402.auth.service;

import com.error402.auth.dao.AuthorityRepository;
import com.error402.auth.dao.UserAuthorityRepository;
import com.error402.auth.dao.UserRepository;
import com.error402.auth.exceptions.UUIDNotFoundException;
import com.error402.auth.exceptions.ValidationException;
import com.error402.auth.model.CustomUserDetails;
import com.error402.auth.model.UserAuthority;
import com.error402.auth.model.dto.CustomGrantedAuthority;
import com.error402.auth.model.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class AuthUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserAuthorityRepository userAuthorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public com.error402.auth.model.dto.CustomUserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        CustomUserDetails customUserDetails = userRepository.findByUsername(username);
        if (customUserDetails == null) throw new UsernameNotFoundException(username);

        com.error402.auth.model.dto.CustomUserDetails customUserDetailsDetails = new com.error402.auth.model.dto.CustomUserDetails();
        customUserDetailsDetails.setUsername(customUserDetails.getUsername());
        customUserDetailsDetails.setPassword(customUserDetails.getPassword());
        customUserDetailsDetails.setAccountExpired(customUserDetails.getAccountNonExpired());
        customUserDetailsDetails.setAccountLocked(customUserDetails.getAccountNonLocked());
        customUserDetailsDetails.setCredentialsExpired(customUserDetails.getCredentialsNonExpired());
        customUserDetailsDetails.setEnabled(customUserDetails.getEnabled());
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (UserAuthority authority : customUserDetails.getUserAuthorities()) {
            authorities.add(new CustomGrantedAuthority(authority.getAuthority().getName()));
        }
        customUserDetailsDetails.setGrantedAuthorities(authorities);
        return customUserDetailsDetails;
    }

    public com.error402.auth.model.dto.CustomUserDetails loadUserByUUID(String UUID) throws UUIDNotFoundException {


        CustomUserDetails customUserDetails = userRepository.findByUUID(UUID);
        if (customUserDetails == null) throw new UUIDNotFoundException(UUID);

        com.error402.auth.model.dto.CustomUserDetails customUserDetailsDetails = new com.error402.auth.model.dto.CustomUserDetails();
        customUserDetailsDetails.setUsername(customUserDetails.getUsername());
        customUserDetailsDetails.setPassword(customUserDetails.getPassword());
        customUserDetailsDetails.setAccountExpired(customUserDetails.getAccountNonExpired());
        customUserDetailsDetails.setAccountLocked(customUserDetails.getAccountNonLocked());
        customUserDetailsDetails.setCredentialsExpired(customUserDetails.getCredentialsNonExpired());
        customUserDetailsDetails.setEnabled(customUserDetails.getEnabled());
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (UserAuthority authority : customUserDetails.getUserAuthorities()) {
            authorities.add(new CustomGrantedAuthority(authority.getAuthority().getName()));
        }
        customUserDetailsDetails.setGrantedAuthorities(authorities);
        return customUserDetailsDetails;

    }

    public UserDetails createUser(UserDetailsDto dto)
            throws ValidationException {
        validateUserDetailsForCreate(dto);
        CustomUserDetails customUserDetails = new CustomUserDetails(CustomUserDetails.AccountType.DEFAULT,dto.getUsername(), passwordEncoder.encode(dto.getPassword()));
        return userRepository.save(customUserDetails);
    }


    public UserDetails createGoogleUser(UserDetailsDto dto) {
        CustomUserDetails customUserDetails = new CustomUserDetails(CustomUserDetails.AccountType.GOOGLE,dto.getUsername(),passwordEncoder.encode(dto.getPassword()));
        customUserDetails.setUUID(dto.getUUID());
        return userRepository.save(customUserDetails);
    }

    public UserDetails updateUser(UserDetailsDto dto)
            throws ValidationException {

        CustomUserDetails customUserDetails = userRepository.findByUsername(dto.getUsername());
        if (isUsernameValid(dto.getNewUsername())) customUserDetails.setUsername(dto.getNewUsername());
        if (isPasswordValid(dto.getNewPassword())) customUserDetails.setPassword(passwordEncoder.encode(dto.getNewPassword()));
        // TODO: Repeat these statements for all future properties

        return userRepository.save(customUserDetails);
    }

    public void validateUserDetailsForCreate(UserDetailsDto dto)
            throws ValidationException {

        if (!isUsernameValid(dto.getUsername())) throw new ValidationException("Username is not valid");
        if (!isPasswordValid(dto.getPassword())) throw new ValidationException("Password is not valid");
        // TODO: add more checks if there are more properties to set

        try {
            loadUserByUsername(dto.getUsername());
            throw new ValidationException("Username is already in use");
        } catch (UsernameNotFoundException ignored) {}
    }

    public boolean isUsernameValid(String username) {
        return !(username == null || username.isEmpty() || username.length() < 3 || username.length() > 200); // TODO: add regex match
    }

    public boolean isPasswordValid(String password) {
        return !(password == null || password.isEmpty() || password.length() < 3 || password.length() > 25); // TODO: add regex match
    }

}
