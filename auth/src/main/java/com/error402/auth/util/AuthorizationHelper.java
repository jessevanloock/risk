package com.error402.auth.util;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Request;

import java.util.HashSet;
import java.util.Set;

public class AuthorizationHelper {


    public static String randomString() {
        StringBuilder random = new StringBuilder();
        String[] alfabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        String[] symbols = {"$", "%", "_", "-"};

        for (int i = 0; i < 30; i++) {
            if (Math.random() > 0.50) {
                random.append(symbols[(int) (Math.random() * symbols.length)]);
            }
            random.append(alfabet[(int) (Math.random() * alfabet.length)]);
        }
        return random.toString();
    }


    public static OAuth2Request extractAuthorizationRequest(UserDetails userDetails) {
        Set<String> scopes = new HashSet<>();
        scopes.add("read");

        Set<String> resourceIds = new HashSet<>();
        resourceIds.add("game");
        resourceIds.add("chat");

        OAuth2Request request = new OAuth2Request(null, "error402", userDetails.getAuthorities(), true, scopes, resourceIds, null, null, null);

        return request;

    }

}
