package com.error402.auth.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler
    public final ResponseEntity<String> handleException(Exception ex, WebRequest request) {

        if (ex instanceof ValidationException) {
            ValidationException unvex = (ValidationException) ex;
            return new ResponseEntity<>(unvex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof UsernameNotFoundException) {
            UsernameNotFoundException unfex = (UsernameNotFoundException) ex;
            return new ResponseEntity<>(unfex.getMessage(), HttpStatus.NOT_FOUND);
        } else {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
            return new ResponseEntity<>(ex.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
