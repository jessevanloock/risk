package com.error402.auth.exceptions;

public class UUIDNotFoundException extends Exception {
    public UUIDNotFoundException(String uuid) {
        super("No user found with UUID: " + uuid);
    }
}
