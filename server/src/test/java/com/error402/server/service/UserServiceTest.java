package com.error402.server.service;

import com.error402.server.ServerApplication;
import com.error402.server.dao.FriendshipRepository;
import com.error402.server.dao.UserRepository;
import com.error402.server.exceptions.AlreadyFriendsException;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.InvalidFriendshipException;
import com.error402.server.model.FriendRequestType;
import com.error402.server.model.Friendship;
import com.error402.server.model.RiskUser;
import com.error402.server.model.dto.FriendshipDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private FriendshipRepository friendshipRepository;

    //region USER

    @Test
    void getUser_shouldThrowEntityNotFoundException_whenUserNotExists() {
        RiskUser testUser = new RiskUser("user");

        assertThrows(EntityNotFoundException.class, () -> userService.getUser(testUser.getUsername()));
    }

    @Test
    void checkIfUserExists() {
        RiskUser testUser1 = new RiskUser("user1");
        RiskUser testUser2 = new RiskUser("user2");

        when(userRepository.findByUsername(testUser1.getUsername()))
                .thenReturn(Optional.of(testUser1));
        when(userRepository.findByUsername(testUser2.getUsername()))
                .thenReturn(Optional.empty());

        boolean result1 = userService.checkIfUserExists(testUser1.getUsername());
        boolean result2 = userService.checkIfUserExists(testUser2.getUsername());

        assertTrue(result1);
        assertFalse(result2);
    }

    //endregion

    //region FRIENDS

    @Test
    void getFriendship_shouldThrowEntityNotFoundException_whenFriendshipNotExists() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);

        assertThrows(EntityNotFoundException.class,
                () -> userService.getFriendship(testFriendship.getId())
        ).printStackTrace();
    }

    @Test
    void getFriendRequests() throws EntityNotFoundException {
        RiskUser testUser = new RiskUser("me");
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship1 = new Friendship(testUser, testReceiver);
        Friendship testFriendship2 = new Friendship(testSender, testUser);
        testUser.getSentFriendships().add(testFriendship1);
        testUser.getReceivedFriendships().add(testFriendship2);

        when(userRepository.findByUsername(testUser.getUsername()))
                .thenReturn(Optional.of(testUser));

        List<FriendshipDto> friendships = userService.getFriendRequests(testUser.getUsername());

        assertEquals(2, friendships.size());
        assertTrue(friendships.stream().anyMatch(f -> testSender.getUsername().equals(f.getFriendName())));
        assertTrue(friendships.stream().anyMatch(f -> testReceiver.getUsername().equals(f.getFriendName())));
    }

    @Test
    void sendFriendRequest() throws AlreadyFriendsException, EntityNotFoundException {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);

        when(userRepository.findByUsername(testSender.getUsername()))
                .thenReturn(Optional.of(testSender));
        when(userRepository.findByUsername(testReceiver.getUsername()))
                .thenReturn(Optional.of(testReceiver));
        when(friendshipRepository.save(testFriendship))
                .thenReturn(testFriendship);

        FriendshipDto result = userService.sendFriendRequest(testSender.getUsername(), testReceiver.getUsername());

        assertEquals(testFriendship.getReceiver().getUsername(), result.getFriendName());
        assertEquals(FriendRequestType.SENT, result.getRequestType());
        assertFalse(result.isConfirmed());
    }

    @Test
    void sendFriendRequest_shouldThrowAlreadyFriendsException_whenFriendRequestAlreadySent() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);
        testSender.getSentFriendships().add(testFriendship);

        when(userRepository.findByUsername(testSender.getUsername()))
                .thenReturn(Optional.of(testSender));
        when(userRepository.findByUsername(testReceiver.getUsername()))
                .thenReturn(Optional.of(testReceiver));

        assertThrows(AlreadyFriendsException.class,
                () -> userService.sendFriendRequest(testSender.getUsername(), testReceiver.getUsername())
        ).printStackTrace();
    }

    @Test
    void sendFriendRequest_shouldThrowAlreadyFriendsException_whenFriendRequestAlreadyReceived() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);
        testReceiver.getReceivedFriendships().add(testFriendship);

        when(userRepository.findByUsername(testSender.getUsername()))
                .thenReturn(Optional.of(testSender));
        when(userRepository.findByUsername(testReceiver.getUsername()))
                .thenReturn(Optional.of(testReceiver));

        assertThrows(AlreadyFriendsException.class,
                () -> userService.sendFriendRequest(testReceiver.getUsername(), testSender.getUsername())
        ).printStackTrace();
    }

    @Test
    void confirmFriendship() throws InvalidFriendshipException, EntityNotFoundException {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);
        testFriendship.setId(1);

        when(friendshipRepository.findById(testFriendship.getId()))
                .thenReturn(Optional.of(testFriendship));
        when(friendshipRepository.save(testFriendship))
                .thenReturn(testFriendship);

        userService.confirmFriendship(testReceiver.getUsername(), testFriendship.getId());

        assertTrue(testFriendship.isConfirmed());
    }

    @Test
    void confirmFriendship_shouldTrowInvalidFriendshipException_whenNotReceiverOfFriendRequest() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);
        testFriendship.setId(1);

        when(friendshipRepository.findById(testFriendship.getId()))
                .thenReturn(Optional.of(testFriendship));
        when(friendshipRepository.save(testFriendship))
                .thenReturn(testFriendship);

        assertThrows(InvalidFriendshipException.class,
                () -> userService.confirmFriendship(testSender.getUsername(), testFriendship.getId())
        ).printStackTrace();
    }

    @Test
    void deleteFriendship() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);

        when(friendshipRepository.findById(anyLong()))
                .thenReturn(Optional.of(testFriendship));

        assertDoesNotThrow(() -> userService.deleteFriendship(testReceiver.getUsername(), testFriendship.getId()));
    }

    @Test
    void deleteFriendship_shouldThrowInvalidFriendshipException_whenNotSenderOrReceiverOfFriendship() {
        RiskUser testSender = new RiskUser("sender");
        RiskUser testReceiver = new RiskUser("receiver");
        Friendship testFriendship = new Friendship(testSender, testReceiver);

        when(friendshipRepository.findById(anyLong()))
                .thenReturn(Optional.of(testFriendship));

        assertThrows(InvalidFriendshipException.class,
                () -> userService.deleteFriendship("otherUser", testFriendship.getId())
        ).printStackTrace();
    }

    //endregion
}
