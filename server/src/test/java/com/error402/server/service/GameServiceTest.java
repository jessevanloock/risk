
package com.error402.server.service;

import com.error402.server.ServerApplication;
import com.error402.server.dao.*;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.FortifyDto;
import org.apache.tomcat.jni.Poll;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.awt.*;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

//@Disabled
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
class GameServiceTest {

    @Autowired
    GameService service;

    @MockBean
    PlayerRepository playerRepository;

    @MockBean
    TurnRepository turnRepository;

    @MockBean
    PhaseRepository phaseRepository;

    @MockBean
    TerritoryRepository territoryRepository;

    @MockBean
    ContinentRepository continentRepository;

    @MockBean
    ActionRepository actionRepository;

    @MockBean
    SessionService sessionService;

    //region INITIALIZE
    @Disabled
    @Test
    void initialize_success() throws EntityNotFoundException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException {
        Player player = new Player("user1", PlayerType.HUMAN);
        player.setId(1);

        Player enemy = new Player("user2", PlayerType.HUMAN);
        enemy.setId(2);

        Player enemy2 = new Player("user2", PlayerType.HUMAN);
        enemy2.setId(3);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.getPlayers().add(player);
        session.getPlayers().add(enemy);
        session.getPlayers().add(enemy2);
        session.setHost(player.getUsername());
        session.setStatus(Status.LOBBY);

        player.setSession(session);
        enemy.setSession(session);
        enemy2.setSession(session);

        List<Phase> phases = new ArrayList<>();
        Turn turn = new Turn(Timestamp.valueOf(LocalDateTime.now()), player, session, phases);


        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(sessionService.saveSession(session))
                .thenReturn(session);
        when(continentRepository.saveAll(Mockito.anyCollection()))
                .thenAnswer(i->i.getArguments()[0]);
        when(territoryRepository.saveAll(Mockito.anyCollection()))
                .thenAnswer(i->i.getArguments()[0]);
        when(territoryRepository.save(Mockito.any(Territory.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(phaseRepository.save(Mockito.any(Phase.class)))
                .thenAnswer(i->i.getArguments()[0]);

        session = service.initialize(player.getUsername(),session.getId(),"Global Conquest");
        assertTrue(session.getTurns().size()>0);
    }


    @Test
    void saveStartState() throws IOException, NoPlayersInSessionException, ParseException, EntityNotFoundException {
        Player player = new Player("user1", PlayerType.HUMAN);
        player.setId(1);

        Turn turn = new Turn();
        turn.setPlayer(player);
        turn.setId(1);

        Phase phase = new Phase();
        phase.setTurn(turn);
        phase.setId(1);
        phase.setPhaseType(PhaseType.ATTACK);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.getPlayers().add(player);
        session.setMap("Global Conquest");
        session.setStatus(Status.PLAYING);

        Continent continent = new Continent();
        continent.setId(1);

        Territory territory1 = new Territory();
        territory1.setId(1);
        territory1.setName("t1");
        territory1.setTroops(5);
        territory1.setPlayer(player);
        territory1.setContinent(continent);

        continent.getTerritories().add(territory1);

        turn.getPhases().add(phase);
        turn.setSession(session);
        player.getTurns().add(turn);

        player.getTerritories().add(territory1);

        player.setSession(session);

        session.getTurns().add(turn);

        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(sessionService.saveSession(session))
                .thenReturn(session);

        session = service.saveStartState(session.getId());

        assertNotNull(session.getStartState());
    }

    //endregion


    //region ALLOCATE
    @Test
    void allocate_success() throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException {
        Player player = new Player("user1", PlayerType.HUMAN);
        player.setId(1);
        player.setTroops(3);

        Turn turn = new Turn();
        turn.setPlayer(player);
        turn.setId(1);

        Phase phase = new Phase();
        phase.setTurn(turn);
        phase.setId(1);
        phase.setPhaseType(PhaseType.ALLOCATION);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.getPlayers().add(player);

        Territory territory = new Territory();
        territory.setId(1);
        territory.setPlayer(player);

        turn.getPhases().add(phase);
        player.getTurns().add(turn);
        player.getTerritories().add(territory);
        player.setSession(session);
        session.getTurns().add(turn);



        AllocateDto allocateDto = new AllocateDto(session.getId(), territory.getId(), player.getTroops());

        when(sessionService.getPlayerInSession(player.getUsername(), allocateDto.getSessionId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);


        session = service.allocate(player.getUsername(), allocateDto);

        assertTrue(session.getTurns().get(0).getPhases().get(0).getActions().size() != 0);

    }

    @Test
    void allocate_shouldThrowInvalidPhaseException_whenPhaseIsNotOfTypeAllocation() throws PlayerNotInSessionException {
        Player player = new Player();
        Session session = new Session();
        player.setSession(session);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));
        when(sessionService.getPlayerInSession(anyString(),anyLong()))
                .thenReturn(player);
        Turn turn = new Turn();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ATTACK); // wrong type
        List<Phase> phases = new ArrayList<>();
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        AllocateDto dto = new AllocateDto();
        dto.setSessionId(1);

        try {
            service.allocate("", dto);
        } catch (InvalidPhaseException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void allocate_shouldThrowInvalidOperationException_whenPlayerIsNotOwnerOfTerritory() throws PlayerNotInSessionException {
        Player player = new Player();
        player.setSession(new Session());
        player.setTerritories(new ArrayList<>());
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));
        Turn turn = new Turn();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ALLOCATION); // wrong type
        List<Phase> phases = new ArrayList<>();
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        AllocateDto dto = new AllocateDto();
        dto.setSessionId(1);
        dto.setTerritoryId(1);
        when(sessionService.getPlayerInSession(anyString(),anyLong()))
                .thenReturn(player);

        try {
            service.allocate("", dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void allocate_shouldThrowInvalidOperationException_whenPlayerDoesNotHaveEnoughTroops() throws PlayerNotInSessionException {
        Player player = new Player();
        player.setTroops(5);
        player.setSession(new Session());
        Territory territory = new Territory();
        territory.setId(1);
        List<Territory> territories = new ArrayList<>();
        territories.add(territory);
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ALLOCATION); // wrong type
        List<Phase> phases = new ArrayList<>();
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));
        when(sessionService.getPlayerInSession(anyString(),anyLong()))
                .thenReturn(player);

        AllocateDto dto = new AllocateDto();
        dto.setSessionId(1);
        dto.setTerritoryId(1);
        dto.setTroops(10);

        try {
            service.allocate("", dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }
    //endregion

    //region ATTACK

    @Test
    void attack_success() throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException {
        Player player = new Player("user1", PlayerType.HUMAN);
        player.setId(1);

        Player enemy = new Player("user2", PlayerType.HUMAN);
        enemy.setId(2);

        Player enemy2 = new Player("user2", PlayerType.HUMAN);
        enemy2.setId(2);

        Turn turn = new Turn();
        turn.setPlayer(player);
        turn.setId(1);

        Phase phase = new Phase();
        phase.setTurn(turn);
        phase.setId(1);
        phase.setPhaseType(PhaseType.ATTACK);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.getPlayers().add(player);
        session.getPlayers().add(enemy);
        session.getPlayers().add(enemy2);
        session.setStatus(Status.PLAYING);

        Territory territory1 = new Territory();
        territory1.setId(1);
        territory1.setName("t1");
        territory1.setTroops(5);
        territory1.setPlayer(player);

        Territory territory2 = new Territory();
        territory2.setId(2);
        territory2.setName("t2");
        territory2.setTroops(2);
        territory2.setPlayer(enemy);

        Territory territory3 = new Territory();
        territory3.setId(3);
        territory3.setName("t3");
        territory3.setTroops(2);
        territory3.setPlayer(enemy);

        Border border1 = new Border();
        border1.setId(1);
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setId(1);
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        turn.getPhases().add(phase);
        turn.setSession(session);
        player.getTurns().add(turn);
        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        player.getTerritories().add(territory1);
        enemy.getTerritories().add(territory2);
        enemy2.getTerritories().add(territory3);
        player.setSession(session);
        enemy.setSession(session);
        enemy2.setSession(session);
        session.getTurns().add(turn);


        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(territoryRepository.findById(territory2.getId()))
                .thenReturn(Optional.of(territory2));
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(actionRepository.save(Mockito.any(Attack.class)))
                .thenAnswer(i->i.getArguments()[0]);


        AttackDto attackDto = new AttackDto(session.getId(), territory1.getId(), territory2.getId());

        session = service.attack(player.getUsername(), attackDto);

        assertTrue(session.getTurns().get(0).getPhases().size() != 0);
    }


    @Test
    void attack_shouldThrowInvalidOperationException_whenPlayerIsNotOwnerOfAttackingTerritory() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);
        when(sessionService.getPlayerInSession(player.getUsername(),session.getId()))
                .thenReturn(player);

        player.setTerritories(new ArrayList<>());
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));


        AttackDto dto = new AttackDto();
        dto.setSessionId(1);
        dto.setAttackingTerritoryId(1);

        try {
            service.attack(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidPhaseException e) {
            e.printStackTrace();
        } catch (PlayerNotInSessionException e) {
            e.printStackTrace();
        }
    }

    @Test
    void attack_shouldThrowInvalidOperationException_whenPlayerIsOwnerOfDefendingTerritory() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);
        List<Territory> territories = new ArrayList<>();
        Territory t1 = new Territory();
        t1.setId(1);
        territories.add(t1);
        Territory t2 = new Territory();
        t2.setId(2);
        territories.add(t2);
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ALLOCATION);
        phases.add(phase);
        turn.setPhases(phases);
        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        when(territoryRepository.findById(anyLong()))
                .thenReturn(Optional.of(t2));


        AttackDto dto = new AttackDto();
        dto.setSessionId(1);
        dto.setAttackingTerritoryId(1);
        dto.setDefendingTerritoryId(2);

        try {
            service.attack(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void attack_shouldThrowInvalidPhaseException_whenPhaseIsOfInvalidType() throws PlayerNotInSessionException {
        Player player = new Player("User1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);
        when(sessionService.getPlayerInSession(player.getUsername(),session.getId()))
                .thenReturn(player);
        List<Territory> territories = new ArrayList<>();
        Territory t1 = new Territory();
        t1.setId(1);
        t1.setTroops(100);
        territories.add(t1);
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Territory t2 = new Territory();
        t2.setId(2);
        t2.setTroops(3);
        when(territoryRepository.findById(anyLong()))
                .thenReturn(Optional.of(t2));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ALLOCATION);
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        AttackDto dto = new AttackDto();
        dto.setSessionId(1);
        dto.setAttackingTerritoryId(1);
        dto.setDefendingTerritoryId(2);

        try {
            service.attack(player.getUsername(), dto);
        } catch (InvalidPhaseException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }
    //endregion

    //region FORTIFY

    @Test
    void fortify_success() throws PlayerNotInSessionException, EntityNotFoundException, InvalidOperationException, InvalidPhaseException {
        Player player = new Player("user1", PlayerType.HUMAN);
        player.setId(1);

        Turn turn = new Turn();
        turn.setPlayer(player);
        turn.setId(1);

        Phase phase = new Phase();
        phase.setTurn(turn);
        phase.setId(1);
        phase.setPhaseType(PhaseType.FORTIFICATION);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.getPlayers().add(player);
        session.setStatus(Status.PLAYING);

        Territory territory1 = new Territory();
        territory1.setId(1);
        territory1.setName("t1");
        territory1.setTroops(5);
        territory1.setPlayer(player);

        Territory territory2 = new Territory();
        territory2.setId(2);
        territory2.setName("t2");
        territory2.setTroops(2);
        territory2.setPlayer(player);


        Border border1 = new Border();
        border1.setId(1);
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setId(1);
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        turn.getPhases().add(phase);
        turn.setSession(session);
        player.getTurns().add(turn);
        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        player.setSession(session);
        session.getTurns().add(turn);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(actionRepository.save(Mockito.any(Attack.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(territoryRepository.findById(territory1.getId()))
                .thenReturn(Optional.of(territory1));
        when(territoryRepository.findAllByPlayerUsernameAndPlayerSessionId(player.getUsername(),session.getId()))
                .thenReturn(Optional.of(player.getTerritories()));

        FortifyDto fortifyDto = new FortifyDto(session.getId(),territory1.getId(),territory2.getId(),1);

        session = service.fortify(player.getUsername(),fortifyDto);

        assertTrue(session.getTurns().get(0).getPhases().get(0).getActions().size()>0);
    }

    @Test
    void fortify_shouldThrowInvalidPhaseException_whenPhaseIsNotOfFortification() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.ALLOCATION);
        phases.add(phase);
        turn.setPhases(phases);

        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        FortifyDto dto = new FortifyDto();
        dto.setSessionId(1);

        try {
            service.fortify(player.getUsername(), dto);
        } catch (InvalidPhaseException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void fortify_shouldThrowInvalidOperationException_whenPhaseAlreadyHasAnAction() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.FORTIFICATION);
        List<Action> actions = new ArrayList<>();
        actions.add(new Fortification());
        phase.setActions(actions);
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        FortifyDto dto = new FortifyDto();
        dto.setSessionId(1);

        try {
            service.fortify(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void fortify_shouldThrowInvalidOperationException_whenFromTerritoryIsNotOwnedByPlayer() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        List<Territory> territories = new ArrayList<>();
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.FORTIFICATION);
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        FortifyDto dto = new FortifyDto();
        dto.setSessionId(1);
        dto.setFromTerritoryId(1);

        try {
            service.fortify(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void fortify_shouldThrowInvalidOperationException_whenToTerritoryIsNotOwnedByPlayer() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        List<Territory> territories = new ArrayList<>();
        Territory t1 = new Territory();
        t1.setId(1);
        territories.add(t1);
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.FORTIFICATION);
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        FortifyDto dto = new FortifyDto();
        dto.setSessionId(1);
        dto.setFromTerritoryId(1);
        dto.setToTerritoryId(2);

        try {
            service.fortify(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    void fortify_shouldThrowInvalidOperationException_whenFromTerritoryDoesNotHaveEnoughTroops() throws PlayerNotInSessionException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session();
        session.setId(1);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(), session.getId()))
                .thenReturn(player);
        List<Territory> territories = new ArrayList<>();
        Territory t1 = new Territory();
        t1.setId(1);
        t1.setTroops(5);
        territories.add(t1);
        Territory t2 = new Territory();
        t2.setId(2);
        territories.add(t2);
        player.setTerritories(territories);
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.of(player));

        Turn turn = new Turn();
        List<Phase> phases = new ArrayList<>();
        Phase phase = new Phase();
        phase.setPhaseType(PhaseType.FORTIFICATION);
        phases.add(phase);
        turn.setPhases(phases);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(anyLong()))
                .thenReturn(Optional.of(turn));

        FortifyDto dto = new FortifyDto();
        dto.setSessionId(1);
        dto.setFromTerritoryId(1);
        dto.setToTerritoryId(2);
        dto.setTroops(7);

        try {
            service.fortify(player.getUsername(), dto);
        } catch (InvalidOperationException e) {
            System.out.println(e.getMessage());
            assertTrue(true);
        } catch (Exception e) {
            fail();
        }
    }
    //endregion

    @Test
    void nextPhase_whenInAllocation_success() throws PlayerNotInSessionException, EntityNotFoundException, NotPlayerTurnException, InvalidPhaseException, ParseException, IOException {
        Player player = new Player("user1",PlayerType.HUMAN);
        player.setId(1);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.setStatus(Status.PLAYING);
        session.getPlayers().add(player);

        player.setSession(session);

        Turn turn = new Turn();
        turn.setId(1);
        turn.setSession(session);
        turn.setPlayer(player);

        Phase phase = new Phase();
        phase.setId(1);
        phase.setPhaseType(PhaseType.ALLOCATION);
        phase.setTurn(turn);

        session.getTurns().add(turn);
        turn.getPhases().add(phase);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(),session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(phaseRepository.save(Mockito.any(Phase.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);

        session = service.nextPhase(session,player.getUsername());

        assertTrue(session.getTurns().get(0).getPhases().size()>1);

    }

    @Test
    void nextPhase_whenInAttack_success() throws PlayerNotInSessionException, EntityNotFoundException, NotPlayerTurnException, InvalidPhaseException, ParseException, IOException {
        Player player = new Player("user1",PlayerType.HUMAN);
        player.setId(1);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.setStatus(Status.PLAYING);
        session.getPlayers().add(player);

        player.setSession(session);

        Turn turn = new Turn();
        turn.setId(1);
        turn.setSession(session);
        turn.setPlayer(player);

        Phase phase = new Phase();
        phase.setId(1);
        phase.setPhaseType(PhaseType.ATTACK);
        phase.setTurn(turn);

        session.getTurns().add(turn);
        turn.getPhases().add(phase);
        player.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(),session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(phaseRepository.save(Mockito.any(Phase.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);

        session = service.nextPhase(session,player.getUsername());

        assertTrue(session.getTurns().get(0).getPhases().size()>1);

    }

    @Test
    void nextPhase_whenInFortification_success() throws PlayerNotInSessionException, EntityNotFoundException, NotPlayerTurnException, InvalidPhaseException, ParseException, IOException {
        Player player = new Player("user1",PlayerType.HUMAN);
        player.setId(1);
        Player player1 = new Player("user2",PlayerType.HUMAN);
        player1.setId(2);

        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        session.setStatus(Status.PLAYING);
        session.getPlayers().add(player);
        session.getPlayers().add(player1);
        session.setMap("Global Conquest");

        player.setSession(session);

        Turn turn = new Turn();
        turn.setId(1);
        turn.setSession(session);
        turn.setPlayer(player);

        Phase phase = new Phase();
        phase.setId(1);
        phase.setPhaseType(PhaseType.FORTIFICATION);
        phase.setTurn(turn);

        Continent continent = new Continent();
        continent.setId(1);
        continent.setBonus(1);
        continent.setName("ASIA");

        Territory territory1 = new Territory();
        territory1.setId(1);
        territory1.setName("BRAZIL");
        territory1.setTroops(5);
        territory1.setPlayer(player);

        Territory territory2 = new Territory();
        territory2.setId(2);
        territory2.setName("PERU");
        territory2.setTroops(2);
        territory2.setPlayer(player1);
        territory2.setContinent(continent);

        session.getTurns().add(turn);
        continent.getTerritories().add(territory2);
        turn.getPhases().add(phase);
        player.getTerritories().add(territory1);
        player1.getTerritories().add(territory2);
        player.setSession(session);
        player1.setSession(session);

        when(sessionService.getPlayerInSession(player.getUsername(),session.getId()))
                .thenReturn(player);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));
        when(phaseRepository.save(Mockito.any(Phase.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(turnRepository.save(Mockito.any(Turn.class)))
                .thenAnswer(i->i.getArguments()[0]);

        session = service.nextPhase(session,player.getUsername());

        assertTrue(session.getTurns().get(0).getPhases().size()>1);
        assertTrue(session.getTurns().size()>1);

    }

    @Test
    void end_succes(){
        Session session = new Session();
        session.setId(1);
        Player player = new Player("user1",PlayerType.HUMAN);
        player.setSession(session);

        Territory territory = new Territory();
        player.getTerritories().add(territory);
        session.getPlayers().add(player);

        when(sessionService.saveSession(session))
                .thenReturn(session);

        session = service.end(session);

        assertSame(session.getStatus(), Status.FINISHED);
    }

    @Test
    void createPhase_success() throws EntityNotFoundException {
        Player player = new Player("user1",PlayerType.HUMAN);
        Session session = new Session(GameType.HUMAN);
        session.setId(1);
        Turn turn = new Turn();
        turn.setSession(session);
        turn.setPlayer(player);
        session.getTurns().add(turn);

        when(phaseRepository.save(Mockito.any(Phase.class)))
                .thenAnswer(i->i.getArguments()[0]);
        when(sessionService.getSession(session.getId()))
                .thenReturn(session);
        when(turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId()))
                .thenReturn(Optional.of(turn));

        session = service.createPhase(session,PhaseType.ALLOCATION);
        assertNotNull(session.getTurns().get(0).getPhases());
    }
}
