package com.error402.server.service;

import com.error402.server.ServerApplication;
import com.error402.server.ai.montecarlo.MonteCarloTreeSearch;
import com.error402.server.dao.*;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.InvalidOperationException;
import com.error402.server.exceptions.NoPlayersInSessionException;
import com.error402.server.model.*;
import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.FortifyDto;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class CpuServiceTest {

    //region Initialize
    @Autowired
    CpuService cpuService;
    @MockBean
    PlayerRepository playerRepository;
    @MockBean
    SessionRepository sessionRepository;
    @MockBean
    NotifierService notifierService;
    @MockBean
    TurnRepository turnRepository;
    @MockBean
    TerritoryRepository territoryRepository;
    @MockBean
    ContinentRepository continentRepository;

    @Autowired
    MonteCarloTreeSearch mcts;

    @MockBean
    GameService gameService;
    @MockBean
    SessionService sessionService;

    //endregion

    //region SIMULATOR
    @Test
    void createCpuSession() {
        Player player = new Player("MainCpu", PlayerType.HARD);

        Session session = new Session(GameType.CPU);
        session.setStatus(Status.LOBBY);
        session.getPlayers().add(player);
        session.setHost(player.getUsername());
        player.setSession(session);


        when(sessionRepository.findById(session.getId()))
                .thenReturn(Optional.of(session));
        when(playerRepository.findByUsernameAndSessionId(player.getUsername(), session.getId()))
                .thenReturn(Optional.of(player));

        assertEquals(1, session.getPlayers().size());
        assertTrue(session.getPlayers().stream().anyMatch(p -> p.getUsername().equals(player.getUsername())));
        assertEquals(session.getGameType(), GameType.CPU);
        assertEquals(session.getStatus(), Status.LOBBY);
        assertEquals(player.getSession(), session);
    }
    //endregion

    //region EASY

    //region ALLOCATION
    @Test
    void determineAllocationForEasy_success() throws InvalidOperationException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Territory territory1 = new Territory();
        territory1.setId(1);

        player.getTerritories().add(territory1);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);

        AllocateDto allocateDto = cpuService.determineAllocationForEasy(player);

        assertEquals(session.getId(), allocateDto.getSessionId());
        assertEquals(territory1.getId(), allocateDto.getTerritoryId());
        assertTrue(allocateDto.getTroops() > 0);
    }

    @Test
    void determineAllocationForEasy_shouldThrowInvalidOperationException() throws InvalidOperationException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Territory territory1 = new Territory();
        territory1.setId(1);

        player.getTerritories().add(territory1);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(0);

        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineAllocationForEasy(player));
    }

    //endregion

    //region ATTACK
    @Test
    void determineAttackEasy_success() throws NoPlayersInSessionException, InvalidOperationException, EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        player.setId(1);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        enemy.setId(2);

        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory2.setId(2);
        territory3.setId(3);

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory2.setTroops(9);
        territory3.setTroops(4);

        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);


        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);


        when(territoryRepository.findById(border4.getAdjacentId())).thenReturn(Optional.of(territory3));

        when(sessionService.getPlayer(player.getId())).thenReturn(player);


        AttackDto attackDto = cpuService.determineAttackForEasy(player);

        assertEquals(session.getId(), attackDto.getSessionId());
        assertEquals(territory2.getId(), attackDto.getAttackingTerritoryId());
        assertEquals(territory3.getId(), attackDto.getDefendingTerritoryId());
    }

    @Test
    void determineAttackEasy_shouldThrowInvalidOperationException_whenRequirementsToAttackNotMet() throws InvalidOperationException, EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        player.setId(1);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        enemy.setId(2);
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory2.setId(2);
        territory3.setId(3);

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory2.setTroops(2);
        territory3.setTroops(4);

        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);

        when(territoryRepository.findById(border4.getAdjacentId()))
                .thenReturn(Optional.of(territory3));
        when(sessionService.getPlayer(player.getId()))
                .thenReturn(player);

        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineAttackForEasy(player));
    }
    //endregion

    //endregion

    //region MEDIUM

    //region ALLOCATION
    @Test
    void determineAllocationForMedium_success() throws NoPlayersInSessionException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);
        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));
        when(territoryRepository.findByPlayerSessionIdAndId(session.getId(),territory2.getId()))
                .thenReturn(territory2);


        AllocateDto allocateDto = cpuService.determineAllocationForMedium(session, player);

        assertEquals(session.getId(), allocateDto.getSessionId());
        assertEquals(territory2.getId(), allocateDto.getTerritoryId());
        assertTrue(allocateDto.getTroops() > 0);
    }

    //endregion

    //region ATTACK
    @Test
    void determineAttackMedium_success() throws NoPlayersInSessionException, InvalidOperationException, EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        player.setId(1);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        enemy.setId(2);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);
        territory3.setId(3);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(9);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);

        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(territoryRepository.findByPlayerSessionIdAndId(session.getId(),territory2.getId()))
                .thenReturn(territory2);

        when(territoryRepository.findById(border1.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border2.getAdjacentId())).thenReturn(Optional.of(territory1));
        when(territoryRepository.findById(border3.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border4.getAdjacentId())).thenReturn(Optional.of(territory3));


        AttackDto attackDto = cpuService.determineAttackForMedium(session, player);

        assertEquals(session.getId(), attackDto.getSessionId());
        assertEquals(territory2.getId(), attackDto.getAttackingTerritoryId());
        assertEquals(territory3.getId(), attackDto.getDefendingTerritoryId());
    }

    @Test
    void determineAttackMedium_shouldThrowInvalidOperationException_whenRequirementsToAttackNotMet() throws InvalidOperationException, EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        player.setId(1);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        enemy.setId(2);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);
        territory3.setId(3);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);

        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(territoryRepository.findByPlayerSessionIdAndId(session.getId(),territory2.getId()))
                .thenReturn(territory2);

        when(territoryRepository.findById(border1.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border2.getAdjacentId())).thenReturn(Optional.of(territory1));
        when(territoryRepository.findById(border3.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border4.getAdjacentId())).thenReturn(Optional.of(territory3));


        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineAttackForMedium(session, player));
    }

    @Test
    void determineAttackMedium_shouldThrowEntityNotFoundException_whenAttackingTerritoryHasNoEnemyTerritories() throws NoPlayersInSessionException, InvalidOperationException, EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        session.getPlayers().add(player);
        player.setSession(session);
        player.setTroops(3);

        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(territoryRepository.findByPlayerSessionIdAndId(session.getId(),territory2.getId()))
                .thenReturn(territory2);

        when(territoryRepository.findById(border1.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border2.getAdjacentId())).thenReturn(Optional.of(territory1));
        when(territoryRepository.findById(border3.getAdjacentId())).thenReturn(Optional.of(territory2));
        when(territoryRepository.findById(border4.getAdjacentId())).thenReturn(Optional.of(territory3));

        assertThrows(EntityNotFoundException.class,
                () -> cpuService.determineAttackForMedium(session, player));

    }

    //endregion

    //region FORTIFICATION
    @Test
    void determineFortificationForMedium_success() throws EntityNotFoundException, NoPlayersInSessionException, InvalidOperationException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(gameService.canReachTerritoryToFortify(territory1.getId(), territory2.getId()))
                .thenReturn(true);

        FortifyDto fortifyDto = cpuService.determineFortificationForMedium(session, player);

        assertEquals(session.getId(), fortifyDto.getSessionId());
        assertEquals(territory2.getId(), fortifyDto.getToTerritoryId());
        assertEquals(territory1.getId(), fortifyDto.getFromTerritoryId());
        assertEquals(territory1.getTroops() - 1, fortifyDto.getTroops());

    }

    @Test
    void determineFortificationForMedium_shouldThrowInvalidOperationException_whenNoTerritoriesWithoutEnemyBorders() throws EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        Border border5 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory3.getId());

        Border border6 = new Border();
        border1.setTerritory(territory3);
        border1.setAdjacentId(territory1.getId());

        territory1.getBorders().add(border1);
        territory1.getBorders().add(border5);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);
        territory3.getBorders().add(border6);


        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(gameService.canReachTerritoryToFortify(territory1.getId(), territory2.getId()))
                .thenReturn(true);

        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineFortificationForMedium(session, player));

    }

    @Test
    void determineFortificationForMedium_shouldThrowInvalidOperationException_WhenNotEnoughTroops() throws EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);

        Border border1 = new Border();
        border1.setTerritory(territory1);
        border1.setAdjacentId(territory2.getId());

        Border border2 = new Border();
        border2.setTerritory(territory2);
        border2.setAdjacentId(territory1.getId());

        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory1.getBorders().add(border1);
        territory2.getBorders().add(border2);
        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(1);
        territory2.setTroops(2);
        territory3.setTroops(4);


        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(gameService.canReachTerritoryToFortify(territory1.getId(), territory2.getId()))
                .thenReturn(true);

        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineFortificationForMedium(session, player));

    }

    @Test
    void determineFortificationForMedium_shouldThrowInvalidOperationException_WhenNoConnectingTerritories() throws EntityNotFoundException {
        Player player = new Player("player1", PlayerType.MEDIUM);
        Player enemy = new Player("enemy1", PlayerType.EASY);
        Territory territory1 = new Territory();
        Territory territory2 = new Territory();
        Territory territory3 = new Territory();

        territory1.setId(1);
        territory2.setId(2);


        Border border3 = new Border();
        border3.setTerritory(territory3);
        border3.setAdjacentId(territory2.getId());

        Border border4 = new Border();
        border4.setTerritory(territory2);
        border4.setAdjacentId(territory3.getId());

        territory2.getBorders().add(border4);
        territory3.getBorders().add(border3);

        territory1.setPlayer(player);
        territory2.setPlayer(player);
        territory3.setPlayer(enemy);

        territory1.setTroops(5);
        territory2.setTroops(2);
        territory3.setTroops(4);

        player.getTerritories().add(territory1);
        player.getTerritories().add(territory2);
        enemy.getTerritories().add(territory3);

        List<Player> players = new ArrayList<>();

        players.add(player);
        players.add(enemy);

        Session session = new Session();
        session.setId(1);
        when(playerRepository.findAllBySessionId(session.getId()))
                .thenReturn(Optional.of(players));

        when(gameService.canReachTerritoryToFortify(territory1.getId(), territory2.getId()))
                .thenReturn(false);

        assertThrows(InvalidOperationException.class,
                () -> cpuService.determineFortificationForMedium(session, player));

    }


    //endregion

    //endregion


}
