package com.error402.server.service;

import com.error402.server.ServerApplication;
import com.error402.server.dao.PlayerRepository;
import com.error402.server.exceptions.InvalidOperationException;
import com.error402.server.exceptions.NoPlayersInSessionException;
import com.error402.server.exceptions.PlayerNotInSessionException;
import com.error402.server.model.Player;
import com.error402.server.model.Session;
import com.error402.server.model.Status;
import net.bytebuddy.implementation.bytecode.Throw;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SessionServiceTest {

    @Autowired
    SessionService service;

    @MockBean
    PlayerRepository playerRepository;

    private Session createValidSessionLobby(int players) {
        Session session = new Session();
        session.setId(1);
        session.setStatus(Status.LOBBY);

        for (int i = 0; i < players; i++) {
            Player player = new Player();
            player.setId(i+1);
            player.setUsername("user #" + i);
            session.getPlayers().add(player);
        }
        for (Player player: session.getPlayers()) player.setSession(session);

        return session;
    }

    //region SUCCESS PATH
    @Test
    void discard_shouldPass_whenValid() {
        Session session = createValidSessionLobby(1);
        Player host = session.getPlayers().get(0);
        session.setHost(host.getUsername());

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));
        assertAll(() -> service.discard(host.getUsername(), session.getId()));
    }

    @Test
    void leave_shouldPass_whenValid() {
        Session session = createValidSessionLobby(2);
        Player notHost = session.getPlayers().get(1);

        when(playerRepository.findByUsernameAndSessionId(notHost.getUsername(), session.getId()))
                .thenReturn(Optional.of(notHost));
        assertAll(() -> service.leave(notHost.getUsername(), session.getId()));
    }

    @Test
    void kick_shouldPass_whenValid() {
        Session session = createValidSessionLobby(2);
        Player host = session.getPlayers().get(0);
        Player kick = session.getPlayers().get(1);
        session.setHost(host.getUsername());

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));
        when(playerRepository.findByUsernameAndSessionId(kick.getUsername(), session.getId()))
                .thenReturn(Optional.of(kick));

        assertAll(() -> service.kick(host.getUsername(), session.getId(), kick.getUsername()));
    }
    //endregion

    //region DISCARD
    @Test
    void discard_shouldThrowException_whenSessionOrPlayerNotExists() {
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.empty());

        PlayerNotInSessionException exception = assertThrows(
                PlayerNotInSessionException.class,
                () -> service.discard("username", 1)
        );
        assertEquals("Player is not in session.", exception.getMessage(),
                "PlayerNotInSessionException should be thrown when player is not in session or session doesn't exist ");
    }

    @Test
    void discard_shouldThrowException_whenSessionIsNotLobby() {
        Session session = createValidSessionLobby(1);
        session.setStatus(Status.PLAYING);
        Player host = session.getPlayers().get(0);

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.discard(host.getUsername(), session.getId())
        );
        assertEquals("Session is not in lobby.", exception.getMessage(),
                "InvalidOperationException should be thrown because session is not in lobby");
    }

    @Test
    void discard_shouldThrowException_whenPlayerIsNotHost() {
        Session session = createValidSessionLobby(2);
        Player notHost = session.getPlayers().get(1);

        when(playerRepository.findByUsernameAndSessionId(notHost.getUsername(), session.getId()))
                .thenReturn(Optional.of(notHost));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.discard(notHost.getUsername(), session.getId())
        );
        assertEquals("Player is not the host.", exception.getMessage(),
                "InvalidOperationException should be thrown because player is not the host");
    }
    //endregion

    //region LEAVE
    @Test
    void leave_shouldThrowException_whenPlayerIsNotInSession() {
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.empty());

        PlayerNotInSessionException exception = Assertions.assertThrows(
                PlayerNotInSessionException.class,
                () -> service.leave("", 1)
        );
        assertEquals("Player is not in session.", exception.getMessage(),
                "PlayerNotInSessionException should be thrown when player is not found in session");
    }

    @Test
    void leave_shouldThrowException_whenSessionIsNotInLobby() {
        Session session = createValidSessionLobby(2);
        session.setStatus(Status.PLAYING);
        Player notHost = session.getPlayers().get(1);

        when(playerRepository.findByUsernameAndSessionId(notHost.getUsername(), session.getId()))
                .thenReturn(Optional.of(notHost));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.leave(notHost.getUsername(), session.getId())
        );
        assertEquals("Session is not in lobby.", exception.getMessage(),
                "InvalidOperationException should be thrown when session is not in lobby");
    }

    @Test
    void leave_shouldThrowException_whenPlayerIsHost() {
        Session session = createValidSessionLobby(1);
        Player host = session.getPlayers().get(0);
        session.setHost(host.getUsername());

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.leave(host.getUsername(), session.getId())
        );
        assertEquals("Player is the host.", exception.getMessage(),
                "InvalidOperationException should be thrown when player is the host");
    }
    //endregion

    //region KICK
    @Test
    void kick_shouldThrowException_whenPlayerIsNotInSession() {
        when(playerRepository.findByUsernameAndSessionId(anyString(), anyLong()))
                .thenReturn(Optional.empty());

        PlayerNotInSessionException exception = Assertions.assertThrows(
                PlayerNotInSessionException.class,
                () -> service.kick("host", 1, "username")
        );
        assertEquals("Player is not in session.", exception.getMessage(),
                "PlayerNotInSessionException should be thrown when player is not found in session");
    }

    @Test
    void kick_shouldThrowException_whenSessionIsNotInLobby() {
        Session session = createValidSessionLobby(1);
        session.setStatus(Status.PLAYING);
        Player host = session.getPlayers().get(0);

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.kick(host.getUsername(), session.getId(), "unknown")
        );
        assertEquals("Session is not in lobby.", exception.getMessage(),
                "InvalidOperationException should be thrown when session is not in lobby");
    }

    @Test
    void kick_shouldThrowException_whenPlayerIsNotHost() {
        Session session = createValidSessionLobby(2);
        Player host = session.getPlayers().get(0);
        Player kick = session.getPlayers().get(1);

        when(playerRepository.findByUsernameAndSessionId(kick.getUsername(), session.getId()))
                .thenReturn(Optional.of(kick));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.kick(kick.getUsername(), session.getId(), kick.getUsername())
        );
        assertEquals("Player is not the host.", exception.getMessage(),
                "InvalidOperationException should be thrown when player is not the host");
    }

    @Test
    void kick_shouldThrowException_whenKickedPlayerIsNotInSession() {
        Session session = createValidSessionLobby(1);
        Player host = session.getPlayers().get(0);
        session.setHost(host.getUsername());

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));
        when(playerRepository.findByUsernameAndSessionId(null, session.getId()))
                .thenReturn(Optional.empty());

        PlayerNotInSessionException exception = Assertions.assertThrows(
                PlayerNotInSessionException.class,
                () -> service.kick(host.getUsername(), session.getId(), null)
        );
        assertEquals("Player is not in session.", exception.getMessage(),
                "PlayerNotInSessionException should be thrown when player to kick is not in session");
    }

    @Test
    void kick_shouldThrowException_whenPlayerTriesToKickHimself() {
        Session session = createValidSessionLobby(1);
        Player host = session.getPlayers().get(0);
        session.setHost(host.getUsername());

        when(playerRepository.findByUsernameAndSessionId(host.getUsername(), session.getId()))
                .thenReturn(Optional.of(host));

        InvalidOperationException exception = Assertions.assertThrows(
                InvalidOperationException.class,
                () -> service.kick(host.getUsername(), session.getId(), host.getUsername())
        );
        assertEquals("Player cannot kick himself.", exception.getMessage(),
                "InvalidOperationException should be thrown when player tries to kick himself");
    }
    //endregion
}
