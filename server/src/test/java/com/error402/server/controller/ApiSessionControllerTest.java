package com.error402.server.controller;

import com.error402.server.ServerApplication;
import com.error402.server.WithMockOAuth2Client;
import com.error402.server.exceptions.PlayerNotInSessionException;
import com.error402.server.model.Session;
import com.error402.server.service.SessionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
class ApiSessionControllerTest {
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private SessionService sessionServiceMock;

    @LocalServerPort
    private int port;

    private String getSessionApiUrl() {
        return "http://localhost:" + port + "/session";
    }

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @WithMockOAuth2Client(scope = {"read"})
    @Test
    void startNewSessionTest() throws Exception {
        Session testSession = new Session();
        testSession.setId(1);

        when(sessionServiceMock.createSession(anyString()))
                .thenReturn(testSession);

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post(getSessionApiUrl() + "/start-new-session")
                .accept(MediaType.APPLICATION_JSON))
//                .contentType(MediaType.APPLICATION_JSON).content("null"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().contains("1"));
    }

    @WithMockOAuth2Client(scope = "read")
    @Test
    void startNewSessionTest_SessionIsNull() throws Exception {
        when(sessionServiceMock.createSession(anyString()))
                .thenReturn(null);

        mvc.perform(MockMvcRequestBuilders
                .post(getSessionApiUrl() + "/start-new-session")
                .principal(new UsernamePasswordAuthenticationToken("Test", "test123")))
                .andDo(print())
                .andExpect(status().is5xxServerError());
    }

    @WithMockOAuth2Client(scope = {"read"})
    @Test
    void leave() throws Exception {
        Session testSession = new Session();
        testSession.setId(1);

        doNothing().when(sessionServiceMock).leave(anyString(), testSession.getId());

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete(getSessionApiUrl() + "/leave/" + testSession.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertNull(result.getResponse().getContentType());
    }

    @WithMockOAuth2Client(scope = {"read"})
    @Test
    void leave_isBadRequest_whenPlayerNotInSession() throws Exception {
        Session testSession = new Session();
        testSession.setId(1);

        doThrow(PlayerNotInSessionException.class).when(sessionServiceMock).leave(anyString(), testSession.getId());

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete(getSessionApiUrl() + "/leave/" + testSession.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertNull(result.getResponse().getContentType());
    }
}
