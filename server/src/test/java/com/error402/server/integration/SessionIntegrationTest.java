package com.error402.server.integration;

import com.error402.server.ServerApplication;
import com.error402.server.WithMockOAuth2Client;
import com.error402.server.communication.chat.ChatStub;
import com.error402.server.dao.InviteRepository;
import com.error402.server.dao.PlayerRepository;
import com.error402.server.dao.SessionRepository;
import com.error402.server.dao.UserRepository;
import com.error402.server.model.*;
import com.error402.server.model.dto.JoinCpuDto;
import com.error402.server.model.dto.SessionInviteDto;
import com.error402.server.model.dto.SessionLobbyDto;
import com.error402.server.model.dto.gameoverview.ConquestDto;
import com.error402.server.model.dto.gameoverview.FinishedDto;
import com.error402.server.model.dto.gameoverview.InviteDto;
import com.error402.server.model.dto.gameoverview.LobbyDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@Transactional
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class SessionIntegrationTest {

    private MockMvc mvc;

    @MockBean
    private ChatStub chatStub;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private InviteRepository inviteRepository;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    //region CREATE

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void startNewSessionTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);

        when(chatStub.createSessionChat(anyLong(), anyString()))
                .thenReturn(true);

        MvcResult result = mvc.perform(post("/session")
                .header("Authorization", ""))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        long sessionId = Long.parseLong(result.getResponse().getContentAsString());
        Session session = sessionRepository.findById(sessionId).get();

        assertEquals(testUser.getUsername(), session.getHost());
        assertEquals(GameType.HUMAN, session.getGameType());
        assertEquals(Status.LOBBY, session.getStatus());
        assertEquals(testUser, session.getPlayers().get(0).getRiskUser());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void startNewSessionTest_whenChatCreationFails() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);

        when(chatStub.createSessionChat(anyLong(), anyString()))
                .thenReturn(false);

        MvcResult result = mvc.perform(post("/session")
                .header("Authorization", ""))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Something went wrong while trying to create a session chat.", result.getResponse().getContentAsString());
    }

    //endregion

    //region DISCARD

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void discardSessionTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        mvc.perform(delete("/session/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        assertFalse(sessionRepository.findById(testSession.getId()).isPresent());
        assertFalse(playerRepository.findById(testPlayer.getId()).isPresent());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void discardSessionTest_whenPlayerNotInSession() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        playerRepository.save(testPlayer);
        Session testSession = new Session(GameType.HUMAN);
        sessionRepository.save(testSession);

        MvcResult result = mvc.perform(delete("/session/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not in session.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void discardSessionTest_whenSessionNotInLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.PLAYING);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(delete("/session/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Session is not in lobby.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void discardSessionTest_whenPlayerIsNotHost() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(delete("/session/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not the host.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    //endregion

    //region LEAVE

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void leaveSessionTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        mvc.perform(delete("/session/leave/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertFalse(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void leaveSessionTest_whenPlayerNotInSession() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        playerRepository.save(testPlayer);
        Session testSession = new Session(GameType.HUMAN);
        sessionRepository.save(testSession);

        MvcResult result = mvc.perform(delete("/session/leave/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not in session.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void leaveSessionTest_whenSessionNotInLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.PLAYING);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(delete("/session/leave/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Session is not in lobby.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void leaveSessionTest_whenPlayerIsHost() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(delete("/session/leave/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is the host.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    //endregion

    //region KICK

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Player testKickedPlayer = new Player("kicked", PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.getPlayers().add(testKickedPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        testKickedPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);
        playerRepository.save(testKickedPlayer);

        mvc.perform(delete("/session/kick/{sessionId}/{username}", testSession.getId(), testKickedPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isOk());

        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
        assertFalse(playerRepository.findById(testKickedPlayer.getId()).isPresent());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest_whenKickerNotInSession() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Player testKickedPlayer = new Player("kicked", PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testKickedPlayer);
        testSession.setStatus(Status.LOBBY);
        testKickedPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);
        playerRepository.save(testKickedPlayer);

        MvcResult result = mvc.perform(delete("/session/kick/{sessionId}/{username}",
                testSession.getId(), testKickedPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not in session.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
        assertTrue(playerRepository.findById(testKickedPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest_whenSessionNotInLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Player testKickedPlayer = new Player("kicked", PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.getPlayers().add(testKickedPlayer);
        testSession.setStatus(Status.PLAYING);
        testPlayer.setSession(testSession);
        testKickedPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);
        playerRepository.save(testKickedPlayer);

        MvcResult result = mvc.perform(delete("/session/kick/{sessionId}/{username}",
                testSession.getId(), testKickedPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Session is not in lobby.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
        assertTrue(playerRepository.findById(testKickedPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest_whenPlayerIsNotHost() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Player testKickedPlayer = new Player("kicked", PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.getPlayers().add(testKickedPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        testKickedPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);
        playerRepository.save(testKickedPlayer);

        MvcResult result = mvc.perform(delete("/session/kick/{sessionId}/{username}",
                testSession.getId(), testKickedPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not the host.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
        assertTrue(playerRepository.findById(testKickedPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest_whenKickedPlayerNotInSession() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Player testKickedPlayer = new Player("kicked", PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);
        playerRepository.save(testKickedPlayer);

        MvcResult result = mvc.perform(delete("/session/kick/{sessionId}/{username}",
                testSession.getId(), testKickedPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player is not in session.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
        assertTrue(playerRepository.findById(testKickedPlayer.getId()).isPresent());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void kickPlayerFromSessionTest_whenPlayerKicksHimself() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(delete("/session/kick/{sessionId}/{username}",
                testSession.getId(), testPlayer.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Player cannot kick himself.", result.getResponse().getContentAsString());
        assertTrue(userRepository.findByUsername(testUser.getUsername()).isPresent());
        assertTrue(sessionRepository.findById(testSession.getId()).isPresent());
        assertTrue(playerRepository.findById(testPlayer.getId()).isPresent());
    }

    //endregion

    //region LOBBY

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getSessionLobbyTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setHost("host");
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/lobby/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        SessionLobbyDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), SessionLobbyDto.class);

        assertEquals(testSession.getId(), dto.getSessionId());
        assertEquals(testSession.getHost(), dto.getHost());
        assertEquals(testSession.getPlayers().get(0).getUsername(), dto.getPlayers().get(0).getUsername());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getSessionLobbyTest_whenUserNotInSession() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost("host");
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/lobby/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("User is not a part of this session.", result.getResponse().getContentAsString());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getSessionLobbyTest_whenSessionIsNotLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost("host");
        testSession.getPlayers().add(testPlayer);
        testSession.setStatus(Status.PLAYING);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/lobby/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("This lobby does not exist anymore.", result.getResponse().getContentAsString());
    }

    //endregion

    //region COLOR

    @Disabled
    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void setPlayerColorTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.getPlayers().add(testPlayer);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/lobby/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        SessionLobbyDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), SessionLobbyDto.class);

        assertEquals(testSession.getId(), dto.getSessionId());
        assertEquals(testSession.getHost(), dto.getHost());
        assertEquals(testSession.getPlayers().get(0).getUsername(), dto.getPlayers().get(0).getUsername());
    }

    //endregion

    //region OVERVIEW

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getSessionInvitesTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        userRepository.save(testUser);
        userRepository.save(testSender);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testSender, testSession, testUser);
        inviteRepository.save(testInvite);

        MvcResult result = mvc.perform(get("/session/overview/invites"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        InviteDto[] dtos = objectMapper.readValue(result.getResponse().getContentAsString(), InviteDto[].class);

        assertEquals(1, dtos.length);
        assertEquals(testSession.getId(), dtos[0].getSessionId());
        assertEquals(testSender.getUsername(), dtos[0].getUsername());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getLobbiesTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setCreated(new Timestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)));
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        testSession.getPlayers().add(testPlayer);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/overview/lobbies"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        LobbyDto[] dtos = objectMapper.readValue(result.getResponse().getContentAsString(), LobbyDto[].class);

        assertEquals(1, dtos.length);
        assertEquals(testSession.getCreated().getTime(), dtos[0].getCreated());
        assertEquals(testSession.getId(), dtos[0].getSessionId());
        assertEquals(testUser.getUsername(), dtos[0].getHost());
        assertEquals(1, dtos[0].getPlayers());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getPlayingTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.PLAYING);
        testSession.getPlayers().add(testPlayer);
        testPlayer.setSession(testSession);
        Phase testPhase = new Phase();
        testPhase.setPhaseType(PhaseType.ATTACK);
        List<Phase> phases = new ArrayList<>();
        phases.add(testPhase);
        Turn testTurn = new Turn(new Timestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)),
                testPlayer, testSession, phases);
        testSession.getTurns().add(testTurn);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/overview/playing"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        ConquestDto[] dtos = objectMapper.readValue(result.getResponse().getContentAsString(), ConquestDto[].class);

        assertEquals(1, dtos.length);
        assertEquals(testSession.getId(), dtos[0].getSessionId());
        assertEquals(testUser.getUsername(), dtos[0].getHost());
        assertEquals(1, dtos[0].getPlayers());
        assertEquals(testTurn.getStartTime().getTime(), dtos[0].getLastPlayed());
        assertTrue(dtos[0].isYourTurn());
        assertEquals(testPhase.getPhaseType(), dtos[0].getCurrentPhase());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getFinishedTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setWinner(testUser.getUsername());
        testSession.setStatus(Status.FINISHED);
        testSession.getPlayers().add(testPlayer);
        testPlayer.setSession(testSession);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        MvcResult result = mvc.perform(get("/session/overview/finished"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        FinishedDto[] dtos = objectMapper.readValue(result.getResponse().getContentAsString(), FinishedDto[].class);

        assertEquals(1, dtos.length);
        assertEquals(testSession.getId(), dtos[0].getSessionId());
        assertEquals(testSession.getWinner(), dtos[0].getWinner());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getOverviewOfGameTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);
        Player testPlayer = new Player(testUser.getUsername(), PlayerType.HUMAN);
        Session testSession = new Session(GameType.HUMAN);
        testSession.setStartState("{\"state\":\"START_STATE\"}");
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.PLAYING);
        testSession.getPlayers().add(testPlayer);
        testPlayer.setSession(testSession);
        Allocation testAction = new Allocation();
        Phase testPhase = new Phase();
        testPhase.setPhaseType(PhaseType.ATTACK);
        testPhase.getActions().add(testAction);
        List<Phase> phases = new ArrayList<>();
        phases.add(testPhase);
        Turn testTurn = new Turn(new Timestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)),
                testPlayer, testSession, phases);
        testSession.getTurns().add(testTurn);
        sessionRepository.save(testSession);
        playerRepository.save(testPlayer);

        mvc.perform(get("/session/overview/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.log.[0].allocation").exists())
                .andExpect(jsonPath("$.startstate.state").value("START_STATE"));
    }

    //endregion

    //region ACCEPT

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void acceptSessionInviteTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testSender.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testSender, testSession, testUser);
        inviteRepository.save(testInvite);
        testUser.getInvites().add(testInvite);
        userRepository.save(testUser);
        userRepository.save(testSender);

        mvc.perform(post("/session/accept/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        assertFalse(inviteRepository.findAllByReceiver_Username(testUser.getUsername()).isPresent());
        assertEquals(1, testSession.getPlayers().size());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void acceptSessionInviteTest_whenUserNotInvited() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testSender.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        userRepository.save(testUser);
        userRepository.save(testSender);

        MvcResult result = mvc.perform(post("/session/accept/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("User is not invited.", result.getResponse().getContentAsString());
        assertEquals(0, testSession.getPlayers().size());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void acceptSessionInviteTest_whenMaxPlayersReached() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");

        List<Player> testPlayers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            testPlayers.add(new Player());
        }

        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testSender.getUsername());
        testSession.setStatus(Status.LOBBY);
        testSession.setPlayers(testPlayers);
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testSender, testSession, testUser);
        testUser.getInvites().add(testInvite);
        userRepository.save(testUser);
        userRepository.save(testSender);
        inviteRepository.save(testInvite);

        MvcResult result = mvc.perform(post("/session/accept/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Max players reached.", result.getResponse().getContentAsString());
        assertEquals(6, testSession.getPlayers().size());
        assertTrue(inviteRepository.findAllByReceiver_Username(testUser.getUsername()).isPresent());
        assertFalse(testSession.getPlayers()
                .stream()
                .map(Player::getUsername)
                .collect(Collectors.toList())
                .contains(testUser.getUsername()));
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void acceptSessionInviteTest_whenUserHasAlreadyJoined() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testSender.getUsername());
        testSession.setStatus(Status.LOBBY);
        testSession.getPlayers().add(new Player(testUser.getUsername(), PlayerType.HUMAN));
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testSender, testSession, testUser);
        inviteRepository.save(testInvite);
        testUser.getInvites().add(testInvite);
        userRepository.save(testUser);
        userRepository.save(testSender);

        MvcResult result = mvc.perform(post("/session/accept/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("User is already in game.", result.getResponse().getContentAsString());
        assertEquals(1, testSession.getPlayers().size());
        assertEquals(testSession.getPlayers().get(0).getUsername(), testUser.getUsername());

    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void acceptSessionInviteTest_whenSessionNotInLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testSender.getUsername());
        testSession.setStatus(Status.PLAYING);
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testSender, testSession, testUser);
        inviteRepository.save(testInvite);
        testUser.getInvites().add(testInvite);
        userRepository.save(testUser);
        userRepository.save(testSender);

        MvcResult result = mvc.perform(post("/session/accept/{id}", testSession.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertEquals("Game cannot be joined.", result.getResponse().getContentAsString());
        assertEquals(0, testSession.getPlayers().size());
    }

    //endregion

    //region SEND

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendSessionInviteTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        userRepository.save(testUser);
        userRepository.save(testReceiver);

        SessionInviteDto dto = new SessionInviteDto(testSession.getId(), testReceiver.getUsername());

        mvc.perform(post("/session/invite", testSession.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());

        assertTrue(inviteRepository.findAllByReceiver_Username(testReceiver.getUsername()).isPresent());
        assertFalse(userRepository.findByUsername(testReceiver.getUsername()).get().getInvites().isEmpty());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendSessionInviteTest_whenPlayerHasAlreadyJoined() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        testSession.getPlayers().add(new Player(testReceiver.getUsername(), PlayerType.HUMAN));
        sessionRepository.save(testSession);
        userRepository.save(testUser);
        userRepository.save(testReceiver);

        SessionInviteDto dto = new SessionInviteDto(testSession.getId(), testReceiver.getUsername());

        MvcResult result = mvc.perform(post("/session/invite", testSession.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertFalse(inviteRepository.findAllByReceiver_Username(testReceiver.getUsername()).isPresent());
        assertEquals("Player already in session.", result.getResponse().getContentAsString());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendSessionInviteTest_whenPlayerAlreadyInvited() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        Invite testInvite = new Invite(testUser, testSession, testReceiver);
        inviteRepository.save(testInvite);
        testReceiver.getInvites().add(testInvite);
        userRepository.save(testUser);
        userRepository.save(testReceiver);

        SessionInviteDto dto = new SessionInviteDto(testSession.getId(), testReceiver.getUsername());

        MvcResult result = mvc.perform(post("/session/invite", testSession.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        assertTrue(inviteRepository.findAllByReceiver_Username(testReceiver.getUsername()).isPresent());
        assertFalse(userRepository.findByUsername(testReceiver.getUsername()).get().getInvites().isEmpty());
        assertEquals("Player already invited.", result.getResponse().getContentAsString());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getSentSessionInvitesTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        Invite testInvite = new Invite(testUser, testSession, testReceiver);
        inviteRepository.save(testInvite);
        sessionRepository.save(testSession);
        userRepository.save(testUser);
        userRepository.save(testReceiver);

        MvcResult result = mvc.perform(get("/session/sent-invites"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        SessionInviteDto[] dtos = objectMapper.readValue(result.getResponse().getContentAsString(), SessionInviteDto[].class);

        assertEquals(1, dtos.length);
        assertEquals(testSession.getId(), dtos[0].getSessionId());
        assertEquals(testReceiver.getUsername(), dtos[0].getUsername());
    }

    //endregion

    //region CPU

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void cpuJoinSessionTest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        userRepository.save(testUser);

        JoinCpuDto dto = new JoinCpuDto(testSession.getId(), PlayerType.EASY);

        mvc.perform(post("/session/join-session-cpu")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isOk());

        Session session = sessionRepository.findById(testSession.getId()).get();

        assertEquals(1, session.getPlayers().size());
        assertEquals(PlayerType.EASY, session.getPlayers().get(0).getPlayerType());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void cpuJoinSessionTest_whenMaxPlayersReached() throws Exception {
        RiskUser testUser = new RiskUser("testUser");

        List<Player> testPlayers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            testPlayers.add(new Player());
        }

        Session testSession = new Session(GameType.HUMAN);
        testSession.setPlayers(testPlayers);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.LOBBY);
        sessionRepository.save(testSession);
        userRepository.save(testUser);

        JoinCpuDto dto = new JoinCpuDto(testSession.getId(), PlayerType.EASY);

        MvcResult result = mvc.perform(post("/session/join-session-cpu")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        Session session = sessionRepository.findById(testSession.getId()).get();

        assertEquals(6, session.getPlayers().size());
        assertEquals("Max players reached.", result.getResponse().getContentAsString());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void cpuJoinSessionTest_whenSessionIsNotLobby() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        Session testSession = new Session(GameType.HUMAN);
        testSession.setHost(testUser.getUsername());
        testSession.setStatus(Status.PLAYING);
        sessionRepository.save(testSession);
        userRepository.save(testUser);

        JoinCpuDto dto = new JoinCpuDto(testSession.getId(), PlayerType.EASY);

        MvcResult result = mvc.perform(post("/session/join-session-cpu")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        Session session = sessionRepository.findById(testSession.getId()).get();

        assertTrue(session.getPlayers().isEmpty());
        assertEquals("Game cannot be joined.", result.getResponse().getContentAsString());
    }

    //endregion
}
