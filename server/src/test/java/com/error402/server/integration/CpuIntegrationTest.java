package com.error402.server.integration;

import com.error402.server.ServerApplication;
import com.error402.server.ai.montecarlo.MonteCarloTreeSearch;
import com.error402.server.dao.PlayerRepository;
import com.error402.server.dao.SessionRepository;
import com.error402.server.dao.TerritoryRepository;
import com.error402.server.dao.TurnRepository;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.event.CpuTurnEvent;
import com.error402.server.model.dto.JoinCpuDto;
import com.error402.server.service.CpuService;
import com.error402.server.service.GameService;
import com.error402.server.service.NotifierService;
import com.error402.server.service.SessionService;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@Transactional
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class CpuIntegrationTest {

    @Autowired
    CpuService cpuService;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    SessionService sessionService;
    @Autowired
    NotifierService notifierService;
    @Autowired
    TurnRepository turnRepository;
    @Autowired
    TerritoryRepository territoryRepository;
    @Autowired
    GameService gameService;
    @Autowired
    MonteCarloTreeSearch mcts;

    @Test
    void createCpuSession_success() throws EntityNotFoundException, InvalidJoinGameException {
        Session session = cpuService.createCpuSession("player1", PlayerType.HARD);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = sessionRepository.findById(session.getId()).get();

        assertEquals(3, session.getPlayers().size());
        assertTrue(session.getPlayers().stream().allMatch(player -> player.getTerritories().size() == 0));
        assertTrue(session.getPlayers().stream().noneMatch(player -> player.getPlayerType().equals(PlayerType.HUMAN)));
    }

    @Test
    void initializeGame_success() throws EntityNotFoundException, InvalidJoinGameException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException {
        Session session = cpuService.createCpuSession("player1", PlayerType.HARD);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = cpuService.initializeCpuGame("player1", session.getId(), "Global Conquest");

        assertTrue(session.getPlayers().stream().noneMatch(player -> player.getTerritories().size() == 0));
        assertTrue(session.getPlayers().stream()
                .map(Player::getTerritories)
                .allMatch(territories -> territories.stream().noneMatch(territory -> territory.getTroops() == 0)));
        assertTrue(session.getPlayers().stream()
                .map(Player::getTerritories)
                .allMatch(territories -> territories.stream().noneMatch(territory -> territory.getContinent() == null)));
        assertTrue(session.getPlayers().stream()
                .map(Player::getTerritories)
                .allMatch(territories -> territories.stream().noneMatch(territory -> territory.getBorders().size() == 0)));
        assertSame(session.getStatus(), Status.PLAYING);
    }

    @Test
    void initialTurn_success() throws EntityNotFoundException, InvalidJoinGameException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException {
        Session session = cpuService.createCpuSession("player1", PlayerType.HARD);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = cpuService.initializeCpuGame("player1", session.getId(), "Global Conquest");

        session = cpuService.initialTurn(session);

        assertTrue(session.getPlayers().get(0).getTroops() > 2);
        assertEquals(1, session.getTurns().size());
        assertEquals(1, session.getTurns().get(0).getPhases().size());
        assertSame(session.getTurns().get(0).getPhases().get(0).getPhaseType(), PhaseType.ALLOCATION);
    }

    @Test
    void playTurnHard_success() throws InvalidJoinGameException, EntityNotFoundException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException, PlayerNotInSessionException, InvalidPhaseException, NotPlayerTurnException {
        Session session = cpuService.createCpuSession("player1", PlayerType.HARD);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = cpuService.initializeCpuGame("player1", session.getId(), "Global Conquest");

        session = cpuService.initialTurn(session);
        CpuTurnEvent cpuTurnEvent = new CpuTurnEvent(this, session, "player1");

        session = cpuService.playTurn(cpuTurnEvent);

        assertEquals(session.getPlayers().get(1), gameService.getActiveTurn(session).getPlayer());
        assertEquals(3, session.getTurns().get(0).getPhases().size());
        assertTrue(session.getTurns().get(0).getPhases().get(0).getActions().size() > 0);
    }

    @Test
    void playTurnEasy_success() throws InvalidJoinGameException, EntityNotFoundException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException, PlayerNotInSessionException, InvalidPhaseException, NotPlayerTurnException {
        Session session = cpuService.createCpuSession("player1", PlayerType.EASY);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = cpuService.initializeCpuGame("player1", session.getId(), "Global Conquest");

        session = cpuService.initialTurn(session);
        CpuTurnEvent cpuTurnEvent = new CpuTurnEvent(this, session, "player1");

        session = cpuService.playTurn(cpuTurnEvent);

        assertEquals(session.getPlayers().get(1), gameService.getActiveTurn(session).getPlayer());
        assertEquals(3, session.getTurns().get(0).getPhases().size());
        assertTrue(session.getTurns().get(0).getPhases().get(0).getActions().size() > 0);
    }

    @Test
    void playTurnMedium_success() throws InvalidJoinGameException, EntityNotFoundException, NotEnoughPlayersException, NoPlayersInSessionException, InvalidOperationException, ParseException, IOException, PlayerNotInSessionException, InvalidPhaseException, NotPlayerTurnException {
        Session session = cpuService.createCpuSession("player1", PlayerType.MEDIUM);
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.MEDIUM));

        session = cpuService.initializeCpuGame("player1", session.getId(), "Global Conquest");

        session = cpuService.initialTurn(session);
        CpuTurnEvent cpuTurnEvent = new CpuTurnEvent(this, session, "player1");

        session = cpuService.playTurn(cpuTurnEvent);

        assertEquals(session.getPlayers().get(1), gameService.getActiveTurn(session).getPlayer());
        assertEquals(3, session.getTurns().get(0).getPhases().size());
        assertTrue(session.getTurns().get(0).getPhases().get(0).getActions().size() > 0);
    }

}
