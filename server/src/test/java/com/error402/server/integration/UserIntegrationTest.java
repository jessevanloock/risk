package com.error402.server.integration;

import com.error402.server.ServerApplication;
import com.error402.server.WithMockOAuth2Client;
import com.error402.server.dao.FriendshipRepository;
import com.error402.server.dao.UserRepository;
import com.error402.server.model.Friendship;
import com.error402.server.model.RiskUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles("test")
@Transactional
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ServerApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class UserIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendshipRepository friendshipRepository;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void createUser() throws Exception {
        RiskUser testUser = new RiskUser("testUser");

        mvc.perform(post("/user/create-user"))
                .andDo(print())
                .andExpect(status().isCreated());

        Optional<RiskUser> userOptional = userRepository.findByUsername(testUser.getUsername());

        assertEquals(testUser.getUsername(), userOptional.get().getUsername());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void createUser_whenAlreadyExists() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        userRepository.save(testUser);

        mvc.perform(post("/user/create-user"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void getFriendRequests() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testFriend = new RiskUser("testFriend");
        Friendship testSentFriendship = new Friendship(testUser, testFriend);
        Friendship testReceivedFriendship = new Friendship(testFriend, testUser);

        userRepository.save(testFriend);

        testUser.getSentFriendships().add(testSentFriendship);
        testUser.getReceivedFriendships().add(testReceivedFriendship);

        userRepository.save(testUser);

        mvc.perform(get("/user/friends/requests"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].friendName").value(testFriend.getUsername()))
                .andExpect(jsonPath("$[0].requestType").value("SENT"))
                .andExpect(jsonPath("$[1].friendName").value(testFriend.getUsername()))
                .andExpect(jsonPath("$[1].requestType").value("RECEIVED"));
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void confirmFriendRequest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Friendship testFriendship = new Friendship(testSender, testUser);

        userRepository.save(testUser);
        userRepository.save(testSender);
        testFriendship = friendshipRepository.save(testFriendship);

        mvc.perform(put("/user/friends/{id}/confirm", testFriendship.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        Optional<Friendship> friendshipOptional = friendshipRepository.findById(testFriendship.getId());

        assertTrue(friendshipOptional.get().isConfirmed());
    }

    @WithMockOAuth2Client(clientId = "otherUser")
    @Test
    void confirmFriendRequest_whenNotReceiver() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Friendship testFriendship = new Friendship(testSender, testUser);

        userRepository.save(testUser);
        userRepository.save(testSender);
        testFriendship = friendshipRepository.save(testFriendship);

        mvc.perform(put("/user/friends/{id}/confirm", testFriendship.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Optional<Friendship> friendshipOptional = friendshipRepository.findById(testFriendship.getId());

        assertFalse(friendshipOptional.get().isConfirmed());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendFriendRequest() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");

        userRepository.save(testUser);
        userRepository.save(testReceiver);

        mvc.perform(post("/user/friends/send-request")
                .content(testReceiver.getUsername()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.friendName").value(testReceiver.getUsername()))
                .andExpect(jsonPath("$.requestType").value("SENT"))
                .andExpect(jsonPath("$.confirmed").value(false));
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendFriendRequest_whenAlreadySent() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Friendship testFriendship = new Friendship(testUser, testReceiver);

        userRepository.save(testReceiver);

        testUser.getSentFriendships().add(testFriendship);

        userRepository.save(testUser);

        mvc.perform(post("/user/friends/send-request")
                .content(testReceiver.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void sendFriendRequest_whenAlreadyReceived() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Friendship testFriendship = new Friendship(testSender, testUser);

        userRepository.save(testSender);

        testUser.getReceivedFriendships().add(testFriendship);

        userRepository.save(testUser);

        mvc.perform(post("/user/friends/send-request")
                .content(testSender.getUsername()))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void deleteFriendRequest_whenSender() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testReceiver = new RiskUser("testReceiver");
        Friendship testFriendship = new Friendship(testUser, testReceiver);

        userRepository.save(testUser);
        userRepository.save(testReceiver);
        testFriendship = friendshipRepository.save(testFriendship);

        mvc.perform(delete("/user/friends/{id}/delete", testFriendship.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        Optional<Friendship> friendshipOptional = friendshipRepository.findById(testFriendship.getId());

        assertThrows(NoSuchElementException.class, friendshipOptional::get);
    }

    @WithMockOAuth2Client(clientId = "testUser")
    @Test
    void deleteFriendRequest_whenReceiver() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Friendship testFriendship = new Friendship(testSender, testUser);

        userRepository.save(testUser);
        userRepository.save(testSender);
        testFriendship = friendshipRepository.save(testFriendship);

        mvc.perform(delete("/user/friends/{id}/delete", testFriendship.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        Optional<Friendship> friendshipOptional = friendshipRepository.findById(testFriendship.getId());

        assertThrows(NoSuchElementException.class, friendshipOptional::get);
    }

    @WithMockOAuth2Client(clientId = "otherUser")
    @Test
    void deleteFriendRequest_whenNotSenderOrReceiver() throws Exception {
        RiskUser testUser = new RiskUser("testUser");
        RiskUser testSender = new RiskUser("testSender");
        Friendship testFriendship = new Friendship(testSender, testUser);

        userRepository.save(testUser);
        userRepository.save(testSender);
        testFriendship = friendshipRepository.save(testFriendship);

        mvc.perform(delete("/user/friends/{id}/delete", testFriendship.getId()))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Optional<Friendship> friendshipOptional = friendshipRepository.findById(testFriendship.getId());

        assertEquals(testSender, friendshipOptional.get().getSender());
        assertEquals(testUser, friendshipOptional.get().getReceiver());
    }
}
