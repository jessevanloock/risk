package com.error402.server.utils;

import com.error402.server.model.Territory;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Based on the GRAPH algorithm
 */
@Getter @Setter
public class Node {
    private Territory territory;
    private Set<Node> neighbors;

    public Node(Territory territory) {
        this.territory = territory;
        this.neighbors = new HashSet<>();
    }

    public void connect(Node node) {
        if (this != node) { // ignore if tries to connect to self
            this.neighbors.add(node);
            node.neighbors.add(this);
        }
    }

    public static Optional<Node> search(long destinationId, Node start) {
        Set<Node> alreadyVisited = new HashSet<>();
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(start);

        Node currentNode;

        while (!queue.isEmpty()) {
            currentNode = queue.remove();

            if (currentNode.getTerritory().getId() == destinationId)  {
                return Optional.of(currentNode);
            } else {
                alreadyVisited.add(currentNode);
                queue.addAll(currentNode.getNeighbors());
                queue.removeAll(alreadyVisited);
            }
        }

        return Optional.empty();
    }

    @Override
    public boolean equals(Object obj) {
        if (this.equals(obj)) {
            return true;
        } else return this.getTerritory().equals(obj);
    }
}
