package com.error402.server.utils;

import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.NoPlayersInSessionException;
import com.error402.server.model.*;
import com.error402.server.model.dto.TurnDto;
import lombok.AllArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

@AllArgsConstructor
public class Utils {

    private static Random random = new Random();
    private static JSONParser parser = new JSONParser();

    public static List<Integer> rollDice(int troops, int max) {
        List<Integer> diceRolls = new ArrayList<>();

        while (troops > 0 && max > 0) {
            diceRolls.add(getRandom(1, 6));
            troops--;
            max--;
        }

        return diceRolls;
    }

    public static int getRandom(int min, int max) {
        return random.nextInt((max + 1) - min) + min;
    }

    public static JSONObject readMap(String map)
            throws IOException, ParseException {
        Object obj = parser.parse(new InputStreamReader(ResourceUtils.getURL("classpath:map_data.json").openStream()));

        JSONObject jsonObject = (JSONObject) obj;
        jsonObject = (JSONObject) ((JSONArray) jsonObject.get(map)).get(0);

        return jsonObject;
    }

    /**
     * Converts the current state of a session to JSON.
     *
     * @param session Session object
     * @return JSON object
     * @throws NoPlayersInSessionException Thrown when there are no players in the session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    public static JSONObject convertSessionToJson(Session session)
            throws NoPlayersInSessionException, IOException, ParseException, EntityNotFoundException {

        if (session.getPlayers().isEmpty())
            throw new NoPlayersInSessionException("No players were found in this session");                                             // throw exception when no players are found

        JSONObject json = new JSONObject();
        switch (session.getStatus()) {
            case LOBBY:
                json.put("host", session.getHost());
                json.put("players", session.getPlayers());
                break;
            case PLAYING:
            case FINISHED:
                List<Territory> territories = new ArrayList<>();                                                                                // retrieve all territories
                for (Player player : session.getPlayers()) {
                    territories.addAll(player.getTerritories());
                }
                List<Continent> continents = new ArrayList<>();                                                                                 // retrieve all continents distinctly
                for (Territory territory : territories) {
                    if (!continents.contains(territory.getContinent())) continents.add(territory.getContinent());
                }
                Turn turn = session.getTurns().stream()
                        .filter(turn1 -> turn1.getEndTime() == null).findFirst()
                        .orElseThrow(() -> new EntityNotFoundException("No active turn found"));                                                               // retrieve current turn information

                json.put("players", session.getPlayers());                                                                                      // add all players to the state
                json.put("continents", continents.stream().map(Continent::toJSON).toArray());                                                   // add all continents with territories to the state
                json.put("extras", Utils.readMap(session.getMap()).get("extras"));                                                                     // add all extras to the state
                json.put("turn", new TurnDto(
                        turn.getPlayer().getUsername(),
                        turn.getPhases().stream().max(Comparator.comparing(Phase::getId))
                                .orElseThrow(() -> new EntityNotFoundException("Last phase not found"))
                                .getPhaseType(),
                        turn.getPlayer().getTroops()));
                json.put("map", session.getMap());
                if (session.getStatus() == Status.FINISHED) {
                    json.put("finished", session.getWinner());
                }

                break;
        }
        return json;
    }
}
