package com.error402.server.ai.montecarlo;

import com.error402.server.ai.tree.Node;
import com.error402.server.ai.tree.Tree;
import com.error402.server.dao.TerritoryRepository;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.InvalidOperationException;
import com.error402.server.exceptions.InvalidPhaseException;
import com.error402.server.model.PhaseType;
import com.error402.server.model.Session;
import com.error402.server.service.GameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Computes best option for AI using Monte Carlo Tree Search technique.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class MonteCarloTreeSearch {
    private final TerritoryRepository territoryRepository;
    private final GameService gameService;

    /**
     * Finds the best next allocation for the AI.
     *
     * @param session Session in which the game is played
     * @param level   Level factor to calculate end of search
     * @param troops  Troops amount that is used for allocation
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws InvalidPhaseException   Thrown when a phase can't be found.
     */
    public GameState findNextAllocate(Session session, int level, int troops)
            throws EntityNotFoundException, InvalidPhaseException {

        log.info("MCTS tries to find best next allocation.");

        long start = System.currentTimeMillis();
        long end = start + 60 * level;
        Tree tree = new Tree();
        Node rootNode = tree.getRoot();
        rootNode.getState().setGameState(new GameState(session));

        while (System.currentTimeMillis() < end) {
            // Phase 1 - Selection
            Node promisingNode = selectPromisingNode(rootNode);

            // Phase 2 - Expansion
            expandNode(session, promisingNode, PhaseType.ALLOCATION);

            // Phase 3 - Simulation
            Node nodeToExplore = promisingNode;
            if (promisingNode.getChildArray().size() > 0)
                nodeToExplore = promisingNode.getRandomChildNode();

            simulateRandomAllocation(nodeToExplore, troops);

            // Phase 4 - Update
            backPropagation(nodeToExplore);
        }

        Node winnerNode = rootNode.getChildWithMaxScore();
        tree.setRoot(winnerNode);

        return winnerNode.getState().getGameState();
    }

    /**
     * Finds the best next attack for the AI.
     *
     * @param session Session in which the game is played
     * @param level   Level factor to calculate end of search
     * @throws EntityNotFoundException   Thrown when a required entity can't be found.
     * @throws InvalidOperationException Thrown when an operation can't be executed.
     * @throws InvalidPhaseException   Thrown when a phase can't be found.
     */
    public GameState findNextAttack(Session session, int level)
            throws EntityNotFoundException, InvalidOperationException, InvalidPhaseException {

        log.info("MCTS tries to find best next attack.");

        long start = System.currentTimeMillis();
        long end = start + 60 * level;
        Tree tree = new Tree();
        Node rootNode = tree.getRoot();
        rootNode.getState().setGameState(new GameState(session));

        while (System.currentTimeMillis() < end) {
            // Phase 1 - Selection
            Node promisingNode = selectPromisingNode(rootNode);

            // Phase 2 - Expansion
            if (promisingNode.getState().getGameState().hasAttacksLeft())
                expandNode(session, promisingNode, PhaseType.ATTACK);

            // Phase 3 - Simulation
            Node nodeToExplore = promisingNode;
            if (promisingNode.getChildArray().size() > 0)
                nodeToExplore = promisingNode.getRandomChildNode();

            simulateRandomAttack(nodeToExplore);

            // Phase 4 - Update
            backPropagation(nodeToExplore);
        }

        Node winnerNode = rootNode.getChildWithMaxScore();
        tree.setRoot(winnerNode);

        if (rootNode.getState().getGameState().getScoreForPlayer() >= winnerNode.getState().getGameState().getScoreForPlayer())
            throw new InvalidOperationException("MCTS didn't find a next attack.");

        return winnerNode.getState().getGameState();
    }

    /**
     * Finds the best next fortification for the AI.
     *
     * @param session Session in which the game is played
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws InvalidPhaseException   Thrown when a phase can't be found.
     */
    public GameState findNextFortify(Session session)
            throws EntityNotFoundException, InvalidPhaseException {

        log.info("MCTS tries to find next best fortification.");

        Tree tree = new Tree();
        Node rootNode = tree.getRoot();
        rootNode.getState().setGameState(new GameState(session));

        // Phase 2 - Expansion
        expandNode(session, rootNode, PhaseType.FORTIFICATION);

        // Phase 3 - Simulation
        Node winnerNode = rootNode.getRandomChildNode();
        for (Node node : rootNode.getChildArray()) {
            if (node.getState().getGameState().getScoreForPlayer() > winnerNode.getState().getGameState().getScoreForPlayer()) {
                winnerNode = node;
            }
        }

        return winnerNode.getState().getGameState();
    }

    /**
     * Selects the node with the best value of all the discovered nodes.
     *
     * @param rootNode Root node
     * @return Node with the best value
     */
    private Node selectPromisingNode(Node rootNode) {

        Node node = rootNode;
        while (node.getChildArray().size() != 0) {
            node = UCT.findBestNodeWithUCT(node);
        }

        return node;
    }

    /**
     * Expands the current node by getting all the possible next states and adding them to the child array of the node.
     *
     * @param session Session in which the game is played
     * @param node    Node that is expanded
     * @param type    Type of phase
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws InvalidPhaseException   Thrown when a phase can't be found.
     */
    private void expandNode(Session session, Node node, PhaseType type)
            throws EntityNotFoundException, InvalidPhaseException {

        List<State> possibleStates;

        switch (type) {
            case ALLOCATION:
                possibleStates = node.getState().getAllPossibleAllocationStates(session);
                break;
            case ATTACK:
                possibleStates = node.getState().getAllPossibleAttackStates(session, territoryRepository);
                break;
            case FORTIFICATION:
                possibleStates = node.getState().getAllPossibleFortificationStates(session, gameService);
                break;
            default:
                throw new InvalidPhaseException("Could not find phase.");
        }

        possibleStates.forEach(state -> {
            Node newNode = new Node(state);
            newNode.setParent(node);
            node.getChildArray().add(newNode);
        });
    }

    /**
     * Recursively updates the number of visits and the score of the node and keeps doing this for its parents.
     *
     * @param nodeToExplore Node to recursively update
     */
    private void backPropagation(Node nodeToExplore) {

        Node tempNode = nodeToExplore;

        while (tempNode != null) {
            tempNode.getState().incrementVisit();
            tempNode.getState().addScore(tempNode.getState().getGameState().getScoreForPlayer());
            tempNode = tempNode.getParent();
        }
    }

    /**
     * Simulates random allocations for the AI until all troops are allocated.
     *
     * @param node   Node to start from
     * @param troops Troops amount to be allocated
     * @return Updated game state
     */
    private GameState simulateRandomAllocation(Node node, int troops) {
        Node tempNode = new Node(node);
        State tempState = tempNode.getState();
        GameState gameState = tempState.getGameState();

        while (troops > 0) {
            tempState.randomAllocation();
            troops--;

            int score = tempState.getGameState().getScoreForPlayer();
            tempNode.getParent().getState().setWinScore(score);

            gameState = tempState.getGameState();
        }

        return gameState;
    }

    /**
     * Simulates random attacks for the AI until no possible attacks can be done.
     *
     * @param node Node to start from
     * @return Updated game state
     */
    private GameState simulateRandomAttack(Node node) {

        Node tempNode = new Node(node);
        State tempState = tempNode.getState();
        GameState gameState = tempState.getGameState();

        while (tempState.getGameState().hasAttacksLeft()) {
            tempState.randomAttack();

            int score = tempState.getGameState().getScoreForPlayer();

            tempNode.getParent().getState().setWinScore(score);
            gameState = tempState.getGameState();
        }

        return gameState;
    }
}

