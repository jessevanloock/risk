package com.error402.server.ai.tree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Tree that is used for Monte Carlo Tree Search technique.
 *
 * @author  Error 402
 * @version 0.2
 */
@AllArgsConstructor
@Getter
@Setter
public class Tree {
    private Node root;

    /**
     * Constructs a new Tree object with empty root node.
     */
    public Tree() {
        root = new Node();
    }

    /**
     * Adds a child to a node.
     *
     * @param parent Node to which the child is added
     * @param child  Node that is added to a parent node
     */
    public void addChild(Node parent, Node child) {
        parent.getChildArray().add(child);
    }
}
