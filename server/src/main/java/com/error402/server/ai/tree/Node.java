package com.error402.server.ai.tree;

import com.error402.server.ai.montecarlo.State;
import com.error402.server.utils.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * Node that is used by AI to recursively search the best option.
 *
 * @author Error 402
 * @version 0.2
 */
@AllArgsConstructor
@Getter
@Setter
public class Node {
    private State state;
    private Node parent;
    private List<Node> childArray;

    /**
     * Constructs new Node object with empty state and empty child array list.
     */
    public Node() {
        this.state = new State();
        childArray = new ArrayList<>();
    }

    /**
     * Constructs new Node object with empty child array list and initializes state.
     *
     * @param state State that is initialized
     */
    public Node(State state) {
        this.state = state;
        childArray = new ArrayList<>();
    }

    /**
     * Constructs new Node object based on previous Node object.
     *
     * @param node Previous node
     */
    public Node(Node node) {
        this.childArray = new ArrayList<>();
        this.state = new State(node.getState());

        if (node.getParent() != null)
            this.parent = node.getParent();

        List<Node> childArray = node.getChildArray();
        for (Node child : childArray) {
            this.childArray.add(new Node(child));
        }
    }

    /**
     * Retrieves a random child node.
     *
     * @return Random child node
     */
    public Node getRandomChildNode() {
        return this.childArray.get(Utils.getRandom(0, childArray.size() - 1));
    }

    /**
     * Retrieves the child node with the max score.
     *
     * @return Child node with max score
     */
    public Node getChildWithMaxScore() {
        return Collections.max(this.childArray, Comparator.comparing(c -> c.getState().getVisitCount()));
    }
}
