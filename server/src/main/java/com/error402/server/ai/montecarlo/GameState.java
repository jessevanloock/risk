package com.error402.server.ai.montecarlo;

import com.error402.server.model.*;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * State of game used by AI to compute best option.
 *
 * @author Error 402
 * @version 0.2
 */
@Getter
@Setter
public class GameState {
    private Map<String, List<Territory>> gameState;
    private String currentPlayer;
    private String fromTerritory;
    private String toTerritory;
    private int troops;
    private int score;

    /**
     * Constructs a GameState object based on a session.
     *
     * @param session Session on which the game state will be based
     */
    public GameState(Session session) {

        this.gameState = new HashMap<>();
        this.currentPlayer = session.getTurns().get(session.getTurns().size() - 1).getPlayer().getUsername();

        for (Player player : session.getPlayers()) {
            List<Territory> territories = new ArrayList<>();
            for (Territory territory : player.getTerritories()) {
                // Create Territory
                Territory newTerritory = new Territory();
                newTerritory.setTroops(territory.getTroops());
                newTerritory.setName(territory.getName());
                newTerritory.setContinent(territory.getContinent());
                newTerritory.setId(territory.getId());
                newTerritory.setPlayer(player);

                // Create Border
                for (Border border : territory.getBorders()) {
                    Border newBorder = new Border();
                    newBorder.setTerritory(newTerritory);
                    newBorder.setAdjacentId(border.getAdjacentId());
                    newTerritory.getBorders().add(newBorder);
                }

                territories.add(newTerritory);
            }

            this.gameState.put(player.getUsername(), territories);
        }
    }

    /**
     * Does an allocation in the game state.
     *
     * @param playerTerritory Territory that needs to be allocated
     */
    public void allocate(Territory playerTerritory) {

        Territory territory = findTerritoryByName(playerTerritory.getName());

        gameState.get(currentPlayer).get(gameState.get(currentPlayer).indexOf(territory)).setTroops(territory.getTroops() + 1);
        toTerritory = territory.getName();
    }

    /**
     * Does an attack in the game state.
     *
     * @param playerTerritory Territory from which is attacked
     * @param enemyTerritory  Territory that is attacked
     */
    public void attack(Territory playerTerritory, Territory enemyTerritory) {

        Territory newPlayerTerritory = findTerritoryByName(playerTerritory.getName());
        newPlayerTerritory.setTroops(newPlayerTerritory.getTroops() - enemyTerritory.getTroops());
        Territory newEnemyTerritory = findTerritoryByName(enemyTerritory.getName());
        removeTerritory(newEnemyTerritory);
        newEnemyTerritory.setTroops(1);

        this.gameState.get(currentPlayer).add(newEnemyTerritory);
        this.fromTerritory = playerTerritory.getName();
        this.toTerritory = enemyTerritory.getName();
    }

    /**
     * Does a fortification in the game state.
     *
     * @param fromTerritory Territory from which the troops are send
     * @param toTerritory   Territory that is fortified
     * @param i             How many troops are used to fortify
     */
    public void fortify(Territory fromTerritory, Territory toTerritory, int i) {

        fromTerritory = findTerritoryByName(fromTerritory.getName());
        gameState.get(currentPlayer).get(gameState.get(currentPlayer).indexOf(fromTerritory)).setTroops(fromTerritory.getTroops() - i);

        toTerritory = findTerritoryByName(toTerritory.getName());
        gameState.get(currentPlayer).get(gameState.get(currentPlayer).indexOf(toTerritory)).setTroops(toTerritory.getTroops() + i);

        this.fromTerritory = fromTerritory.getName();
        this.toTerritory = toTerritory.getName();
        this.troops = i;
    }

    /**
     * Retrieves the territory from the game state by name.
     *
     * @param name Name of the territory that needs to be retrieved
     */
    public Territory findTerritoryByName(String name) {

        for (Map.Entry<String, List<Territory>> pair : gameState.entrySet()) {
            for (Territory territory : pair.getValue()) {
                if (territory.getName().equals(name)) {
                    return territory;
                }
            }
        }

        return null;
    }

    /**
     * Calculates the score of the current game state and returns it
     */
    public int getScoreForPlayer() {

        // If game has ended and player won then return max score
        if (gameState.size() == 1 && gameState.containsKey(currentPlayer))
            return Integer.MAX_VALUE;

        List<Territory> territories = this.gameState.get(this.currentPlayer);
        int score = territories.size() * 10;

        List<Continent> checkedContinents = new ArrayList<>();
        for (Map.Entry<String, List<Territory>> pair : gameState.entrySet()) {
            for (Territory territory : pair.getValue()) {
                if (!checkedContinents.contains(territory.getContinent())) {
                    checkedContinents.add(territory.getContinent());
                }
            }
        }

        for (Continent continent : checkedContinents) {
            List<Territory> territoriesInMap = new ArrayList<>();
            for (Territory territory : territories) {
                if (territory.getContinent().getName().equals(continent.getName())) {
                    territoriesInMap.add(territory);
                }
            }

            score += (territoriesInMap.size() * 5) / continent.getTerritories().size();

            if (territoriesInMap.size() == continent.getTerritories().size())
                score += continent.getBonus() * 50;

        }

        for (Territory territory : territories) {
            int numberOfEnemyBorders = 0;
            int maxEnemyTroops = 0;
            for (Border border : territory.getBorders()) {
                boolean found = false;
                for (Territory territoryToCheck : territories) {
                    if (territoryToCheck.getId() == border.getAdjacentId()) {
                        found = true;

                        if (territoryToCheck.getTroops() > maxEnemyTroops)
                            maxEnemyTroops = territoryToCheck.getTroops();
                    }
                }

                if (!found)
                    numberOfEnemyBorders++;
            }

            score += (numberOfEnemyBorders + territory.getTroops());
            score += (territory.getTroops() - maxEnemyTroops);
        }

        this.score = score;

        return score;
    }

    /**
     * Removes the territory from the game state.
     *
     * @param territory Territory that needs to be removed.
     */
    private void removeTerritory(Territory territory) {

        this.gameState.get(territory.getPlayer().getUsername()).removeIf(t -> t.getName().equals(territory.getName()));

        if (gameState.get(territory.getPlayer().getUsername()).isEmpty())
            gameState.remove(territory.getPlayer().getUsername());
    }

    /**
     * Checks if there are still attacks left.
     *
     * @return True if there are still attacks left;
     *         False otherwise
     */
    public boolean hasAttacksLeft() {
        return getAllPossibleAttacks().size() > 0;
    }

    /**
     * Retrieves a map of all the possible attacks that can be done with each territory of the AI.
     *
     * @return Map with all possible attacks
     */
    public Map<Territory, List<Territory>> getAllPossibleAttacks() {

        List<Territory> playerTerritories = this.gameState.get(currentPlayer);
        Map<Territory, List<Territory>> possibleAttacks = new HashMap<>();
        for (Territory playerTerritory : playerTerritories) {
            List<Territory> enemyTerritories = new ArrayList<>();
            possibleAttacks.put(playerTerritory, enemyTerritories);

            for (Map.Entry<String, List<Territory>> pair : gameState.entrySet()) {
                if (!pair.getKey().equals(currentPlayer)) {
                    for (int i = 0; i < pair.getValue().size(); i++) {
                        Territory enemyTerritory = pair.getValue().get(i);

                        for (Border border : playerTerritory.getBorders()) {
                            if (enemyTerritory.getId() == border.getAdjacentId() && enemyTerritory.getTroops() < playerTerritory.getTroops())
                                enemyTerritories.add(enemyTerritory);
                        }
                    }
                }
            }

            if (enemyTerritories.isEmpty())
                possibleAttacks.remove(playerTerritory);
        }

        return possibleAttacks;
    }

    /**
     * Does a random allocation in the game state.
     */
    public void randomAllocation() {

        Random r = new Random();
        allocate(gameState.get(currentPlayer).get(r.nextInt(gameState.get(currentPlayer).size())));
    }

    /**
     * Does a random attack in the game state.
     */
    public void randomAttack() {

        Map<Territory, List<Territory>> allPossibleAttacks = getAllPossibleAttacks();
        List<Territory> keysAsArray = new ArrayList<>(allPossibleAttacks.keySet());
        Random r = new Random();

        if (allPossibleAttacks.size() > 0) {
            Territory playerTerritory = keysAsArray.get(r.nextInt(keysAsArray.size()));
            List<Territory> enemyTerritories = allPossibleAttacks.get(playerTerritory);

            Territory enemyTerritory = enemyTerritories.get(r.nextInt(enemyTerritories.size()));

            attack(playerTerritory, enemyTerritory);
        }
    }
}
