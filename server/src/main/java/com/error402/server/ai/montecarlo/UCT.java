package com.error402.server.ai.montecarlo;

import com.error402.server.ai.tree.Node;

import java.util.Collections;
import java.util.Comparator;

/**
 * Calculates best node.
 *
 * @author  Error 402
 * @version 0.2
 */
public class UCT {

    /**
     * Calculates UCT value.
     *
     * @param totalVisit    Total visits
     * @param nodeWinScore  Win score
     * @param nodeVisit     Node visits
     * @return UCT value
     */
    public static double uctValue(int totalVisit, double nodeWinScore, int nodeVisit) {
        if (nodeVisit == 0)
            return Integer.MAX_VALUE;

        return (nodeWinScore / (double) nodeVisit) + 1.41 * Math.sqrt(Math.log(totalVisit) / (double) nodeVisit);
    }

    /**
     * Finds the best node based on UCT value.
     *
     * @param node Root node
     * @return Node with best UCT value
     */
    public static Node findBestNodeWithUCT(Node node) {
        int parentVisit = node.getState().getVisitCount();

        return Collections.max(
                node.getChildArray(),
                Comparator.comparing(c -> uctValue(parentVisit, c.getState().getWinScore(), c.getState().getVisitCount())));
    }
}
