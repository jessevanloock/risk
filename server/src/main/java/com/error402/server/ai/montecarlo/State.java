package com.error402.server.ai.montecarlo;

import com.error402.server.dao.TerritoryRepository;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.model.Border;
import com.error402.server.model.Player;
import com.error402.server.model.Session;
import com.error402.server.model.Territory;
import com.error402.server.service.GameService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles possibilities and state of game to compute best option.
 *
 * @author Error 402
 * @version 0.2
 */
@NoArgsConstructor
@Getter
@Setter
@Component
public class State {
    private GameState gameState;
    private int visitCount;
    private double winScore;

    /**
     * Constructs State object based on previous State object.
     *
     * @param state Previous state
     */
    public State(State state) {
        this.gameState = state.getGameState();
        this.visitCount = state.getVisitCount();
        this.winScore = state.getWinScore();
    }

    /**
     * Constructs a new State object and initializes the GameState property.
     *
     * @param gameState State of game
     */
    public State(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Increments visit count by 1.
     */
    void incrementVisit() {
        this.visitCount++;
    }

    /**
     * Adds given score to win score.
     *
     * @param score Score to be added onto win score
     */
    void addScore(double score) {
        if (this.winScore != Integer.MIN_VALUE)
            this.winScore += score;
    }

    /**
     * Asks GameState property to randomly allocate.
     */
    void randomAllocation() {
        this.gameState.randomAllocation();
    }

    /**
     * Asks GameState property to randomly attack.
     */
    void randomAttack() {
        this.gameState.randomAttack();
    }

    /**
     * Retrieves a list of states of all possible allocations that can be done.
     *
     * @param session Session in which the game is played
     */
    public List<State> getAllPossibleAllocationStates(Session session) {

        List<State> possibleStates = new ArrayList<>();
        Player currentPlayer = session.getTurns().get(session.getTurns().size() - 1).getPlayer();
        List<Territory> playerTerritories = currentPlayer.getTerritories();

        for (Territory playerTerritory : playerTerritories) {
            GameState gameState = new GameState(session);
            gameState.allocate(playerTerritory);
            possibleStates.add(new State(gameState));
        }

        return possibleStates;
    }

    /**
     * Retrieves a list of states of all possible attacks that can be done.
     *
     * @param session             Session in which the game is played
     * @param territoryRepository TerritoryRepository
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<State> getAllPossibleAttackStates(Session session, TerritoryRepository territoryRepository)
            throws EntityNotFoundException {

        List<State> possibleStates = new ArrayList<>();
        Player currentPlayer = session.getTurns().get(session.getTurns().size() - 1).getPlayer();
        List<Territory> playerTerritories = currentPlayer.getTerritories();

        for (Territory playerTerritory : playerTerritories) {
            for (Border border : playerTerritory.getBorders()) {
                Territory enemyTerritory = territoryRepository.findById(border.getAdjacentId())
                        .orElseThrow(() -> new EntityNotFoundException("Could not find territory."));

                if (!enemyTerritory.getPlayer().getUsername().equals(currentPlayer.getUsername()) &&
                        playerTerritory.getTroops() > 1 &&
                        enemyTerritory.getTroops() < playerTerritory.getTroops()) {
                    GameState gameState = new GameState(session);
                    gameState.attack(playerTerritory, enemyTerritory);
                    possibleStates.add(new State(gameState));
                }
            }
        }

        return possibleStates;
    }

    /**
     * Retrieves a list of states of all possible fortifications that can be done.
     *
     * @param session     Session in which the game is played
     * @param gameService GameService
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<State> getAllPossibleFortificationStates(Session session, GameService gameService)
            throws EntityNotFoundException {

        List<State> possibleStates = new ArrayList<>();
        Player currentPlayer = session.getTurns().get(session.getTurns().size() - 1).getPlayer();
        List<Territory> playerTerritories = currentPlayer.getTerritories();

        for (Territory fromTerritory : playerTerritories) {
            for (Territory toTerritory : playerTerritories) {
                if (gameService.canReachTerritoryToFortify(fromTerritory.getId(), toTerritory.getId()) &&
                        fromTerritory.getId() != (toTerritory.getId()) &&
                        fromTerritory.getTroops() > 1) {
                    for (int i = 1; i < fromTerritory.getTroops(); i++) {
                        GameState gameState = new GameState(session);
                        gameState.fortify(fromTerritory, toTerritory, i);
                        possibleStates.add(new State(gameState));
                    }
                }
            }
        }

        return possibleStates;
    }
}
