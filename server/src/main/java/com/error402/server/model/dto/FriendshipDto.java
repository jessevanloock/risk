package com.error402.server.model.dto;

import com.error402.server.model.FriendRequestType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FriendshipDto {
    private long friendshipId;
    private String friendName;
    private boolean confirmed;
    private FriendRequestType requestType;
}

