package com.error402.server.model.data;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to demonstrate global data.
 *
 * @Author:
 */
@Getter
@Setter
public class GlobalData {
    private long conquestsFinished;
    private long conquestsPlaying;
    private long conquestsLobby;
    private long usersInConquestPlaying;
    private List<Highscore> highscores;

    public GlobalData() {
        this.highscores = new ArrayList<>();
    }
}
