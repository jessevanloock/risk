package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ATTACK")
@Getter @Setter
public class Attack extends Action {

    @Column(name = "ATTACKER_USERNAME")
    private String attacker;

    @Column(name = "ATTACKER_TERRITORY")
    private String attackingTerritory;

    @Column(name = "ATTACKER_TROOPS_START")
    private Integer attackingTroopsAtStart;

    @Column(name = "ATTACKER_TROOPS_END")
    private Integer attackingTroopsAtEnd;

    @Column(name = "ATTACKER_SUCCESSFUL")
    private boolean attackSuccessful;

    @Column(name = "DEFENDER_USERNAME")
    private String defender;

    @Column(name = "DEFENDER_TERRITORY")
    private String defendingTerritory;

    @Column(name = "DEFENDER_TROOPS_START")
    private Integer defendingTroopsAtStart;

    @Column(name = "DEFENDER_TROOPS_END")
    private Integer defendingTroopsAtEnd;
}
