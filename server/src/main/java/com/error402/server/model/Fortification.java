package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("FORTIFICATION")
@Getter @Setter
public class Fortification extends Action {

    @Column(name = "PLAYER_NAME")
    private String player;

    @Column(name = "FORTIFYING_TERRITORY")
    private String fortifyingTerritory;

    @Column(name = "FORTIFIED_TERRITORY")
    private String fortifiedTerritory;

    @Column(name = "TROOPS")
    private Integer troops;
}
