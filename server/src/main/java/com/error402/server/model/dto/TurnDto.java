package com.error402.server.model.dto;

import com.error402.server.model.PhaseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TurnDto {
    private String username;
    private PhaseType phase;
    private int placeableTroops;
}
