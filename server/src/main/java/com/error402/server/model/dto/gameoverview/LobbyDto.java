package com.error402.server.model.dto.gameoverview;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LobbyDto {
    private long sessionId;
    private String host;
    private long created;
    private int players;
}
