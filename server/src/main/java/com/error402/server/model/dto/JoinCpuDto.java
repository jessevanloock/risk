package com.error402.server.model.dto;

import com.error402.server.model.PlayerType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bouncycastle.est.jcajce.JsseESTServiceBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JoinCpuDto {
    private long sessionId;
    private PlayerType playerType;
}
