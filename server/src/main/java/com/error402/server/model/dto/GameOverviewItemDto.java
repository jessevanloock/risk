package com.error402.server.model.dto;

import com.error402.server.model.PhaseType;
import com.error402.server.model.Session;
import com.error402.server.model.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter @Setter
@AllArgsConstructor
public class GameOverviewItemDto {
    private Status status;
    private long sessionId;
    private String host;
    private String map;
    private int players;
    private PhaseType phase;
    private String username;
    private Timestamp timestamp;

    public GameOverviewItemDto(Session session) {
        switch(session.getStatus()) {
            case LOBBY:
                this.sessionId = session.getId();
                this.map = session.getMap();
                this.host = session.getHost();
                this.players = session.getPlayers().size();
                this.timestamp = session.getCreated();
                break;
            case PLAYING:
                this.sessionId = session.getId();
                this.map = session.getMap();
                this.host = session.getHost();
                this.players = session.getPlayers().size();
                this.timestamp = session.getTurns().get(session.getTurns().size() - 1).getStartTime();
                this.username = session.getTurns().get(session.getTurns().size() - 1).getPlayer().getUsername();
                this.phase = session.getTurns().get(session.getTurns().size() - 1).getPhases().get(session.getTurns().get(session.getTurns().size() - 1).getPhases().size() - 1).getPhaseType();
        }
    }

}
