package com.error402.server.model.dto;

import com.error402.server.model.Action;
import org.json.simple.JSONObject;

import java.util.List;

public class ReplaySessionDto {
    private JSONObject startState;
    private List<ActionDto> actions;

    public ReplaySessionDto(JSONObject startState, List<ActionDto> actions) {
        this.startState = startState;
        this.actions = actions;
    }
}
