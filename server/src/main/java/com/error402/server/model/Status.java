package com.error402.server.model;

public enum Status {
    LOBBY,
    PLAYING,
    FINISHED,
    INVITED
}
