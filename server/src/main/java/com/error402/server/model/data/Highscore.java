package com.error402.server.model.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Highscore {
    private String username;
    private long conquestsWon;
    private long conquestsPlayed;

    public Highscore(String username) {
        this.username = username;
        this.conquestsPlayed = 0;
        this.conquestsPlayed = 0;
    }

    public Highscore(String username, long conquestsWon, long conquestsPlayed) {
        this.username = username;
        this.conquestsWon = conquestsWon;
        this.conquestsPlayed = conquestsPlayed;
    }
}
