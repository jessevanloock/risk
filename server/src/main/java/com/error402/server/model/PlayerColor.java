package com.error402.server.model;

public enum PlayerColor {
    red,
    blue,
    yellow,
    orange,
    purple,
    green
}
