package com.error402.server.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name="friendship")
@Data
public class Friendship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "sender", foreignKey = @ForeignKey(name = "FK_FRIENDSHIP_SENDER_ID"))
    private RiskUser sender;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "receiver", foreignKey = @ForeignKey(name = "FK_FRIENDSHIP_RECEIVER_ID"))
    private RiskUser receiver;

    @Column(name = "confirmed")
    private boolean confirmed = false;

    public Friendship(RiskUser sender, RiskUser receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }
}
