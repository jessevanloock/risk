package com.error402.server.model;

public enum PhaseType {
    ALLOCATION, ATTACK, FORTIFICATION
}
