package com.error402.server.model.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SessionData {
    private long sessionId;
    private String map;
    private boolean won;
}
