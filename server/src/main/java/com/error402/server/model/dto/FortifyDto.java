package com.error402.server.model.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter
@ToString
public class FortifyDto {
    private long sessionId;
    private long fromTerritoryId;
    private long toTerritoryId;
    private int troops;
}
