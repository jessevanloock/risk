package com.error402.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name="phase")
@Getter @Setter
public class Phase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column
    private PhaseType phaseType;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "turn_id", foreignKey = @ForeignKey(name = "FK_PHASE_TURN_ID"))
    private Turn turn;

    @OneToMany(mappedBy = "phase", targetEntity = Action.class, cascade = {
            CascadeType.ALL }, orphanRemoval = true/*, fetch = FetchType.EAGER*/)
    private List<Action> actions;

    public Phase() {
        actions = new ArrayList<>();
    }
}
