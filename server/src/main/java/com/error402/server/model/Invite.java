package com.error402.server.model;


import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Invite")
public class Invite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "session_id", foreignKey = @ForeignKey(name = "FK_INVITE_SESSION_ID"))
    private Session session;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "sender", foreignKey = @ForeignKey(name = "FK_INVITE_SENDER_ID"))
    private RiskUser sender;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "receiver", foreignKey = @ForeignKey(name = "FK_INVITE_RECEIVER_ID"))
    private RiskUser receiver;

    public Invite(RiskUser sender, Session session, RiskUser receiver) {
        this.session = session;
        this.sender = sender;
        this.receiver = receiver;
    }
}
