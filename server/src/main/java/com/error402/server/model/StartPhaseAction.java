package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("STARTPHASE")
@Getter
@Setter
public class StartPhaseAction extends Action {

    @Column(name = "PLAYER_NAME")
    private String player;

    @Column(name = "PHASE_TYPE")
    private PhaseType phasetype;

}
