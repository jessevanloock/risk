package com.error402.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.json.simple.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="continent")
@Getter @Setter
public class Continent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="bonus")
    private int bonus;

    @JsonIgnore
    @OneToMany(mappedBy = "continent", targetEntity = Territory.class, cascade = {
            CascadeType.ALL }, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Territory> territories;

    public Continent() {
        this.territories = new ArrayList<>();
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        json.put("id", id);
        json.put("name", name);
        json.put("bonus", bonus);
        json.put("territories", territories.stream().map(Territory::toJSON).toArray());

        return json;
    }
}
