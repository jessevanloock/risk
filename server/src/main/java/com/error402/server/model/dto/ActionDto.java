package com.error402.server.model.dto;

import com.error402.server.model.Allocation;
import com.error402.server.model.Attack;
import com.error402.server.model.Fortification;
import com.error402.server.model.StartPhaseAction;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class ActionDto {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Allocation allocation;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Attack attack;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Fortification fortification;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private StartPhaseAction startPhaseAction;

    public ActionDto(Allocation allocation) {
        allocation.setPhase(null);
        this.allocation = allocation;
    }
    public ActionDto(Attack attack) {
        attack.setPhase(null);
        this.attack = attack;
    }
    public ActionDto(Fortification fortification) {
        fortification.setPhase(null);
        this.fortification = fortification;
    }

    public ActionDto(StartPhaseAction startPhaseAction) {
        startPhaseAction.setPhase(null);
        this.startPhaseAction = startPhaseAction;
    }
}
