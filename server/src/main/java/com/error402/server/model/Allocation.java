package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ALLOCATION")
@Getter
@Setter
public class Allocation extends Action {

    @Column(name = "PLAYER_NAME")
    private String player;

    @Column(name = "TROOPS")
    private int troops;

    @Column(name = "TERRITORY_NAME")
    private String territory;
}
