package com.error402.server.model.event;

import com.error402.server.model.Session;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class CpuTurnEvent extends ApplicationEvent {

    private Session session;
    private String username;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public CpuTurnEvent(Object source,Session session,String username) {
        super(source);
        this.session = session;
        this.username = username;
    }
}
