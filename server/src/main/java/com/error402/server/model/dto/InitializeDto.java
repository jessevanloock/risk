package com.error402.server.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InitializeDto {
    private long sessionId;
    private String map;
    /* add game settings here */
}
