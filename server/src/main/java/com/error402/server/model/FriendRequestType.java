package com.error402.server.model;

public enum FriendRequestType {
    SENT,
    RECEIVED
}
