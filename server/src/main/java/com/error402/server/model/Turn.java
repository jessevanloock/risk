package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "turn")
@Getter
@Setter
public class Turn {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "start_time")
    private Timestamp startTime;

    @Column(name = "end_time")
    private Timestamp endTime;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "player_id", foreignKey = @ForeignKey(name = "FK_TURN_PLAYER_ID"))
    private Player player;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "session_id", foreignKey = @ForeignKey(name = "FK_TURN_SESSION_ID"))
    private Session session;

    @OneToMany(mappedBy = "turn", targetEntity = Phase.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Phase> phases;

    public Turn() {
        this.phases = new ArrayList<>();
    }

    public Turn(Timestamp startTime, Player player, Session session, List<Phase> phases) {
        this.startTime = startTime;
        this.player = player;
        this.session = session;
        this.phases = phases;
}
    public Turn(Session session) {
        this.session = session;
        this.phases = new ArrayList<>();
    }

}
