package com.error402.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "risk_user")
@Getter
@Setter
@NoArgsConstructor
public class RiskUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "username")
    private String username;

    @OneToMany(mappedBy = "sender", targetEntity = Friendship.class, cascade = {
            CascadeType.ALL}, orphanRemoval = true)
    private List<Friendship> sentFriendships;

    @OneToMany(mappedBy = "receiver", targetEntity = Friendship.class, cascade = {
            CascadeType.ALL}, orphanRemoval = true)
    private List<Friendship> receivedFriendships;

    @OneToMany(mappedBy = "receiver", targetEntity = Invite.class, cascade = {
            CascadeType.ALL}, orphanRemoval = true)
    private List<Invite> invites;

    @OneToMany(mappedBy = "riskUser", targetEntity = Player.class, cascade = {
            CascadeType.ALL}, orphanRemoval = true)
    private List<Player> players;

    public RiskUser(String username) {
        this.username = username;
        this.sentFriendships = new ArrayList<>();
        this.receivedFriendships = new ArrayList<>();
        this.invites = new ArrayList<>();
        this.players = new ArrayList<>();
    }

}
