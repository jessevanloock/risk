package com.error402.server.model.dto.gameoverview;

import com.error402.server.model.PhaseType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConquestDto {
    private long sessionId;
    private String host;
    private String map;
    private int players;
    private long lastPlayed;
    private boolean isYourTurn;
    private PhaseType currentPhase;
}
