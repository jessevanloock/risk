package com.error402.server.model;

public enum PlayerAIName {
    JONAS,
    JEROEN,
    NIELS,
    JESSÉ,
    GLENN,
    DAAN
}
