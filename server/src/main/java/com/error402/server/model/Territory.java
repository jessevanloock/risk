package com.error402.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.json.simple.JSONObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="territory")
@Getter @Setter
public class Territory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @JsonIgnore
    @Column(name = "json_id")
    private long jsonId;

    @Column(name="name")
    private String name;

    @Column(name="troops")
    private int troops;

    @Lob
    private String path;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "player_id", foreignKey = @ForeignKey(name = "FK_TERRITORY_PLAYER_ID"))
    private Player player;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "continent_id", foreignKey = @ForeignKey(name = "FK_TERRITORY_CONTINENT_ID"))
    private Continent continent;

    @Override
    public String toString() {
        return "Territory{" +
                "id=" + id +
                "name='" + name + '\'' +
                ", troops=" + troops +
                '}';
    }

    @JsonIgnore
    @OneToMany(mappedBy = "territory", targetEntity = Border.class, cascade = {
            CascadeType.ALL }, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Border> borders;

    public Territory() {
        this.borders = new ArrayList<>();
        this.troops = 0;
    }

    public int addTroops(int troops){
        this.troops += troops;
        return this.troops;
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();

        json.put("id", id);
        json.put("name", name);
        json.put("troops", troops);
        json.put("path", path);
        if (player != null) json.put("player", player.getUsername());
        else json.put("player_id", 0);
        json.put("neighbors", borders.stream().map(Border::getAdjacentId).toArray());

        return json;
    }
}
