package com.error402.server.model.dto.gameoverview;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FinishedDto {
    private String map;
    private String winner;
    private long sessionId;
}
