package com.error402.server.model.dto;

import com.error402.server.model.GameType;
import com.error402.server.model.PlayerType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SimulatorDto {
    private GameType gameType;
    private String username;
    private PlayerType playerType;
    private int games;
}
