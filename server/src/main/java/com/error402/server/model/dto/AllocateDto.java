package com.error402.server.model.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter @Setter
public class AllocateDto {
    private long sessionId;
    private long territoryId;
    private int troops;
}
