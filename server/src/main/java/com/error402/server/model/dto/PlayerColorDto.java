package com.error402.server.model.dto;

import com.error402.server.model.PlayerColor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PlayerColorDto {
    private long sessionId;
    private String username;
    private PlayerColor color;
}
