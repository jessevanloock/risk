package com.error402.server.model.dto;

import com.error402.server.model.Player;
import com.error402.server.model.Session;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class SessionLobbyDto {
    private long sessionId;
    private String host;
    private List<Player> players;

    public SessionLobbyDto(Session session) {
        sessionId = session.getId();
        host = session.getHost();
        players = session.getPlayers();
    }
}
