package com.error402.server.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SessionInviteDto {
    private long sessionId;
    private String username;
}
