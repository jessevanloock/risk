package com.error402.server.model.dto;

import com.error402.server.model.Territory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class BestTerritoryDto {
    private Territory territory;
    private double Difference;
}
