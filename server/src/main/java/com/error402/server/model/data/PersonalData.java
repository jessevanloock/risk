package com.error402.server.model.data;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to demonstrate personal data.
 *
 * @Author:
 */
@Getter
@Setter
public class PersonalData {
    private String username;
    // Conquests
    private long conquestsTotal;
    private long conquestsWon;
    private long conquestsLost;
    private float conquestsRatio;
    // Attacks
    private long attacksTotal;
    private long attacksSuccessful;
    private long attacksFailed;
    private float attacksRatio;
    // Defends
    private long defendsTotal;
    private long defendsSuccessful;
    private long defendsFailed;
    private float defendsRatio;
    // Troops
    private long troopsDeployed;
    private long troopsDefeated;
    private float troopsLost;
    // History
    private List<SessionData> history;

    public PersonalData(String username) {
        this.username = username;
        this.history = new ArrayList<>();
    }
}
