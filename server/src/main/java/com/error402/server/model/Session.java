package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.json.simple.JSONObject;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "session")
@Getter
@Setter
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "map")
    private String map;

    @Column(name = "host")
    private String host;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "winner")
    private String winner;

    @OneToMany(mappedBy = "session", targetEntity = Player.class, orphanRemoval = true, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private List<Player> players;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "session", targetEntity = Turn.class,  orphanRemoval = true)
    private List<Turn> turns;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "session", targetEntity = Invite.class, orphanRemoval = true)
    private List<Invite> invites;

    @Column(name = "status")
    private Status status;

    @Column(name = "gameType")
    private GameType gameType;

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", winner='" + winner + '\'' +
                ", status=" + status +
                ", gameType=" + gameType +
                '}';
    }

    @Lob
    private String startState;

    public Session() {
        this.status = Status.LOBBY;
        this.players = new ArrayList<>();
        this.turns = new ArrayList<>();
        this.invites = new ArrayList<>();
    }

    public Session(GameType gameType) {
        this.status = Status.LOBBY;
        this.gameType = gameType;
        this.players = new ArrayList<>();
        this.turns = new ArrayList<>();
        this.invites = new ArrayList<>();
    }

    public void getGameStatus(){
        for (Player player: players) {
            System.out.println(player.getUsername() + ": ");
            for (Territory territory: player.getTerritories()) {
                System.out.println(territory.getName() + " with " + territory.getTroops() + " troops.");
            }
        }
    }
}
