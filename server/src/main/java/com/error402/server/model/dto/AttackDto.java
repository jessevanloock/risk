package com.error402.server.model.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class AttackDto {
    private long sessionId;
    private long attackingTerritoryId;
    private long defendingTerritoryId;
}
