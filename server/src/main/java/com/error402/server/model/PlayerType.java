package com.error402.server.model;

public enum PlayerType {
    HUMAN,
    EASY,
    MEDIUM,
    HARD
}
