package com.error402.server.model;

import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.FortifyDto;
import com.error402.server.utils.Utils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "player")
@Getter
@Setter
public class Player {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "color")
    private PlayerColor color;

    @JsonIgnore
    @Column(name = "troops")
    private int troops;

    @Column(name = "player_type")
    private PlayerType playerType;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "session_id", foreignKey = @ForeignKey(name = "FK_PLAYER_SESSION_ID"))
    private Session session;

    @JsonIgnore
    @OneToMany(mappedBy = "player", targetEntity = Turn.class, orphanRemoval = true)
    private List<Turn> turns;

    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "player", targetEntity = Territory.class, orphanRemoval = true)
    private List<Territory> territories;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_RISK_USER_ID"))
    private RiskUser riskUser;

    public Player() {
        troops = 0;
        turns = new ArrayList<>();
        territories = new ArrayList<>();
    }

    public Player(String username, PlayerType playerType) {
        this.username = username;
        troops = 0;
        turns = new ArrayList<>();
        territories = new ArrayList<>();
        this.playerType = playerType;
        this.color = PlayerColor.red; // default color
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Player)) {
            return false;
        }

        Player player = (Player) o;

        return this.id == player.id;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", troops=" + troops +
                ", playerType=" + playerType +
                ", territoryAmount=" + territories.size() +
                '}';
    }
}
