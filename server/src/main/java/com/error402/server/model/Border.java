package com.error402.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="border")
@Getter
@Setter
public class Border {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "territory_id", foreignKey = @ForeignKey(name = "FK_BORDER_TERRITORY_ID"))
    private Territory territory;

    @Column(name = "adjacent_territory")
    private long adjacentId;

}
