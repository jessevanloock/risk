package com.error402.server.dao;

import com.error402.server.model.GameType;
import com.error402.server.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SessionRepository extends JpaRepository<Session, Long> {
    Optional<List<Session>> findAllByGameType(GameType gameType);
}
