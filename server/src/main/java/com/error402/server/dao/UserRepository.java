package com.error402.server.dao;

import com.error402.server.model.RiskUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<RiskUser, Long> {
    Optional<RiskUser> findByUsername(String username);
}
