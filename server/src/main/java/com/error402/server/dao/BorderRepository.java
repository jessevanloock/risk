package com.error402.server.dao;

import com.error402.server.model.Border;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BorderRepository extends JpaRepository<Border, Long> {
}
