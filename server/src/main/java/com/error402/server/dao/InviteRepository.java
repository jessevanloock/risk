package com.error402.server.dao;

import com.error402.server.model.Invite;
import com.error402.server.model.RiskUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface InviteRepository extends JpaRepository<Invite, Long> {
    Optional<List<Invite>> findAllBySender(RiskUser sender);
    Optional<List<Invite>> findAllByReceiver(RiskUser receiver);
    Optional<List<Invite>> findAllByReceiver_Username(String username);
}
