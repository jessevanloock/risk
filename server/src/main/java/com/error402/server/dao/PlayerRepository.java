package com.error402.server.dao;

import com.error402.server.model.Player;
import com.error402.server.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    Optional<Player> findByUsernameAndSessionId(String username, long sessionId);
    Optional<List<Player>> findAllBySessionId(long sessionId);
    Optional<List<Player>> findAllByUsername(String username);
    Optional<List<Player>> findAllByUsernameAndSession_Status(String username, Status status);
}
