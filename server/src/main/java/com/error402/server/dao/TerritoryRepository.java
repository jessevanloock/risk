package com.error402.server.dao;

import com.error402.server.model.Territory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TerritoryRepository extends JpaRepository<Territory, Long> {
    Optional<List<Territory>> findAllByPlayerUsernameAndPlayerSessionId(String username, long sessionId);
    Territory findByPlayerSessionIdAndName(long sessionId, String name);
    Territory findByPlayerSessionIdAndId(long sessionId, long territoryId);
}
