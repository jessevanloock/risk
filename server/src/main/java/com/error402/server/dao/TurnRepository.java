package com.error402.server.dao;

import com.error402.server.model.Turn;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TurnRepository extends JpaRepository<Turn, Long> {
    Optional<Turn> findFirstBySessionIdOrderByIdDesc(long sessionId);
}
