package com.error402.server.config;

import com.error402.server.communication.chat.ChatStub;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class BeanConfiguration {

    @Bean
    ChatStub chatStub() {
        return new ChatStub();
    }

    @Bean
    public ExecutorService executor() {
        return Executors.newFixedThreadPool(6);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
