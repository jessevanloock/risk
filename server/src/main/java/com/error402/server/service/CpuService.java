package com.error402.server.service;

import com.error402.server.ai.montecarlo.GameState;
import com.error402.server.ai.montecarlo.MonteCarloTreeSearch;
import com.error402.server.dao.PlayerRepository;
import com.error402.server.dao.SessionRepository;
import com.error402.server.dao.TerritoryRepository;
import com.error402.server.dao.TurnRepository;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.event.CpuTurnEvent;
import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.BestTerritoryDto;
import com.error402.server.model.dto.FortifyDto;
import com.error402.server.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Handles al CPU specific logic.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
@Service
public class CpuService {

    @Value("${websocket.topic.state}")
    private String GAME_STATE_TOPIC;

    @Value("${ai.level.allocation}")
    private int ALLOCATION_LEVEL;

    @Value("${ai.level.attack}")
    private int ATTACK_LEVEL;

    //region INJECTED PROPERTIES

    private final PlayerRepository playerRepository;
    private final SessionRepository sessionRepository;
    private final SessionService sessionService;
    private final NotifierService notifierService;
    private final TurnRepository turnRepository;
    private final TerritoryRepository territoryRepository;
    private final GameService gameService;
    private final MonteCarloTreeSearch mcts;

    //endregion

    /**
     * Plays a turn.
     *
     * @param event Object that contains the session with the username of the player whose turn it is
     * @return Session updated after the turn of an AI-player
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws PlayerNotInSessionException Thrown when the player is not a participant.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws NotPlayerTurnException      Thrown when it isn't the player's turn.
     */
    @EventListener
    public Session playTurn(CpuTurnEvent event) throws EntityNotFoundException, InvalidPhaseException, PlayerNotInSessionException, ParseException, IOException, NoPlayersInSessionException, NotPlayerTurnException {

        Session session = sessionService.getSession(event.getSession().getId());

        try {
            playAllocationPhase(session, event.getUsername());                                                          // plays the allocation phase
        } catch (Exception e) {
            if (e instanceof InvalidOperationException)
                log.info(e.getMessage());
            else
                log.error(e + ": " + e.getMessage());
        }

        session = gameService.nextPhase(session, event.getUsername());                                                  // start the next phase

        notifierService.notify(                                                                                         // notify all players in session of the state update
                session.getPlayers(),
                GAME_STATE_TOPIC,
                session);

        try {
            playAttackPhase(session, event.getUsername());                                                              // plays the attack phase
        } catch (Exception e) {
            if (e instanceof InvalidOperationException)
                log.info(e.getMessage());
            else
                log.error(e + ": " + e.getMessage());
        }

        session = gameService.nextPhase(session, event.getUsername());                                                  // start the next phase

        notifierService.notify(                                                                                         // notify all players in session of the state update
                session.getPlayers(),
                GAME_STATE_TOPIC,
                session);

        try {
            playFortificationPhase(session, event.getUsername());                                                       // plays the fortification phase
        } catch (Exception e) {
            if (e instanceof InvalidOperationException)
                log.info(e.getMessage());
            else
                log.error(e + ": " + e.getMessage());
        }

        if (sessionService.getSession(session.getId()).getStatus() != Status.FINISHED)
            session = gameService.nextPhase(session, event.getUsername());                                              // start the next phase

        notifierService.notify(                                                                                         // notify all players in session of the state update
                session.getPlayers(),
                GAME_STATE_TOPIC,
                session);

        return session;
    }

    //region PHASES

    /**
     * Plays the allocation phase of an AI-player.
     *
     * @param session  Session in which the game is played
     * @param username Username of the AI-player that will play the allocation phase
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws PlayerNotInSessionException Thrown when the player is not a participant.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws PlayerNotAIException        Thrown when the player is not an AI.
     */
    public void playAllocationPhase(Session session, String username)
            throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, NoPlayersInSessionException, IOException, ParseException, PlayerNotAIException {

        Player player = getAIPlayerOfSession(session, username);

        AllocateDto dto;
        while (player.getTroops() > 0) {                                                                                // keep performing allocations until the player has no troops left
            switch (player.getPlayerType()) {                                                                           // determine the allocation depending on what type the player is
                case EASY:
                    log.info("Allocation phase started for AI of type EASY.");
                    dto = determineAllocationForEasy(player);
                    break;
                case MEDIUM:
                    log.info("Allocation phase started for AI of type MEDIUM.");
                    dto = determineAllocationForMedium(session, player);
                    break;
                case HARD:
                    log.info("Allocation phase started for AI of type HARD.");
                    dto = determineAllocationForHard(player, session);
                    break;
                default:
                    throw new PlayerNotAIException("This player is not an AI.");
            }

            if (dto != null) {
                session = gameService.allocate(player.getUsername(), dto);                                              // perform the allocation

                log.info("Allocation phase for AI successfully finished.");

                notifierService.notify(                                                                                 // notify all players in session of the state update
                        session.getPlayers(),
                        GAME_STATE_TOPIC,
                        session);

                player = getAIPlayerOfSession(session, username);                                                       // update the AI-player info
            }
        }
    }

    /**
     * Plays the attack phase of an AI-player.
     *
     * @param session  Session in which the game is played
     * @param username Username of the AI-player that will play the allocation phase
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws PlayerNotInSessionException Thrown when the player is not a participant.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws PlayerNotAIException        Thrown when the player is not an AI.
     */
    public void playAttackPhase(Session session, String username)
            throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, NoPlayersInSessionException, IOException, ParseException, PlayerNotAIException {

        Player player = getAIPlayerOfSession(session, username);                                                        // get the AI-player info

        while (session.getStatus() != Status.FINISHED) {                                                                                           // keep performing attacks until otherwise determined
            session = sessionService.getSession(session.getId());

            AttackDto dto;
            switch (player.getPlayerType()) {                                                                           // determine the allocation depending on what type the player is
                case EASY:
                    log.info("Attack phase started for AI of type EASY.");
                    dto = determineAttackForEasy(player);
                    break;
                case MEDIUM:
                    log.info("Attack phase started for AI of type MEDIUM.");
                    dto = determineAttackForMedium(session, player);
                    break;
                case HARD:
                    log.info("Attack phase started for AI of type HARD.");
                    dto = determineAttackForHard(session);
                    break;
                default:
                    throw new PlayerNotAIException("This player is not an AI.");
            }

            if (dto != null) {
                gameService.attack(player.getUsername(), dto);                                                          // perform the attack

                log.info("Attack phase for AI successfully finished.");

                notifierService.notify(                                                                                 // notify all players in session of the state update
                        session.getPlayers(),
                        GAME_STATE_TOPIC,
                        session);
            }

            player = getAIPlayerOfSession(session, username);                                                           // update the AI-player info
        }
    }

    /**
     * Plays the fortification phase of an AI-player.
     *
     * @param session  Session in which the game is played
     * @param username Username of the AI-player that will play the fortification phase
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws PlayerNotAIException        Thrown when the player is not an AI.
     */
    public void playFortificationPhase(Session session, String username)
            throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, NoPlayersInSessionException, IOException, ParseException, PlayerNotAIException {

        Player player = getAIPlayerOfSession(session, username);                                                        // get the AI-player info

        FortifyDto dto;
        switch (player.getPlayerType()) {                                                                               // determine the allocation depending on what type the player is
            case EASY:
                log.info("Fortification phase started for AI of type EASY");
                dto = determineFortificationForEasy();
                break;
            case MEDIUM:
                log.info("Fortification phase started for AI of type MEDIUM");
                dto = determineFortificationForMedium(session, player);
                break;
            case HARD:
                log.info("Fortification phase started for AI of type HARD");
                dto = determineFortificationForHard(session);
                break;
            default:
                throw new PlayerNotAIException("This player is not an AI.");
        }

        if (dto != null) {
            gameService.fortify(username, dto);                                                                         // perform the fortification

            log.info("Fortification phase for AI successfully finished.");

            notifierService.notify(                                                                                     // notify all players in session of the state update
                    session.getPlayers(),
                    GAME_STATE_TOPIC,
                    session);
        }
    }

    //endregion

    //region EASY CPU

    /**
     * Determines the allocation for an AI of type EASY.
     *
     * @param player Easy AI-player that will allocate troops
     * @return Dto that contains how many troops are allocated to which territory
     * @throws InvalidOperationException Thrown when an operation can't be executed.
     */
    public AllocateDto determineAllocationForEasy(Player player) throws InvalidOperationException {

        if (player.getTroops() > 0) {
            Territory territory = player.getTerritories().get(Utils.getRandom(0, player.getTerritories().size() - 1));
            int troops = Utils.getRandom(1, player.getTroops());

            return new AllocateDto(player.getSession().getId(), territory.getId(), troops);
        }

        throw new InvalidOperationException("No more troops to allocate.");
    }

    /**
     * Determines the attack for an AI of type EASY.
     *
     * @param player Easy AI-player that will attack a territory
     * @return Dto that contains which territory attacks which territory
     * @throws EntityNotFoundException   Thrown when a required entity can't be found.
     * @throws InvalidOperationException Thrown when an operation can't be executed.
     */
    public AttackDto determineAttackForEasy(Player player) throws EntityNotFoundException, InvalidOperationException {
        final int MIN_DIFFERENCE = -1;
        player = sessionService.getPlayer(player.getId());
        List<Territory> territories = getTerritoriesWithEnemyBorder(player);
        Territory attackingTerritory = territories.get(Utils.getRandom(0, territories.size() - 1));
        List<Border> borders = attackingTerritory.getBorders();
        Territory defendingTerritory = new Territory();
        boolean isEnemy = false;
        int counter = 0;

        while (!isEnemy) {
            defendingTerritory = getTerritoryFromBorder(borders.get(Utils.getRandom(0, borders.size() - 1)));

            if (!defendingTerritory.getPlayer().equals(player))
                isEnemy = true;
            else if (counter == borders.size())
                throw new InvalidOperationException("No enemies found.");

            counter++;
        }

        if (attackingTerritory.getTroops() - defendingTerritory.getTroops() > MIN_DIFFERENCE && attackingTerritory.getTroops() > 1)
            return new AttackDto(player.getSession().getId(), attackingTerritory.getId(), defendingTerritory.getId());

        throw new InvalidOperationException("Requirements to attack not met.");
    }

    /**
     * Determines the fortification for an AI of type EASY.
     *
     * @return Dto that contains how many troops are send from which territory to which territory
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     */
    public FortifyDto determineFortificationForEasy() throws InvalidOperationException {
        throw new InvalidOperationException("Easy AI can't fortify");
    }

    //endregion

    //region MEDIUM CPU

    /**
     * Determines the allocation for an AI of type MEDIUM.
     *
     * @param session Session in which the game is played
     * @param player  Medium AI-player that will allocate troops
     * @return Dto that contains how many troops are allocated to which territory
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     */
    public AllocateDto determineAllocationForMedium(Session session, Player player)
            throws NoPlayersInSessionException {

        final int MAX_DIFFERENCE = 5;
        double troops;

        List<BestTerritoryDto> bestTerritories = getBestTerritory(session, player);
        Territory territory = bestTerritories.get(0).getTerritory();

        if (Math.abs(player.getTroops() - bestTerritories.get(0).getDifference()) > MAX_DIFFERENCE) {
            if (Math.abs(player.getTroops() - bestTerritories.get(0).getDifference()) < MAX_DIFFERENCE * 2) {
                territory = bestTerritories.get(1).getTerritory();
            }
            troops = player.getTroops();
        } else if (bestTerritories.get(0).getDifference() <= MAX_DIFFERENCE) {
            troops = player.getTroops();
        } else {
            troops = bestTerritories.get(0).getDifference() - MAX_DIFFERENCE;
        }

        return new AllocateDto(
                player.getSession().getId(),
                territory.getId(),
                (int) troops);
    }

    /**
     * Determines the attack for an AI of type MEDIUM.
     *
     * @param player  Medium AI-player that will attack a territory
     * @param session Session in which the game is played
     * @return Dto that contains which territory attacks which territory
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     */
    public AttackDto determineAttackForMedium(Session session, Player player)
            throws NoPlayersInSessionException, EntityNotFoundException, InvalidOperationException {

        final int MIN_DIFFERENCE = 5;
        final double PERCENTAGE_DIFFERENCE = .5;
        int amountOfAttackChecks = 5;
        List<BestTerritoryDto> bestTerritories = getBestTerritory(session, player);

        if (bestTerritories.size() < amountOfAttackChecks)
            amountOfAttackChecks = bestTerritories.size();

        for (int i = 0; i < amountOfAttackChecks; i++) {
            List<Territory> enemyTerritories = new ArrayList<>();
            List<Border> borders = bestTerritories.get(i).getTerritory().getBorders();
            for (Border border : borders) {
                Territory territory = getTerritoryFromBorder(border);
                if (!territory.getPlayer().equals(player)) {
                    enemyTerritories.add(territory);
                }
            }

            Territory defendingTerritory = enemyTerritories.stream().min(Comparator.comparing(Territory::getTroops))
                    .orElseThrow(() -> new EntityNotFoundException("Could not find territory with least troops"));

            if ((bestTerritories.get(i).getTerritory().getTroops() - defendingTerritory.getTroops() > MIN_DIFFERENCE
                    || (double) bestTerritories.get(i).getTerritory().getTroops() / (double) defendingTerritory.getTroops() > PERCENTAGE_DIFFERENCE)
                    && bestTerritories.get(i).getTerritory().getTroops() > 1
            ) {
                return new AttackDto(player.getSession().getId(),
                        bestTerritories.get(i).getTerritory().getId(),
                        defendingTerritory.getId());
            }
        }

        throw new InvalidOperationException("Requirements to attack not met");
    }

    /**
     * Determines fortification for an AI of type MEDIUM.
     *
     * @param session Session in which the game is played
     * @param player  Medium AI-player that will fortify a territory
     * @return Dto that contains how many troops are send from which territory to which territory
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     */
    public FortifyDto determineFortificationForMedium(Session session, Player player)
            throws NoPlayersInSessionException, EntityNotFoundException, InvalidOperationException {

        List<Territory> territoriesWithoutEnemyBorders = player.getTerritories().stream()
                .filter(territory -> !getTerritoriesWithEnemyBorder(player).contains(territory))
                .sorted(Comparator.comparing(Territory::getTroops).reversed())
                .collect(Collectors.toList());

        if (territoriesWithoutEnemyBorders.isEmpty())
            throw new InvalidOperationException("No territories without enemy borders");

        Territory originTerritory = territoriesWithoutEnemyBorders.get(0);
        if (originTerritory.getTroops() < 2)
            throw new InvalidOperationException("Not enough troops");

        LinkedHashMap<Territory, Integer> territoryMap = getTotalEnemyTroopsOfTerritory(session, player);
        for (Map.Entry<Territory, Integer> pair : territoryMap.entrySet()) {
            Territory destinationTerritory = pair.getKey();
            if (gameService.canReachTerritoryToFortify(originTerritory.getId(), destinationTerritory.getId())) {
                return new FortifyDto(session.getId(),
                        originTerritory.getId(),
                        destinationTerritory.getId(),
                        originTerritory.getTroops() - 1);
            }
        }

        throw new InvalidOperationException("No connecting territory");
    }

    //endregion

    //region HARD CPU

    /**
     * Determines the allocation for an AI of type HARD.
     *
     * @param player  Hard AI-player that will allocate troops
     * @param session Session in which the game is played
     * @return Dto that contains how many troops are allocated to which territory
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws InvalidPhaseException   Thrown when a phase can't be found.
     */
    public AllocateDto determineAllocationForHard(Player player, Session session)
            throws EntityNotFoundException, InvalidPhaseException {

        GameState gameState = mcts.findNextAllocate(session, ALLOCATION_LEVEL, player.getTroops());

        return new AllocateDto(session.getId(),
                territoryRepository.findByPlayerSessionIdAndName(session.getId(), gameState.getToTerritory()).getId(), 1);
    }

    /**
     * Determines the attack for an AI of type HARD.
     *
     * @param session Session in which the game is played
     * @return Dto that contains which territory attacks which territory
     * @throws EntityNotFoundException      Thrown when a required entity can't be found.
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     * @throws InvalidPhaseException        Thrown when a phase can't be found.
     */
    public AttackDto determineAttackForHard(Session session)
            throws EntityNotFoundException, InvalidOperationException, InvalidPhaseException {

        GameState gameState = mcts.findNextAttack(session, ATTACK_LEVEL);

        return new AttackDto(session.getId(),
                territoryRepository.findByPlayerSessionIdAndName(session.getId(), gameState.getFromTerritory()).getId(),
                territoryRepository.findByPlayerSessionIdAndName(session.getId(), gameState.getToTerritory()).getId());
    }

    /**
     * Determines the fortification for an AI of type HARD.
     *
     * @param session Session in which the game is played
     * @return Dto that contains how many troops are send from which territory to which territory
     * @throws EntityNotFoundException      Thrown when a required entity can't be found.
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     * @throws InvalidPhaseException        Thrown when a phase can't be found.
     */
    public FortifyDto determineFortificationForHard(Session session)
            throws EntityNotFoundException, InvalidOperationException, InvalidPhaseException {

        GameState gameState = mcts.findNextFortify(session);

        if (gameState == null)
            throw new InvalidOperationException("MCTS didn't find next fortification");

        return new FortifyDto(session.getId(),
                    territoryRepository.findByPlayerSessionIdAndName(session.getId(), gameState.getFromTerritory()).getId(),
                    territoryRepository.findByPlayerSessionIdAndName(session.getId(), gameState.getToTerritory()).getId(),
                    gameState.getTroops());
    }

    //endregion

    //region LOGIC

    /**
     * Creates a list of all territories that have at least one enemy territory as neighbour.
     *
     * @param player AI-player whose territories need to be checked for enemy borders
     * @return List of territories with enemy borders
     */
    public List<Territory> getTerritoriesWithEnemyBorder(Player player) {

        List<Territory> allTerritories = player.getTerritories();
        List<Territory> territoriesWithEnemyBorder = new ArrayList<>();
        for (Territory t : allTerritories) {
            if (!allTerritories.stream().map(Territory::getId).collect(Collectors.toList())                             // check if al borderIds are in the list of all territories of that player if not
                    .containsAll(t.getBorders().stream().map(Border::getAdjacentId).collect(Collectors.toList()))) {    // that means this territory has an enemy territory adjacent and add this territory to the new list
                territoriesWithEnemyBorder.add(t);
            }
        }

        return territoriesWithEnemyBorder;
    }

    /**
     * Creates a map which holds all territories with enemy borders and the amount of enemy troops around them.
     *
     * @param player  AI-player whose total enemy troops around his territories are counted
     * @param session Session in which the game is played
     * @return LinkedHashMap with all enemy territories and how many troops they have
     * @throws NoPlayersInSessionException Thrown when no players are in this session.
     */
    public LinkedHashMap<Territory, Integer> getTotalEnemyTroopsOfTerritory(Session session, Player player)
            throws NoPlayersInSessionException {

        List<Territory> allTerritories = new ArrayList<>();
        List<Territory> territoriesWithEnemyBorder = getTerritoriesWithEnemyBorder(player);
        Map<Territory, Integer> unsortedMap = new HashMap<>();

        List<Player> players = playerRepository.findAllBySessionId(session.getId())
                .orElseThrow(() -> new NoPlayersInSessionException("No players found."));

        for (Player p : players) {
            if (p != player) {
                allTerritories.addAll(p.getTerritories());                                                                 //get all territories of this game execpt yours
            }
        }

        for (Territory t : territoriesWithEnemyBorder) {
            List<Border> borders = t.getBorders();
            for (Border b : borders) {
                for (Territory t1 : allTerritories) {
                    if (t1.getId() == b.getAdjacentId()) {                                                              //get the troops of an enemy territory that borders yours and add them to the map
                        unsortedMap.merge(t, t1.getTroops(), Integer::sum);                                             //this map contains all your territories with enemy borders with the amount of enemy troops around them
                    }
                }
            }
        }

        LinkedHashMap<Territory, Integer> totalEnemyTroopsOfterritory = new LinkedHashMap<>();                          //Sort the map from highest amount of enemy troops to the lowest amount
        unsortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> totalEnemyTroopsOfterritory.put(x.getKey(), x.getValue()));

        return totalEnemyTroopsOfterritory;
    }

    /**
     * Calculates the best territory with the highest difference in troop count.
     *
     * @param player  AI-player whom the list is calculated for
     * @param session Session in which the game is played
     * @return List of territories with the highest difference in troop count
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     */
    public List<BestTerritoryDto> getBestTerritory(Session session, Player player) throws NoPlayersInSessionException {

        LinkedHashMap<Territory, Integer> territoriesAndEnemyTroops = getTotalEnemyTroopsOfTerritory(session, player);
        List<BestTerritoryDto> bestTerritories = new ArrayList<>();
        for (Map.Entry<Territory, Integer> pair : territoriesAndEnemyTroops.entrySet()) {
            Territory territory = territoryRepository.findByPlayerSessionIdAndId(session.getId(), pair.getKey().getId());
            bestTerritories.add(new BestTerritoryDto(territory, territory.getTroops() - pair.getValue()));
        }

        bestTerritories.sort(Comparator.comparingDouble(BestTerritoryDto::getDifference).reversed());

        return bestTerritories;
    }

    //endregion

    //region HELPER METHODS

    /**
     * Retrieves the AI-player with the given username in the given session.
     *
     * @param session   Session in which the game is played
     * @param username  Username of the AI-player that has to be retrieved
     * @return AI-player with the given username in the given session
     * @throws PlayerNotInSessionException  Thrown when the AI-player is not a participant.
     */
    private Player getAIPlayerOfSession(Session session, String username)
            throws PlayerNotInSessionException {

        return session.getPlayers().stream()                                                                            // get the AI-player info
                .filter(p -> p.getUsername().equals(username) && !p.getPlayerType().equals(PlayerType.HUMAN))
                .findFirst()
                .orElseThrow(() -> new PlayerNotInSessionException("Player is not in this session."));
    }

    /**
     * Retrieves the neighbouring territory
     *
     * @param border Border to neighbouring territory
     * @return Neighbouring territory
     * @throws EntityNotFoundException  Thrown when a required entity can't be found.
     */
    public Territory getTerritoryFromBorder(Border border)
            throws EntityNotFoundException {

        return territoryRepository.findById(border.getAdjacentId())
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find territory."));
    }

    //endregion

    //region SIMULATOR

    /**
     * Creates a session with only AI-players.
     *
     * @param username Username of the host
     * @param type     PlayerType of the host
     * @return Created session with only AI-players
     */
    public Session createCpuSession(String username, PlayerType type) {

        Player player = playerRepository.save(new Player(username, type));                                              // create a new player

        Session session = new Session(GameType.CPU);                                                                    // create a new session
        session.getPlayers().add(player);
        session.setStatus(Status.LOBBY);
        session.setHost(player.getUsername());
        session = sessionRepository.save(session);

        player.setSession(session);                                                                                     // add the player to the session
        player.setColor(PlayerColor.blue);
        player = playerRepository.save(player);

        log.info("Successfully created a new CPU session: " + session.getId());

        return player.getSession();                                                                                     // return the updated session
    }

    /**
     * Initializes a session with only AIs to a state where it can be played.
     *
     * @param username  Username of the authenticated sender
     * @param sessionId Id of the session
     * @param map       Map on which will be played
     * @return Session with initialized game for AIs
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws NotEnoughPlayersException   Thrown when there are not enough players in this session to start.
     * @throws NoSuchElementException      Thrown when no such element is found.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    public Session initializeCpuGame(String username, long sessionId, String map)
            throws IOException, ParseException, NoPlayersInSessionException, NotEnoughPlayersException, NoSuchElementException, EntityNotFoundException, InvalidOperationException {

        Session session = sessionService.getSession(sessionId);
        List<Player> players = sessionService.getAllPlayersOfSession(sessionId);                                        // retrieve all players

        if (players.size() < 3)
            throw new NotEnoughPlayersException("Not enough players to initialize");                                    // throw exception when there are not enough players

        if (!players.get(0).getUsername().equals(username))
            throw new InvalidOperationException("This player is not the host");                                         // throw exception when the sender is not the host

        JSONObject json = Utils.readMap(map);                                                                           // retrieve the json data to initialize the map
        session.setMap(map);

        List<JSONObject> jsonTerritories = new ArrayList<>();                                                           // retrieve all territory objects from json
        for (Object obj : (JSONArray) requireNonNull(json).get("continents")) {
            Object countries = ((JSONObject) obj).get("countries");
            if (countries != null) jsonTerritories.addAll((JSONArray) countries);
        }

        List<Territory> territories = new ArrayList<>();                                                                // create all continent and territory entities
        for (Continent continent : gameService.createEntities(json)) {
            territories.addAll(continent.getTerritories());                                                             // retrieve all created territory entities
        }

        territories = gameService.createBorders(territories, jsonTerritories);                                          // create borders for all territories
        territories = gameService.assignPlayersToTerritories(territories, players);                                     // assign territories to players
        gameService.assignTroopsToTerritories(territories, players);                                                    // assign troops to all territories

        session = sessionService.getSession(sessionId);
        session.setStatus(Status.PLAYING);                                                                              // change the session status to PLAYING
        session = sessionService.saveSession(session);

        return session;
    }

    /**
     * Initializes the first turn to play a game with only AIs.
     *
     * @param session Session in which the game is played
     * @return Session with an initial turn so the game can start
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws IOException             Thrown when an I/O error occurs.
     * @throws ParseException          Thrown when the state can't be parsed.
     */
    public Session initialTurn(Session session)
            throws EntityNotFoundException, IOException, ParseException {

        Date date = new Date();
        Player player = session.getPlayers().get(0);

        player.setTroops(gameService.calculateTroopsForAllocation(player));
        List<Phase> phases = new ArrayList<>();
        Turn turn = new Turn(new Timestamp(date.getTime()), player, session, phases);
        turn = turnRepository.save(turn);
        session.getTurns().add(turn);
        playerRepository.save(player);

        return gameService.createPhase(session, PhaseType.ALLOCATION);
    }

    //endregion
}
