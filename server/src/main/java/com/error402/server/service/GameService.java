package com.error402.server.service;

import com.error402.server.dao.*;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.event.CpuTurnEvent;
import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.FortifyDto;
import com.error402.server.utils.Node;
import com.error402.server.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Handles all inside-session logic.
 * Always results in an updated state.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
@Service
public class GameService {

    @Value("${game.players.min}")
    private int MIN_PLAYERS;

    @Value("${game.troops.allocation.min}")
    private int ALLOCATE_BASE_TROOPS;

    @Value("${game.troops.start.max}")
    private int START_MAX_TROOPS;

    @Value("${game.troops.start.factor}")
    private int START_FACTOR_TROOPS;

    @Value("${game.dice.attack.max}")
    private int MAX_ATTACKING_DICE;

    @Value("${game.dice.defend.max}")
    private int MAX_DEFENDING_DICE;

    //region INJECTED PROPERTIES

    private final ContinentRepository continentRepository;
    private final TerritoryRepository territoryRepository;
    private final TurnRepository turnRepository;
    private final PhaseRepository phaseRepository;
    private final ActionRepository actionRepository;
    private final BorderRepository borderRepository;
    private final SessionService sessionService;
    private final PlayerRepository playerRepository;
    private final ApplicationEventPublisher publisher;

    @Autowired
    private final ExecutorService executor;

    //endregion

    //region INITIALIZE

    /**
     * Initializes a session to a state where it can be played.
     *
     * @param username  Username of the authenticated sender
     * @param sessionId Id of the session
     * @param map       Map on which will be played
     * @return Session with initialized game
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws NotEnoughPlayersException   Thrown when there are not enough players in this session to start.
     * @throws NoSuchElementException      Thrown when no such element is found.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    public Session initialize(String username, long sessionId, String map)
            throws IOException, ParseException, NoPlayersInSessionException, NotEnoughPlayersException, NoSuchElementException, EntityNotFoundException, InvalidOperationException {

        log.info(username + " tries to initialize the game.");

        try {
            Session session = sessionService.getSession(sessionId);

            if (session.getPlayers().size() < MIN_PLAYERS)
                throw new NotEnoughPlayersException("Not enough players to initialize.");                               // throw exception when there are not enough players

            if (!session.getHost().equals(username))
                throw new InvalidOperationException("This player is not the host.");                                    // throw exception when the sender is not the host

            JSONObject json = Utils.readMap(map);                                                                       // retrieve the json data to initialize the map
            session.setMap(map);

            List<JSONObject> jsonTerritories = new ArrayList<>();                                                       // retrieve all territory objects from json
            for (Object obj : (JSONArray) requireNonNull(json).get("continents")) {
                Object countries = ((JSONObject) obj).get("countries");
                if (countries != null) jsonTerritories.addAll((JSONArray) countries);
            }

            List<Territory> territories = new ArrayList<>();                                                            // create all continent and territory entities
            for (Continent continent : createEntities(json)) {
                territories.addAll(continent.getTerritories());                                                         // retrieve all created territory entities
            }

            territories = createBorders(territories, jsonTerritories);                                                  // create borders for all territories
            territories = assignPlayersToTerritories(territories, session.getPlayers());                                // assign territories to players
            assignTroopsToTerritories(territories, session.getPlayers());                                               // assign troops to all territories

            session.setStatus(Status.PLAYING);                                                                          // change the session status to PLAYING
            session.setMap(map);
            session = nextTurn(session);                                                                                // start the first turn
            session = saveStartState(sessionId);
            session = sessionService.saveSession(session);

            log.info("Game is successfully initialized.");

            return session;

        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }
    }

    /**
     * Persists the start state of a session.
     *
     * @param sessionId Id of the session
     * @return Session with a start state
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     */
    public Session saveStartState(long sessionId)
            throws EntityNotFoundException, IOException, ParseException, NoPlayersInSessionException {

        Session session = sessionService.getSession(sessionId);
        JSONObject startState = Utils.convertSessionToJson(session);
        session.setStartState(new ObjectMapper().writeValueAsString(startState));

        return sessionService.saveSession(session);
    }

    /**
     * Creates continent and territory entities.
     * Called in initialize().
     *
     * @param json Json to retrieve data from
     * @return List of continent entities with each their belonging territory entities
     */
    public List<Continent> createEntities(JSONObject json) {

        List<Continent> continents = new ArrayList<>();                                                                 // create all continents one by one
        for (Object obj : (JSONArray) requireNonNull(json).get("continents")) {
            Continent continent = new Continent();
            continent.setName(((JSONObject) obj).get("name").toString());
            continent.setBonus(Integer.parseInt(((JSONObject) obj).get("bonus").toString()));

            for (Object o : (JSONArray) ((JSONObject) obj).get("countries")) {                                          // create all territories for this continent
                Territory territory = new Territory();
                territory.setName(((JSONObject) o).get("name").toString());
                territory.setPath(((JSONObject) o).get("path").toString());
                territory.setJsonId(Integer.parseInt(((JSONObject) o).get("id").toString()));                           // will use these later to create borders
                territory.setContinent(continent);

                List<Territory> tList = continent.getTerritories();                                                     // add territory to the continent
                tList.add(territory);
                continent.setTerritories(tList);
            }
            continents.add(continent);
        }

        return continentRepository.saveAll(continents);                                                                 // return the persisted entities
    }

    /**
     * Creates border entities for all territories.
     * Called in initialize().
     *
     * @param entities Territories for which borders need to be created
     * @param json     Json territories corresponding to entities
     * @return List of updated territory entities
     * @throws NoSuchElementException Thrown when an element can't be found.
     */
    public List<Territory> createBorders(List<Territory> entities, List<JSONObject> json)
            throws NoSuchElementException {

        for (Territory entity : entities) {
            JSONObject jsonObject = json.stream()                                                                       // get the corresponding json territory
                    .filter(o -> Integer.parseInt(o.get("id").toString()) == entity.getJsonId())
                    .findAny()
                    .orElseThrow(() -> new NoSuchElementException("Could not find corresponding json object"));         // throw exception when the json territory can't be found

            List<Territory> adjacent = new ArrayList<>();                                                               // find adjacent territories using the json id
            for (Object id : (JSONArray) jsonObject.get("neighbors")) {
                int jsonId = Integer.parseInt(String.valueOf(id));
                entities.stream()
                        .filter(t -> t.getJsonId() == jsonId)
                        .findAny()
                        .ifPresent(adjacent::add);
            }

            List<Border> borders = new ArrayList<>();                                                                   // create borders between adjacent territories and the current territory
            for (Territory adjTerritory : adjacent) {
                Border border = new Border();
                border.setAdjacentId(adjTerritory.getId());
                border = borderRepository.save(border);
                border.setTerritory(entity);
                borders.add(border);
            }

            entity.setBorders(borders);
        }

        return territoryRepository.saveAll(entities);                                                                   // return the persisted entities
    }

    /**
     * Assigns players to territories.
     * Called in initialize().
     *
     * @param territories Territories that need to be assigned
     * @param players     Players whom territories need to be assigned to
     * @return List of updated territories which are assigned to a player
     */
    public List<Territory> assignPlayersToTerritories(List<Territory> territories, List<Player> players) {
        List<Territory> assigned = new ArrayList<>();
        for (int i = 0; i <= players.size(); i++) {
            if (i == players.size())
                i = 0;                                                                                                  // go back to the first player

            int random = Utils.getRandom(0, territories.size() - 1);

            Territory t = territories.remove(random);                                                                   // prevent territories to be assigned multiple times
            t.setPlayer(players.get(i));
            t = territoryRepository.save(t);                                                                            // assign territory to the player
            List<Territory> playerTerritories = players.get(i).getTerritories();
            playerTerritories.add(t);
            Player player = players.get(i);
            player.setTerritories(playerTerritories);
            playerRepository.save(player);
            assigned.add(t);

            if (territories.isEmpty())
                break;                                                                                                  // stop when all territories are assigned
        }

        return assigned;
    }

    /**
     * Assigns troops to territories.
     * Called in initialize().
     * <p>
     * Rules:
     * 3 players -> 35 troops each
     * 4 players -> 30 troops each
     * 5 players -> 25 troops each
     * 6 players -> 20 troops each
     *
     * @param territories Territories to which troops need to be assigned
     * @param players     Players that are given troops
     * @return List of updated territories with troops assigned to them
     */
    public List<Territory> assignTroopsToTerritories(List<Territory> territories, List<Player> players) {

        List<Territory> initialized = new ArrayList<>();
        for (Player player : players) {
            player.setTroops(START_MAX_TROOPS - (players.size() * START_FACTOR_TROOPS));                                // give each player troops to assign
            List<Territory> list = territories.stream()
                    .filter(t -> t.getPlayer().getId() == player.getId())
                    .collect(Collectors.toList());

            list.forEach(t -> {                                                                                         // make sure all territories have at least 1 troop assigned
                t.setTroops(1);
                player.setTroops(player.getTroops() - 1);
            });

            int n = player.getTroops();                                                                                 // assign the remaining troops randomly
            for (int i = 0; i < n; i++) {
                int id = Utils.getRandom(0, list.size() - 1);
                list.get(id).setTroops(list.get(id).getTroops() + 1);
                player.setTroops(player.getTroops() - 1);
            }

            for (Territory territory : list) {
                initialized.add(territoryRepository.saveAndFlush(territory));                                           // for some reason, saveAll messes everything up
            }
        }

        return initialized;
    }

    //endregion

    //region ACTIONS

    /**
     * Performs an allocation.
     *
     * @param username Username of the authenticated sender
     * @param dto      Data transfer object with allocation data
     * @return Updated session with new allocation phase
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws InvalidPhaseException       Thrown when the current turn is not in the allocation phase.
     * @throws InvalidOperationException   Thrown then the request is invalid.
     */
    public Session allocate(String username, AllocateDto dto)
            throws EntityNotFoundException, PlayerNotInSessionException, InvalidPhaseException, InvalidOperationException {

        log.info(username + " tries to allocate troops.");

        try {
            Player player = sessionService.getPlayerInSession(username, dto.getSessionId());                            // retrieve the current player
            Turn activeTurn = getActiveTurn(player.getSession());                                                       // retrieve the active turn
            Phase activePhase = getActivePhase(activeTurn);                                                             // retrieve the active phase

            if (!activePhase.getPhaseType().equals(PhaseType.ALLOCATION))
                throw new InvalidPhaseException(username + " is currently not in the ALLOCATION phase.");               // throw exception when the current turn is not in the allocation phase

            Territory territory = player.getTerritories()                                                               // retrieve the territory
                    .stream()
                    .filter(t -> t.getId() == dto.getTerritoryId())
                    .findAny()
                    .orElseThrow(() -> new InvalidOperationException(                                                   // throw exception when the territory is not owned by the player
                            "Can't send troops to this territory as it is not occupied by " + username));

            if (dto.getTroops() > player.getTroops())
                throw new InvalidOperationException("Insufficient amount of troops.");                                  // throw exception when the player does not have enough troops

            Allocation allocation = new Allocation();                                                                   // perform the allocation
            allocation.setPhase(activePhase);
            allocation.setPlayer(player.getUsername());
            allocation.setTerritory(territory.getName());
            allocation.setTroops(dto.getTroops());
            allocation = actionRepository.save(allocation);
            activePhase.getActions().add(allocation);
            phaseRepository.save(activePhase);

            player.setTroops(player.getTroops() - dto.getTroops());
            territory.setTroops(territory.getTroops() + dto.getTroops());
            sessionService.savePlayer(player);
            territoryRepository.save(territory);

            Session session = sessionService.getSession(dto.getSessionId());                                            // return the updated session

            log.info("ALLOCATION phase is successfully finished.");

            return session;

        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }
    }

    /**
     * Performs an attack.
     * Note: only 'BLITZ' is implemented (roll until either is out of troops)
     *
     * @param username Username of the authenticated sender
     * @param dto      Data transfer object with attack data
     * @return Updated session with new attack phase
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidPhaseException       Thrown when the current turn is not in the attack phase.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws InvalidOperationException   Thrown then the request is invalid.
     */
    public Session attack(String username, AttackDto dto)
            throws EntityNotFoundException, InvalidPhaseException, PlayerNotInSessionException, InvalidOperationException {

        log.info(username + " tries to attack.");

        try {
            Player attackingPlayer = sessionService.getPlayerInSession(username, dto.getSessionId());                   // retrieve the current player
            Turn activeTurn = getActiveTurn(attackingPlayer.getSession());                                              // retrieve the active turn
            Phase activePhase = getActivePhase(activeTurn);                                                             // retrieve the active phase

            Territory attackingTerritory = attackingPlayer.getTerritories()                                             // retrieve the attacking territory
                    .stream()
                    .filter(t -> t.getId() == dto.getAttackingTerritoryId())
                    .findAny()
                    .orElseThrow(() -> new InvalidOperationException(                                                   // throw exception when the territory is not owned by this player
                            "Can't send troops from this territory as it is not occupied by " + username));

            if (attackingTerritory.getTroops() < 2)
                throw new InvalidOperationException("Insufficient troops.");                                            // throw exception when the territory does not have enough troops to attack

            Territory defendingTerritory = territoryRepository.findById(dto.getDefendingTerritoryId())                  // retrieve the defending territory
                    .orElseThrow(() -> new EntityNotFoundException("Could not find territory."));                       // throw exception when the territory can't be found

            if (attackingPlayer.getTerritories().contains(defendingTerritory))
                throw new InvalidOperationException("Can't attack your own territory.");                                // throw exception when the player tries to attack his own territory

            if (activePhase.getPhaseType().equals(PhaseType.ATTACK)) {
                Attack attack = new Attack();                                                                           // create a new attack
                attack.setPhase(activePhase);

                attack.setAttacker(attackingTerritory.getPlayer().getUsername());                                       // set process data at start
                attack.setAttackingTerritory(attackingTerritory.getName());
                attack.setAttackingTroopsAtStart(attackingTerritory.getTroops());
                attack.setDefender(defendingTerritory.getPlayer().getUsername());
                attack.setDefendingTerritory(defendingTerritory.getName());
                attack.setDefendingTroopsAtStart(defendingTerritory.getTroops());

                for (int i = 0; i < attackingTerritory.getTroops() + defendingTerritory.getTroops(); i++) {             // start rolling the dice
                    List<List<Integer>> diceRolls = getDiceRolls(attackingTerritory.getTroops(), defendingTerritory.getTroops());
                    List<Integer> attackingDice = diceRolls.get(0);
                    List<Integer> defendingDice = diceRolls.get(1);

                    if (Math.min(attackingDice.size(), defendingDice.size()) == 2) {
                        for (int k = 0; k < 2; k++) {
                            if (attackingDice.get(k) > defendingDice.get(k))
                                defendingTerritory.setTroops(defendingTerritory.getTroops() - 1);                       // take a troop away from the loser
                            else
                                attackingTerritory.setTroops(attackingTerritory.getTroops() - 1);                       // attacker also loses on a draw

                            if (attackingTerritory.getTroops() < 2 || defendingTerritory.getTroops() == 0)              // Stop rolling when attacker or defender can't continue
                                break;
                        }
                    } else {
                        if (attackingDice.get(0) > defendingDice.get(0))
                            defendingTerritory.setTroops(defendingTerritory.getTroops() - 1);                           // take a troop away from the loser
                        else
                            attackingTerritory.setTroops(attackingTerritory.getTroops() - 1);                           // attacker also loses on a draw
                    }

                    if (attackingTerritory.getTroops() < 2 || defendingTerritory.getTroops() == 0)                      // Stop rolling when attacker or defender can't continue
                        break;
                }

                attack.setAttackingTroopsAtEnd(attackingTerritory.getTroops());                                         // set process data at end
                attack.setDefendingTroopsAtEnd(defendingTerritory.getTroops());
                attack.setAttackSuccessful(attackingTerritory.getTroops() >= 2 && defendingTerritory.getTroops() == 0); // figure out who won

                attack = actionRepository.save(attack);                                                                 // persist the attack
                List<Action> actions = activePhase.getActions();
                actions.add(attack);
                activePhase.setActions(actions);
                phaseRepository.save(activePhase);
                Player defender = defendingTerritory.getPlayer();

                if (attack.isAttackSuccessful()) {                                                                      // take over the defending territory when the attacker won
                    defendingTerritory.setPlayer(attackingTerritory.getPlayer());
                    defendingTerritory.setTroops(attackingTerritory.getTroops() - 1);
                    attackingTerritory.setTroops(1);
                }

                territoryRepository.save(attackingTerritory);
                Territory t2 = territoryRepository.save(defendingTerritory);

                if (attack.isAttackSuccessful()) {
                    defender.getTerritories().remove(defendingTerritory);
                    attackingPlayer.getTerritories().add(t2);
                    playerRepository.save(defender);
                    playerRepository.save(attackingPlayer);
                }
            } else
                throw new InvalidPhaseException("Player is currently not in the ATTACK phase");                         // throw exception when the current turn is not in the attack phase

            Session session = sessionService.getSession(dto.getSessionId());

            if (isGameFinished(session)) {
                log.info("Game is finished.");
                return end(session);                                                                                    // end session when finished
            }

            log.info("ATTACK phase is successfully finished.");

            return session;                                                                                             // return the updated session

        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }
    }

    /**
     * Gets dice rolls for attacking and defending.
     *
     * @param attackingTroops Troops count of attacking territory
     * @param defendingTroops Troops count of defending territory
     * @return List of two lists of dice rolls for the attacking and defending territory
     */
    private List<List<Integer>> getDiceRolls(int attackingTroops, int defendingTroops) {
        List<List<Integer>> diceRolls = new ArrayList<>();
        List<Integer> attackingDice = Utils.rollDice(attackingTroops - 1, MAX_ATTACKING_DICE);
        List<Integer> defendingDice = Utils.rollDice(defendingTroops, MAX_DEFENDING_DICE);

        attackingDice.sort(Comparator.reverseOrder());
        defendingDice.sort(Comparator.reverseOrder());

        diceRolls.add(attackingDice);
        diceRolls.add(defendingDice);

        return diceRolls;
    }

    /**
     * Ends a session.
     *
     * @param session Session in which the game is played
     * @return Updated session of which the game is finished
     */
    public Session end(Session session) {
        session.setStatus(Status.FINISHED);
        session.setWinner(session.getPlayers().stream()
                .filter(p -> !p.getTerritories().isEmpty())
                .findAny()
                .map(Player::getUsername).get());

        return sessionService.saveSession(session);
    }

    /**
     * Performs a fortification.
     * Uses the BFS algorithm to decide if the fortifying troops can reach the territory.
     *
     * @param username Username of the authenticated sender
     * @param dto      Data transfer object
     * @return Updated session with new fortification phase
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidPhaseException       Thrown when the current turn is not in the attack phase.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws InvalidOperationException   Thrown then the request is invalid.
     */
    public Session fortify(String username, FortifyDto dto)
            throws PlayerNotInSessionException, EntityNotFoundException, InvalidPhaseException, InvalidOperationException {

        log.info(username + " tries to fortify a territory.");

        try {
            Player player = sessionService.getPlayerInSession(username, dto.getSessionId());                            // retrieve the current player
            Turn activeTurn = getActiveTurn(player.getSession());                                                       // retrieve the active turn
            Phase activePhase = getActivePhase(activeTurn);                                                             // retrieve the active phase

            if (!activePhase.getPhaseType().equals(PhaseType.FORTIFICATION))
                throw new InvalidPhaseException(username + " is currently not in the FORTIFICATION phase.");            // throw exception when the current turn is not in the fortification phase
            else if (activePhase.getActions().size() > 1)
                throw new InvalidOperationException(username + " has already fortified a territory.");                  // throw exception when the player has already performed a fortification

            Territory fromTerritory = player.getTerritories()                                                           // retrieve the territory where the player is fortifying from
                    .stream()
                    .filter(t -> t.getId() == dto.getFromTerritoryId())
                    .findAny()
                    .orElseThrow(() -> new InvalidOperationException(
                            "Can't send troops from this territory as it is not occupied by " + username));             // throw exception when the territory is not owned by the player

            Territory toTerritory = player.getTerritories()                                                             // retrieve the territory the player is fortifying
                    .stream()
                    .filter(t -> t.getId() == dto.getToTerritoryId())
                    .findAny()
                    .orElseThrow(() -> new InvalidOperationException(
                            "Can't send troops to this territory as it is not occupied by " + username));               // throw exception when the territory is not owned by the player

            if (fromTerritory.getTroops() <= dto.getTroops())
                throw new InvalidOperationException(                                                                    // throw exception when the territory does not have enough troops
                        "Can't send " + dto.getTroops()
                                + " troops as this territory has " + fromTerritory.getTroops()
                                + " troops and there needs to remain at least 1 troop behind");

            if (!this.canReachTerritoryToFortify(dto.getFromTerritoryId(), dto.getToTerritoryId()))
                throw new InvalidOperationException("Unable to reach this territory");                                  // throw exception when the territory can't be reached

            Fortification fortification = new Fortification();
            fortification.setPhase(activePhase);
            fortification.setPlayer(player.getUsername());
            fortification.setFortifyingTerritory(fromTerritory.getName());
            fortification.setFortifiedTerritory(toTerritory.getName());
            fortification.setTroops(dto.getTroops());
            fortification = actionRepository.save(fortification);
            activePhase.getActions().add(fortification);
            phaseRepository.save(activePhase);

            fromTerritory.setTroops(fromTerritory.getTroops() - dto.getTroops());                                       // perform the fortification
            toTerritory.setTroops(toTerritory.getTroops() + dto.getTroops());
            ArrayList<Territory> toUpdate = new ArrayList<>();
            toUpdate.add(fromTerritory);
            toUpdate.add(toTerritory);
            territoryRepository.saveAll(toUpdate);

            Session session = sessionService.getSession(dto.getSessionId());

            log.info("FORTIFICATION phase is successfully finished.");

            return session;                                                                                             // return the updated session

        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }
    }

    /**
     * Checks if a territory can be reached.
     *
     * @param departureId   Id of the territory we're departing from
     * @param destinationId Id of the territory we're trying to reach
     * @return True if the destination can be reached from the departing territory;
     * False otherwise
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public boolean canReachTerritoryToFortify(long departureId, long destinationId)
            throws EntityNotFoundException {

        Node departure = this.createBFSNode(departureId);                                                               // create the BFS structure
        return Node.search(destinationId, departure).isPresent();                                                       // search for a path and return if possible
    }

    /**
     * Creates a BFS graph structure.
     * Source: https://www.baeldung.com/java-breadth-first-search
     *
     * @param territoryId Id of the territory we're departing from
     * @return Node with connections
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    private Node createBFSNode(long territoryId)
            throws EntityNotFoundException {

        Territory territory = territoryRepository.findById(territoryId)
                .orElseThrow(() -> new EntityNotFoundException("Can't find territory"));                                // throw exception when the territory can't be found

        Player player = territory.getPlayer();

        List<Territory> territories = territoryRepository
                .findAllByPlayerUsernameAndPlayerSessionId(player.getUsername(), player.getSession().getId())
                .orElseThrow(() -> new EntityNotFoundException("Can't find territories"));                              // throw exception when the territories can't be found

        List<Territory> notConnected = new ArrayList<>(territories);
        List<Node> graph = new ArrayList<>();
        Node node;
        Node toConnect;

        while (!notConnected.isEmpty()) {
            for (Territory t : territories) {
                if (graph.stream().noneMatch(n -> n.getTerritory().equals(t)))
                    graph.add(new Node(t));                                                                             // add the node if it doesn't exist yet
                node = graph.stream().filter(n -> n.getTerritory().equals(t)).findAny()
                        .orElseThrow(() -> new EntityNotFoundException("Can't find node"));                             // throw exception when the node can't be found

                for (Territory t2 : territories) {                                                                      // connect the adjacent territories
                    if (graph.stream().noneMatch(n -> n.getTerritory().equals(t2)))
                        graph.add(new Node(t2));                                                                        // add the node if it doesn't exist yet

                    if (t.getBorders().stream().anyMatch(b -> b.getAdjacentId() == t2.getId())) {                       // t2 has a border with t, connect them
                        toConnect = graph.stream().filter(n -> n.getTerritory().equals(t2)).findAny()
                                .orElseThrow(() -> new EntityNotFoundException("Can't find node to connect with"));     // throw exception when the node can't be found

                        node.connect(toConnect);
                    }
                }

                notConnected.remove(t);
            }
        }

        return graph.stream().filter(n -> n.getTerritory().getId() == territoryId).findAny().orElse(null);
    }

    //endregion

    /**
     * Starts the next phase.
     *
     * @param session  Session
     * @param username Username of the authenticated sender
     * @return Updated session
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws PlayerNotInSessionException Thrown when the player is not a participant.
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws NotPlayerTurnException      Thrown when it isn't the player's turn.
     */
    public Session nextPhase(Session session, String username)
            throws EntityNotFoundException, PlayerNotInSessionException, InvalidPhaseException, IOException, ParseException, NotPlayerTurnException {

        Player player = sessionService.getPlayerInSession(username, session.getId());                                   // retrieve the player

        if (session.getPlayers().stream().anyMatch(p -> p.getUsername().equals(player.getUsername()))) {
            Turn activeTurn = getActiveTurn(session);

            if (!activeTurn.getPlayer().equals(player))
                throw new NotPlayerTurnException("It is not the players turn.");                   // throw exception when this player is not the active one

            Phase activePhase = getActivePhase(activeTurn);

            switch (activePhase.getPhaseType()) {
                case ALLOCATION:
                    log.info(username + " changes from ALLOCATION to ATTACK phase.");
                    return createPhase(session, PhaseType.ATTACK);                                                      // create a new attack phase
                case ATTACK:
                    log.info(username + " changes from ATTACK to FORTIFICATION phase.");
                    return createPhase(session, PhaseType.FORTIFICATION);                                               // create a new fortification phase
                case FORTIFICATION:
                    log.info(username + " ends turn.");
                    return nextTurn(session);                                                                           // start a new turn
                default:
                    throw new InvalidPhaseException("Phase is invalid.");                                               // throw exception when the phase is invalid
            }
        } else
            throw new PlayerNotInSessionException(                                                                      // throw exception when the player is not a participant
                    player.getUsername() + " is not active in session: " + session.getId());
    }

    /**
     * Creates a new phase.
     *
     * @param session Session to create a new phase in
     * @param type    Type of the phase to be created
     * @return Updated session with a new phase
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public Session createPhase(Session session, PhaseType type)
            throws EntityNotFoundException {

        Turn turn = getActiveTurn(session);                                                                             // retrieve the active turn
        Phase phase = new Phase();                                                                                      // create the new phase
        phase.setPhaseType(type);
        phase.setTurn(turn);
        StartPhaseAction action = new StartPhaseAction();
        action.setPhasetype(phase.getPhaseType());
        action.setPlayer(turn.getPlayer().getUsername());
        action.setPhase(phase);
        actionRepository.save(action);
        phase = phaseRepository.save(phase);
        turn.getPhases().add(phase);
        turnRepository.save(turn);
        session = sessionService.getSession(session.getId());

        return session;                                                                                                 // return the updated session
    }

    /**
     * Starts the next turn.
     *
     * @param session Session to start the next turn in.
     * @return Updated session with a new turn
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws IOException             Thrown when an I/O error occurs.
     * @throws ParseException          Thrown when the state can't be parsed.
     */
    public Session nextTurn(Session session)
            throws EntityNotFoundException, IOException, ParseException {

        Player player;

        session = sessionService.getSession(session.getId());
        if (session.getTurns().isEmpty()) {
            player = session.getPlayers().get(0);
        } else {
            Turn previousTurn = getActiveTurn(session);
            previousTurn.setEndTime(Timestamp.valueOf(LocalDateTime.now()));
            turnRepository.save(previousTurn);
            Player previousPlayer = previousTurn.getPlayer();

            // Turns can never end at a losing player -> previousTurn always has a valid player (maybe even the winner)
            // Try to find the next player with territories left
            // Start with getting all players in that session who can still play
            List<Player> players = session.getPlayers()
                    .stream()
                    .filter(p -> p.getTerritories().size() > 0)
                    .collect(Collectors.toList());

            if (players.size() > 1) {
                // Game is still going on, time to find the next player in line
                // To do that, we need to find the index of the previous player
                int index = players.indexOf(previousPlayer);
                if (index == players.size() - 1)
                    player = players.get(0);                                                                            // if the previous player was the last on in line, take the first one
                else
                    player = players.get(++index);                                                                     // if not the last in line, take the next one
            } else {
                return end(session);                                                                                    // if the list only has 1 player left, that guy won (or girl.. but hey be realistic)
            }
        }

        player.setTroops(calculateTroopsForAllocation(player));
        List<Phase> phases = new ArrayList<>();
        Turn turn = new Turn(Timestamp.valueOf(LocalDateTime.now()), player, session, phases);
        turn = turnRepository.save(turn);
        session.getTurns().add(turn);
        sessionService.saveSession(session);
        playerRepository.save(player);

        if (session.getGameType().equals(GameType.HUMAN)) {
            if (!player.getPlayerType().equals(PlayerType.HUMAN)) {
                session = createPhase(session, PhaseType.ALLOCATION);
                Session finalSession = session;
                executor.submit(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        publisher.publishEvent(new CpuTurnEvent(this, finalSession, player.getUsername()));
                    } catch (Exception e) {
                        log.error(e.toString() + ": " + e.getMessage());
                    }
                });

                return session;
            }
        }

        return createPhase(session, PhaseType.ALLOCATION);
    }

    /**
     * Calculates how many troops a player can allocate.
     *
     * @param player player
     * @return amount of troops
     * @throws IOException    Thrown when an I/O error occurs.
     * @throws ParseException Thrown when the state can't be parsed.
     */
    public int calculateTroopsForAllocation(Player player)
            throws IOException, ParseException {

        int troops;
        if (player.getTerritories().size() < 9)
            troops = ALLOCATE_BASE_TROOPS;                                                                              // give at least 3 troops
        else troops = (int) Math.floor(player.getTerritories().size() / 3);

        JSONObject json = Utils.readMap(player.getSession().getMap());                                                  // retrieve map data
        for (Object continent : (JSONArray) requireNonNull(json).get("continents")) {
            Object territories = ((JSONObject) continent).get("countries");
            if (player.getTerritories().stream()
                    .filter(t -> t.getContinent().getName().equals(((JSONObject) continent).get("name")))
                    .count() == ((JSONArray) territories).size()) {
                troops += Integer.parseInt(((JSONObject) continent).get("bonus").toString());                           // add the continental bonus if player owns all its territories
            }
        }
        return troops;
    }

    /**
     * Retrieves the currently active turn of a session.
     *
     * @param session Session in of which the active turn needs to be retrieved
     * @return Active turn
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public Turn getActiveTurn(Session session)
            throws EntityNotFoundException {

        return turnRepository.findFirstBySessionIdOrderByIdDesc(session.getId())
                .orElseThrow(() -> new EntityNotFoundException("Could not find the active turn."));
    }

    /**
     * Retrieve the currently active phase of a session.
     *
     * @param activeTurn Turn that is currently active
     * @return Active phase of turn
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    Phase getActivePhase(Turn activeTurn)
            throws EntityNotFoundException {

        return activeTurn.getPhases().stream().max(Comparator.comparingLong(Phase::getId))
                .orElseThrow(() -> new EntityNotFoundException("Could not find the active phase."));
    }

    /**
     * Checks if a game is finished.
     *
     * @param session Session in which the game is played
     * @return True if only one player in the sessions still has territories;
     * False otherwise
     */
    public boolean isGameFinished(Session session) {

        int playersWithTerritories = 0;

        for (Player player : session.getPlayers()) {
            if (!player.getTerritories().isEmpty())
                playersWithTerritories++;
        }

        return playersWithTerritories == 1;
    }
}
