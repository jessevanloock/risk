package com.error402.server.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * Handles communication with authorization microservice.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@Component
public class CustomRemoteTokenService implements ResourceServerTokenServices {

    @Value("${auth_url}")
    private String AUTH_URL;

    @Autowired
    private RestTemplate restTemplate;

    private AccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();

    /**
     * Loads authentication from token.
     *
     * @param accessToken Token to load authentication from
     * @return Authentication that is loaded from given token
     * @throws AuthenticationException Thrown when authentication is invalid.
     * @throws InvalidTokenException   Thrown when access token is invalid.
     */
    @Override
    public OAuth2Authentication loadAuthentication(String accessToken)
            throws AuthenticationException, InvalidTokenException {

        HttpHeaders headers = new HttpHeaders();
        Map<String, Object> map = executeGet(AUTH_URL + "/oauth/check_token?token=" + accessToken, headers);

        if (map == null || map.isEmpty())
            throw new InvalidTokenException("Invalid Token.");

        return accessTokenConverter.extractAuthentication(map);
    }

    /**
     * Gets map authentication data from authorization.
     *
     * @param path    Path to call with GET
     * @param headers Headers headers to pass with GET
     * @return Map with authentication data
     */
    private Map<String, Object> executeGet(String path, HttpHeaders headers) {

        try {
            if (headers.getContentType() == null)
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            @SuppressWarnings("rawTypes")
            Map map = restTemplate.exchange(path, HttpMethod.POST,
                    new HttpEntity<MultiValueMap<String, String>>(null, headers), Map.class).getBody();

            @SuppressWarnings("unchecked")
            Map<String, Object> result = map;

            return result;

        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
        }

        return null;
    }

    /**
     * Reads the access token. (Note: this method is not supported)
     *
     * @param accessToken Token that needs to be read
     * @return Token that is read
     * @throws UnsupportedOperationException Thrown when an operation is not supported.
     */
    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }
}
