package com.error402.server.service;

import com.error402.server.dao.FriendshipRepository;
import com.error402.server.dao.UserRepository;
import com.error402.server.exceptions.AlreadyFriendsException;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.InvalidFriendshipException;
import com.error402.server.model.FriendRequestType;
import com.error402.server.model.Friendship;
import com.error402.server.model.RiskUser;
import com.error402.server.model.dto.FriendshipDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles all specific user logic.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
@Service
public class UserService {

    //region INJECTED PROPERTIES

    private final UserRepository userRepository;
    private final FriendshipRepository friendshipRepository;

    //endregion

    // region USER

    /**
     * Creates a user if he doesn't already exist.
     *
     * @param username Username of the user that is created if he doesn't already exist
     */
    public void createUserIfNotExists(String username) {

        if (userRepository.findByUsername(username).isEmpty())
            saveUser(new RiskUser(username));
    }

    /**
     * Saves a user.
     *
     * @param user User to be saved
     * @return User that is saved
     */
    public RiskUser saveUser(RiskUser user) {

        return userRepository.save(user);
    }

    /**
     * Checks if a user with the given username already exists.
     *
     * @param username Username to check
     * @return True if a user with the give username already exists;
     *         False otherwise
     */
    public boolean checkIfUserExists(String username) {

        return userRepository.findByUsername(username).isPresent();
    }

    /**
     * Retrieves a user.
     *
     * @param username Username of user that needs to be retrieved
     * @return User with the given username
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public RiskUser getUser(String username)
            throws EntityNotFoundException {

        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("Could not find user with username " + username));
    }

    //endregion

    //region FRIENDS

    /**
     * Retrieves a friendship.
     *
     * @param friendshipId Id of the friendship that needs to be retrieved
     * @return Friendship with the given id
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public Friendship getFriendship(long friendshipId)
            throws EntityNotFoundException {

        return friendshipRepository.findById(friendshipId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find friend request."));
    }

    /**
     * Retrieves all friend requests of a user.
     *
     * @param username Username of the authenticated user whose friend requests need to be retrieved
     * @return List of data transfer objects that contain friend request data
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<FriendshipDto> getFriendRequests(String username)
            throws EntityNotFoundException {

        RiskUser user = getUser(username);

        List<FriendshipDto> friendRequests = new ArrayList<>();

        friendRequests.addAll(getSentFriendRequests(user));
        friendRequests.addAll(getReceivedFriendRequests(user));

        return friendRequests;
    }

    /**
     * Retrieves sent friend request of a user.
     *
     * @param user User whose sent friend requests need to be retrieved
     * @return List of data transfer objects that contain friend request data
     */
    private List<FriendshipDto> getSentFriendRequests(RiskUser user) {

        return user.getSentFriendships()
                .stream()
                .map(f -> new FriendshipDto(f.getId(), f.getReceiver().getUsername(), f.isConfirmed(), FriendRequestType.SENT))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves received friend request of a user.
     *
     * @param user User whose received friend requests need to be retrieved
     * @return List of data transfer objects that contain friend request data
     */
    private List<FriendshipDto> getReceivedFriendRequests(RiskUser user) {

        return user.getReceivedFriendships()
                .stream()
                .map(f -> new FriendshipDto(f.getId(), f.getSender().getUsername(), f.isConfirmed(), FriendRequestType.RECEIVED))
                .collect(Collectors.toList());
    }

    /**
     * Sends a friend request.
     *
     * @param senderName   Username of the sender of the friend request
     * @param receiverName Username of the receiver of the friend request
     * @return Data transfer object that contains friend request data
     * @throws AlreadyFriendsException Thrown when a friendship already exists.
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public FriendshipDto sendFriendRequest(String senderName, String receiverName)
            throws AlreadyFriendsException, EntityNotFoundException {

        RiskUser sender = getUser(senderName);
        RiskUser receiver = getUser(receiverName);

        if (sender.getSentFriendships()
                .stream()
                .map(Friendship::getReceiver)
                .map(RiskUser::getUsername)
                .collect(Collectors.toList())
                .contains(receiverName))
            throw new AlreadyFriendsException("You've already sent " + receiverName + " a friend request.");

        if (sender.getReceivedFriendships()
                .stream()
                .map(Friendship::getSender)
                .map(RiskUser::getUsername)
                .collect(Collectors.toList())
                .contains(receiverName))
            throw new AlreadyFriendsException(receiverName + " has already send you a friend request.");

        Friendship friendship = friendshipRepository.save(new Friendship(sender, receiver));
        sender.getSentFriendships().add(friendship);
        receiver.getReceivedFriendships().add(friendship);
        userRepository.save(sender);
        userRepository.save(receiver);

        log.info(senderName + " successfully send " + receiverName + " a friend request.");

        return new FriendshipDto(
                friendship.getId(),
                friendship.getReceiver().getUsername(),
                friendship.isConfirmed(),
                FriendRequestType.SENT);
    }

    /**
     * Confirms a sent friend request.
     *
     * @param username     Username of the authenticated user that tries to confirm a friend request
     * @param friendshipId Id of the friendship that needs to be confirmed
     * @throws InvalidFriendshipException Thrown when a friendship action can't be executed.
     * @throws EntityNotFoundException    Thrown when a required entity can't be found.
     */
    public void confirmFriendship(String username, long friendshipId)
            throws InvalidFriendshipException, EntityNotFoundException {

        Friendship friendshipToConfirm = getFriendship(friendshipId);

        if (!username.equals(friendshipToConfirm.getReceiver().getUsername()))
            throw new InvalidFriendshipException("You cannot confirm this friendship.");

        friendshipToConfirm.setConfirmed(true);
        friendshipRepository.save(friendshipToConfirm);

        log.info(username + " successfully confirmed friendship with " + friendshipToConfirm.getReceiver().getUsername());
    }

    /**
     * Deletes a friendship.
     *
     * @param username     Username of the authenticated user that tries to delete a friendship
     * @param friendshipId Id of the friendship that needs to be deleted
     * @throws InvalidFriendshipException Thrown when a friendship action can't be executed.
     * @throws EntityNotFoundException    Thrown when a required entity can't be found.
     */
    public void deleteFriendship(String username, long friendshipId) throws InvalidFriendshipException, EntityNotFoundException {
        Friendship friendshipToDelete = getFriendship(friendshipId);

        if (!username.equals(friendshipToDelete.getReceiver().getUsername()) &&
                !username.equals(friendshipToDelete.getSender().getUsername()))
            throw new InvalidFriendshipException("You cannot delete this friendship.");

        friendshipRepository.delete(friendshipToDelete);

        log.info(username + " successfully deleted friendship.");
    }

    //endregion
}
