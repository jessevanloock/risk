package com.error402.server.service;

import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.NoPlayersInSessionException;
import com.error402.server.model.Player;
import com.error402.server.model.PlayerType;
import com.error402.server.model.Session;
import com.error402.server.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Handles the communication to the websocket for live updates.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class NotifierService {
    private final SimpMessageSendingOperations simpTemplate;

    /**
     * Notifies all players in a session of an update.
     *
     * @param players Players needed to be notified
     * @param topic   Topic to send to
     * @param session Session with updated data of which the players need to be notified
     * @throws ParseException              Thrown when a parse error occurs.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     */
    public void notify(List<Player> players, String topic, Session session)
            throws ParseException, NoPlayersInSessionException, IOException, EntityNotFoundException {

        JSONObject json = Utils.convertSessionToJson(session);
        players.stream()
                .filter(p -> p.getPlayerType().equals(PlayerType.HUMAN))
                .forEach(player -> {
                    simpTemplate.convertAndSendToUser(player.getUsername(), topic, json);                               // send the session state to all players in that session
                    log.info(player.getUsername() + " notified on " + topic);
                });
    }
}
