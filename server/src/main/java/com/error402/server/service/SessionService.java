package com.error402.server.service;

import com.error402.server.dao.InviteRepository;
import com.error402.server.dao.PlayerRepository;
import com.error402.server.dao.SessionRepository;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.dto.JoinCpuDto;
import com.error402.server.model.dto.PlayerColorDto;
import com.error402.server.model.dto.SessionInviteDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Handles all outside-session logic.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
@Service
public class SessionService {

    @Value("${game.players.max}")
    private int MAX_PLAYERS;

    //region INJECTED PROPERTIES

    private final PlayerRepository playerRepository;
    private final SessionRepository sessionRepository;
    private final InviteRepository inviteRepository;
    private final UserService userService;

    //endregion

    //region SESSION

    /**
     * Retrieves a session.
     *
     * @param sessionId Id of session that needs to be retrieved
     * @return Session that is retrieved
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public Session getSession(long sessionId)
            throws EntityNotFoundException {

        return sessionRepository.findById(sessionId)                                                                    // retrieve the session
                .orElseThrow(() -> new EntityNotFoundException("Could not find session."));                             // throw exception when session not found
    }

    /**
     * Retrieves sessions by game type.
     * @param gameType GameType of the sessions that need to be retrieved
     * @return List of sessions of the given game type
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<Session> getSessionsByGameType(GameType gameType)
            throws EntityNotFoundException {

        return sessionRepository.findAllByGameType(gameType)                                                            // retrieve sessions
                .orElseThrow(() -> new EntityNotFoundException("Could not find sessions."));                            // throw exception when no sessions are found
    }

    /**
     * Retrieves a session in LOBBY state.
     *
     * @param username  Username of the authenticated sender
     * @param sessionId Id of the session that needs to be retrieved
     * @return Session in the LOBBY state with the given id
     * @throws EntityNotFoundException   Thrown when a required entity can't be found.
     * @throws InvalidOperationException Thrown when operation can't be executed.
     */
    public Session getSessionLobby(String username, long sessionId)
            throws EntityNotFoundException, InvalidOperationException {

        Session session = getSession(sessionId);                                                                        // retrieve the session

        if (session.getPlayers().stream().noneMatch(p -> p.getUsername().equals(username)))
            throw new InvalidOperationException("User is not a part of this session.");                                 // throw exception when user is not in session

        if (session.getStatus().equals(Status.LOBBY))
            return session;
        else
            throw new InvalidOperationException("This lobby does not exist anymore.");                                  // throw exception when session is not in lobby
    }

    /**
     * Saves a session.
     *
     * @param session Session that needs to be saved
     * @return Saved session
     */
    public Session saveSession(Session session) {

        return sessionRepository.save(session);
    }

    /**
     * Creates a session.
     *
     * @param username Username of the host
     * @return Created session
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public Session createSession(String username)
            throws EntityNotFoundException {

        log.info(username + " tries to create a new session.");

        RiskUser user = userService.getUser(username);                                                                  // retrieve the user
        Player player = playerRepository.save(new Player(user.getUsername(), PlayerType.HUMAN));                        // create a new player

        Session session = new Session(GameType.HUMAN);                                                                  // create a new session
        session.setStatus(Status.LOBBY);
        session.setHost(username);
        session.setCreated(Timestamp.valueOf(LocalDateTime.now()));
        session.getPlayers().add(player);
        session = sessionRepository.save(session);

        player.setSession(session);                                                                                     // add the player to the session
        player.setRiskUser(user);
        player = playerRepository.save(player);

        user.getPlayers().add(player);
        userService.saveUser(user);

        log.info("Successfully created a new session: " + session.getId());

        return player.getSession();                                                                                     // return the updated session
    }

    /**
     * Retrieves all executed actions of a session.
     *
     * @param sessionId Id of the session in which the actions were executed
     * @return List of actions that were executed in session with given id
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<Action> getSessionLog(long sessionId)
            throws EntityNotFoundException {

        Session session = getSession(sessionId);                                                                        // retrieve the session

        List<Action> actions = new ArrayList<>();
        for (Turn turn : session.getTurns().stream()
                .sorted(Comparator.comparing(Turn::getStartTime))
                .collect(Collectors.toList())) {
            for (Phase phase : turn.getPhases().stream()
                    .sorted(Comparator.comparing(Phase::getPhaseType))
                    .collect(Collectors.toList())) {
                actions.addAll(phase.getActions().stream()
                        .sorted(Comparator.comparing(Action::getId))
                        .collect(Collectors.toList()));                                                                     // add actions to the list
            }
        }

        return actions;                                                                                                 // return the actions
    }

    /**
     * Retrieves sessions of a user by their status.
     *
     * @param username Username of the user whom the sessions need to be retrieved for
     * @param status   Status which the session should have
     * @return List of sessions of the given player with the given status
     */
    public List<Session> getSessionsOfPlayerByStatus(String username, Status status) {
        List<Player> players = playerRepository.findAllByUsernameAndSession_Status(username, status).orElse(null);

        return players != null ? players.stream().map(Player::getSession).collect(Collectors.toList()) : null;
    }

    //region DISCARD

    /**
     * Deletes a session when still in Lobby.
     *
     * @param username  Username of the authenticated user who wants to discard a session
     * @param sessionId Id of the session that is in the LOBBY state
     * @throws PlayerNotInSessionException  Thrown when the authenticated user is not a participant.
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     */
    public void discard(String username, long sessionId)
            throws PlayerNotInSessionException, InvalidOperationException {

        Player player = playerRepository.findByUsernameAndSessionId(username, sessionId)                                // retrieve player info
                .orElseThrow(() -> new PlayerNotInSessionException("Player is not in session."));                       // throw exception if not found

        if (!player.getSession().getStatus().equals(Status.LOBBY))
            throw new InvalidOperationException("Session is not in lobby.");

        if (!player.getUsername().equals(player.getSession().getHost()))
            throw new InvalidOperationException("Player is not the host.");

        sessionRepository.delete(player.getSession());                                                                  // delete session

        log.info("Successfully deleted session: " + sessionId);
    }

    /**
     * Deletes a player from a session.
     *
     * @param username  Username of the authenticated user who wants to leave a session
     * @param sessionId Id of the session the user wants to leave
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     * @throws PlayerNotInSessionException  Thrown when the authenticated user is not a participant.
     */
    public void leave(String username, long sessionId)
            throws InvalidOperationException, PlayerNotInSessionException {

        Player player = playerRepository.findByUsernameAndSessionId(username, sessionId)                                // retrieve the player
                .orElseThrow(() -> new PlayerNotInSessionException("Player is not in session."));

        if (!player.getSession().getStatus().equals(Status.LOBBY))
            throw new InvalidOperationException("Session is not in lobby.");                                            // throw exception when session isn't in LOBBY state

        if (player.getUsername().equals(player.getSession().getHost()))
            throw new InvalidOperationException("Player is the host.");                                                 // throw exception when player isn't host

        playerRepository.delete(player);

        log.info(username + " successfully left lobby of session: " + sessionId);
    }

    /**
     * Kicks another player from a session.
     *
     * @param username     Username of the player who tries to kick an other player
     * @param sessionId    Id of the session from which a player is tried to be kicked
     * @param kickUsername Username of the player who is kicked
     * @throws PlayerNotInSessionException  Thrown when the authenticated sender is not a participant.
     * @throws InvalidOperationException    Thrown when an operation can't be executed.
     */
    public void kick(String username, long sessionId, String kickUsername)
            throws PlayerNotInSessionException, InvalidOperationException {

        Player player = playerRepository.findByUsernameAndSessionId(username, sessionId)                                // retrieve the player
                .orElseThrow(() -> new PlayerNotInSessionException("Player is not in session."));

        if (!player.getSession().getStatus().equals(Status.LOBBY))
            throw new InvalidOperationException("Session is not in lobby.");                                            // throw exception when session isn't in LOBBY state

        if (!player.getUsername().equals(player.getSession().getHost()))
            throw new InvalidOperationException("Player is not the host.");                                             // throw exception when player isn't host

        Player playerToKick = playerRepository.findByUsernameAndSessionId(kickUsername, sessionId)                      // retrieves the player to kick
                .orElseThrow(() -> new PlayerNotInSessionException("Player is not in session."));

        if (player == playerToKick)
            throw new InvalidOperationException("Player cannot kick himself.");                                         // throw exception when player tries to kick himself

        playerRepository.delete(playerToKick);                                                                          // delete player from session

        log.info(username + " successfully kicked " + kickUsername + " from session: " + sessionId);
    }

    //endregion

    //endregion

    //region PLAYER

    /**
     * Saves a player.
     *
     * @param player Player that needs to be saved
     * @return Saved player
     */
    public Player savePlayer(Player player) {

        return playerRepository.save(player);
    }

    /**
     * Retrieves a player.
     *
     * @param playerId Id of the player that needs to be retrieved
     * @return Player that is retrieved
     * @throws EntityNotFoundException Thrown when the player can't be found.
     */
    public Player getPlayer(long playerId)
            throws EntityNotFoundException {

        return playerRepository.findById(playerId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find player."));
    }

    /**
     * Retrieves a player in a session.
     *
     * @param username  Username of the player that needs to be retrieved
     * @param sessionId Id of the session of which the player needs to be retrieved
     * @return Player that is retrieved from the given session
     * @throws PlayerNotInSessionException Thrown when no player is found.
     */
    public Player getPlayerInSession(String username, long sessionId)
            throws PlayerNotInSessionException {

        return playerRepository.findByUsernameAndSessionId(username, sessionId)
                .orElseThrow(() -> new PlayerNotInSessionException("This player is not active in this session."));
    }

    /**
     * Retrieves all players in a session.
     *
     * @param sessionId Id of the session of which the players need to retrieved
     * @return List of players that are in the given session
     * @throws NoPlayersInSessionException Thrown when no players are found.
     */
    public List<Player> getAllPlayersOfSession(long sessionId)
            throws NoPlayersInSessionException {

        return playerRepository.findAllBySessionId(sessionId)
                .orElseThrow(() -> new NoPlayersInSessionException("There are no players in this session."));
    }

    /**
     * Persists the color of the player.
     *
     * @param username Username of the authenticated user who tries to change a player's color
     * @param dto      Data transfer object that contains session, player and color
     * @return Player with updated color
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws PlayerNotInSessionException Thrown when no player is found.
     */
    public Player setPlayerColor(String username, PlayerColorDto dto)
            throws InvalidOperationException, PlayerNotInSessionException {

        Player host = playerRepository.findByUsernameAndSessionId(username, dto.getSessionId())                         // retrieve the player
                .orElseThrow(() -> new PlayerNotInSessionException("Player not in session."));

        if (!host.getSession().getHost().equals(username))
            throw new InvalidOperationException("Player is not the host.");                                             // throw exception when the player isn't host

        Player toUpdate = playerRepository.findByUsernameAndSessionId(dto.getUsername(), dto.getSessionId())
                .orElseThrow(() -> new PlayerNotInSessionException("Player not in session."));                          // throw exception when the player isn't in session

        toUpdate.setColor(dto.getColor());                                                                              // set the color of the player

        return playerRepository.save(toUpdate);
    }

    //endregion

    //region INVITE

    /**
     * Retrieves all invites sent by a player.
     *
     * @param username Username of the authenticated user who send the invites
     * @return List of invites that are send by the given user
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public List<SessionInviteDto> getSentInvites(String username)
            throws EntityNotFoundException {

        RiskUser sender = userService.getUser(username);
        List<Invite> invites = inviteRepository.findAllBySender(sender)
                .orElseThrow(() -> new EntityNotFoundException("No invites sent."));

        return invites
                .stream()
                .map(invite -> new SessionInviteDto(invite.getSession().getId(), invite.getReceiver().getUsername()))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves all invites received of a player.
     *
     * @param username Username of the authenticated user who received the invites
     * @return list of invites that are received by the given user
     */
    public List<Invite> getReceivedInvites(String username) {
        return inviteRepository.findAllByReceiver_Username(username).orElse(null);
    }

    /**
     * Sends an invite.
     *
     * @param username Username of the authenticated user who tries to send the invite
     * @param dto      Data transfer object that contains session and invitee data
     * @throws EntityNotFoundException       Thrown when a required entity can't be found.
     * @throws InvalidInviteSessionException Thrown when a session invite can't be sent.
     */
    public void sendSessionInvite(String username, SessionInviteDto dto)
            throws EntityNotFoundException, InvalidInviteSessionException {

        Session session = getSession(dto.getSessionId());                                                               // retrieve the session
        RiskUser invitedUser = userService.getUser(dto.getUsername());                                                  // retrieve the receiver
        RiskUser senderUser = userService.getUser(username);                                                            // retrieve the sender

        List<Player> list = session.getPlayers();
        if (list.stream().anyMatch(player -> player.getUsername().equals(invitedUser.getUsername())))
            throw new InvalidInviteSessionException("Player already in session.");                                      // throw exception when player is already in session

        if (invitedUser.getInvites().stream().anyMatch((i -> i.getSession().getId() == session.getId())))
            throw new InvalidInviteSessionException("Player already invited.");                                         // throw exception when user is already invited

        Invite invite = inviteRepository.save(new Invite(senderUser, session, invitedUser));                            // send invite
        invitedUser.getInvites().add(invite);
        session.getInvites().add(invite);
        sessionRepository.save(session);
        userService.saveUser(invitedUser);

        log.info(username + " successfully sent session invite to " + dto.getUsername());
    }

    /**
     * Accepts an invite.
     *
     * @param username  Username of the authenticated user who tries to accept the invite
     * @param sessionId Id of the session for which the given user is invited
     * @throws EntityNotFoundException  Thrown when a required entity can't be found.
     * @throws InvalidJoinGameException Thrown when an invite can't be accepted.
     */
    public void acceptSessionInvite(String username, long sessionId)
            throws EntityNotFoundException, InvalidJoinGameException {

        Session session = getSession(sessionId);                                                                        // retrieve the session
        RiskUser invitedUser = userService.getUser(username);                                                           // retrieve the user
        Invite invite = invitedUser.getInvites()                                                                        // retrieve the invite
                .stream()
                .filter(inv -> inv.getReceiver().getUsername().equals(username) && inv.getSession().getId() == sessionId)
                .findAny()
                .orElseThrow(() -> new InvalidJoinGameException("User is not invited."));                               // throw an exception when the user is not invited

        if (session.getPlayers().size() >= MAX_PLAYERS)
            throw new InvalidJoinGameException("Max players reached.");                                                 // throw an exception when the session is full

        if (session.getPlayers().stream().anyMatch(player -> player.getUsername().equals(username)))
            throw new InvalidJoinGameException("User is already in game.");                                             // throw an exception when the user is already in the session

        if (session.getStatus() != Status.LOBBY)
            throw new InvalidJoinGameException("Game cannot be joined.");                                               // throw an exception when the session is not in the LOBBY state

        Player invitedPlayer = playerRepository.save(new Player(invitedUser.getUsername(), PlayerType.HUMAN));
        boolean sameColor = true;
        while (sameColor) {                                                                                             // get random player color until it doesn't match any other player's color
            PlayerColor color = getRandomPlayerColor();
            if (session.getPlayers().stream().noneMatch(player -> player.getColor().equals(color))) {
                sameColor = false;
                invitedPlayer.setColor(color);
            }
        }
        invitedPlayer.setRiskUser(invitedUser);
        invitedUser.getPlayers().add(invitedPlayer);
        session.getPlayers().add(invitedPlayer);
        invitedPlayer.setSession(session);
        playerRepository.save(invitedPlayer);
        invitedUser = userService.saveUser(invitedUser);
        sessionRepository.save(session);


        Invite inviteToRemove = inviteRepository.findById(invite.getId())
                .orElseThrow(() -> new EntityNotFoundException("Could not find invite."));

        invitedUser.getInvites().removeIf(inv -> inv.getId() == inviteToRemove.getId());
        userService.saveUser(invitedUser);

        log.info(username + " successfully accepted invite for session: " + sessionId);
    }

    //endregion

    //region AI

    /**
     * Retrieves a random AI-player.
     *
     * @return Name of random AI-player
     */
    public PlayerAIName getRandomPlayerName() {

        int pick = new Random().nextInt(PlayerAIName.values().length);
        return PlayerAIName.values()[pick];
    }

    /**
     * Retrieves a random player color.
     *
     * @return Random color
     */
    public PlayerColor getRandomPlayerColor() {

        int pick = new Random().nextInt(PlayerColor.values().length);
        return PlayerColor.values()[pick];
    }

    /**
     * Makes an AI-player join the session.
     *
     * @param dto Data transfer object that contains session and player type
     * @return Session which an AI-player joined
     * @throws EntityNotFoundException  Thrown when a required entity can't be found.
     * @throws InvalidJoinGameException Thrown when an invite can't be accepted.
     */
    public Session joinPlayerAI(JoinCpuDto dto)
            throws EntityNotFoundException, InvalidJoinGameException {

        Session session = getSession(dto.getSessionId());
        String finalName = "";
        PlayerColor finalColor = PlayerColor.red;
        boolean sameName = true;
        boolean sameColor = true;

        if (session.getPlayers().size() >= MAX_PLAYERS)
            throw new InvalidJoinGameException("Max players reached.");

        if (session.getStatus() != Status.LOBBY)
            throw new InvalidJoinGameException("Game cannot be joined.");

        while (sameName) {                                                                                              // get random AI-player name until it doesn't match any other player's name
            String name = getRandomPlayerName().name();
            if (session.getPlayers().stream().noneMatch(player -> player.getUsername().equals(name)))
                sameName = false;

            finalName = name;
        }

        while (sameColor) {                                                                                             // get random player color until it doesn't match any other player's color
            PlayerColor color = getRandomPlayerColor();
            if (session.getPlayers().stream().noneMatch(player -> player.getColor().equals(color)))
                sameColor = false;

            finalColor = color;
        }

        Player player = playerRepository.save(new Player(finalName, dto.getPlayerType()));
        player.setColor(finalColor);
        session.getPlayers().add(player);
        player.setSession(session);
        playerRepository.save(player);
        session = sessionRepository.save(session);

        log.info(finalName + " has successfully joined session: " + session.getId());

        return session;
    }

    //endregion
}
