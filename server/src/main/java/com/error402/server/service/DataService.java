package com.error402.server.service;

import com.error402.server.dao.PlayerRepository;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.model.*;
import com.error402.server.model.data.GlobalData;
import com.error402.server.model.data.Highscore;
import com.error402.server.model.data.PersonalData;
import com.error402.server.model.data.SessionData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles demonstration of personal and global statistics.
 *
 * @author Error 402
 * @version 0.2
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@RequiredArgsConstructor
@Service
public class DataService {

    //region INJECTED PROPERTIES

    private final UserService userService;
    private final SessionService sessionService;
    private final PlayerRepository playerRepository;

    //endregion

    /**
     * Retrieves the personal data of a user.
     *
     * @param username Username of user whom the personal data needs to be retrieved for
     * @return Personal data for given user
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public PersonalData getPersonalDataOfUser(String username) {

        try {
            List<Player> playerList = playerRepository.findAllByUsernameAndSession_Status(username, Status.FINISHED).orElse(null);

            PersonalData personalData = new PersonalData(username);

            if (playerList != null) {
                List<Session> sessions = new ArrayList<>();
                playerList.forEach(player -> {
                    if (player.getSession().getStatus().equals(Status.FINISHED)) {
                        personalData.getHistory().add(
                                new SessionData(
                                        player.getSession().getId(),
                                        player.getSession().getMap(),
                                        player.getSession().getWinner().equals(username)
                                )
                        );
                        sessions.add(player.getSession());
                    }
                });

                if (!sessions.isEmpty()) {
                    personalData.setConquestsTotal(
                            sessions.size()
                    );

                    personalData.setConquestsWon(                                                                               // Conquests won
                            sessions
                                    .stream()
                                    .filter(conquest -> conquest.getWinner().equals(username))
                                    .count()
                    );

                    personalData.setConquestsLost(                                                                              // Conquests lost
                            sessions
                                    .stream()
                                    .filter(conquest -> !conquest.getWinner().equals(username))
                                    .count()
                    );

                    List<Action> actions = new ArrayList<>();
                    sessions.forEach(session -> {
                        session.getTurns().forEach(turn -> {
                            turn.getPhases().forEach(phase -> {
                                actions.addAll(phase.getActions());
                            });
                        });
                    });

                    personalData.setAttacksTotal(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getAttacker().equals(username))
                                    .count()
                    );

                    personalData.setAttacksSuccessful(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getAttacker().equals(username)
                                            && ((Attack) action).isAttackSuccessful())
                                    .count()
                    );

                    personalData.setAttacksFailed(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getAttacker().equals(username)
                                            && !((Attack) action).isAttackSuccessful())
                                    .count()
                    );

                    personalData.setDefendsTotal(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getDefender().equals(username))
                                    .count()
                    );

                    personalData.setDefendsSuccessful(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getDefender().equals(username)
                                            && !((Attack) action).isAttackSuccessful())
                                    .count()
                    );

                    personalData.setDefendsFailed(
                            actions.stream()
                                    .filter(action -> action instanceof Attack
                                            && ((Attack) action).getDefender().equals(username)
                                            && ((Attack) action).isAttackSuccessful())
                                    .count()
                    );

                    personalData.setTroopsDeployed(
                            actions.stream()
                                    .filter(action -> action instanceof Allocation
                                            && ((Allocation) action).getPlayer().equals(username))
                                    .mapToLong(action -> ((Allocation) action).getTroops())
                                    .sum()
                    );

                    long troopsDefeatedAsAttacker = actions.stream()
                            .filter(action -> action instanceof Attack
                                    && ((Attack) action).getAttacker().equals(username))
                            .mapToLong(action -> ((Attack) action).getDefendingTroopsAtStart() - ((Attack) action).getDefendingTroopsAtEnd())
                            .sum();

                    long troopsDefeatedAsDefender = actions.stream()
                            .filter(action -> action instanceof Attack
                                    && ((Attack) action).getDefender().equals(username))
                            .mapToLong(action -> ((Attack) action).getAttackingTroopsAtStart() - ((Attack) action).getAttackingTroopsAtEnd())
                            .sum();

                    personalData.setTroopsDefeated(troopsDefeatedAsAttacker + troopsDefeatedAsDefender);

                    long troopsLostAsAttacker = actions.stream()
                            .filter(action -> action instanceof Attack
                                    && ((Attack) action).getAttacker().equals(username))
                            .mapToLong(action -> ((Attack) action).getAttackingTroopsAtStart() - ((Attack) action).getAttackingTroopsAtEnd())
                            .sum();

                    long troopsLostAsDefender = actions.stream()
                            .filter(action -> action instanceof Attack
                                    && ((Attack) action).getDefender().equals(username))
                            .mapToLong(action -> ((Attack) action).getDefendingTroopsAtStart() - ((Attack) action).getDefendingTroopsAtEnd())
                            .sum();

                    personalData.setTroopsLost(troopsLostAsAttacker + troopsLostAsDefender);
                }
            }

            return personalData;
        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            log.error (e.getCause().getMessage());
            throw e;
        }
    }

    /**
     * Retrieves the global data.
     *
     * @return Global data
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    public GlobalData getGlobalData()
            throws EntityNotFoundException {

        try {
            List<Session> sessions = sessionService.getSessionsByGameType(GameType.HUMAN);

            GlobalData globalData = new GlobalData();

            if (!sessions.isEmpty()) {
                globalData.setConquestsFinished(
                        sessions.stream()
                                .filter(session -> session.getStatus().equals(Status.FINISHED))
                                .count()
                );

                globalData.setConquestsPlaying(
                        sessions.stream()
                                .filter(session -> session.getStatus().equals(Status.PLAYING))
                                .count()
                );

                globalData.setConquestsLobby(
                        sessions.stream()
                                .filter(session -> session.getStatus().equals(Status.LOBBY))
                                .count()
                );

                List<String> winners = sessions.stream()
                        .filter(session -> session.getStatus().equals(Status.FINISHED))
                        .map(Session::getWinner)
                        .collect(Collectors.toList());

                winners.stream()
                        .distinct()
                        .collect(Collectors.toList())
                        .forEach(winner -> {
                            List<Player> players = new ArrayList<>();
                            int games = playerRepository.findAllByUsernameAndSession_Status(winner, Status.FINISHED).orElse(players).size();
                            globalData.getHighscores().add(
                                    new Highscore(
                                            winner,
                                            winners.stream().filter(winner1 -> winner1.equals(winner)).count(),
                                            games
                                    )
                            );
                        });

                globalData.setHighscores(
                        globalData.getHighscores().stream().limit(10).collect(Collectors.toList())
                );

                globalData.setUsersInConquestPlaying(
                        sessions.stream()
                                .filter(session -> session.getStatus().equals(Status.PLAYING))
                                .flatMap(session -> session.getPlayers().stream())
                                .filter(player -> player.getPlayerType().equals(PlayerType.HUMAN))
                                .map(Player::getUsername)
                                .distinct()
                                .count()
                );
            }

            return globalData;
        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }
    }
}
