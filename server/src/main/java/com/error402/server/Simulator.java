package com.error402.server;

import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.event.CpuTurnEvent;
import com.error402.server.model.dto.*;
import com.error402.server.service.CpuService;
import com.error402.server.service.GameService;
import com.error402.server.service.SessionService;
import com.error402.server.service.UserService;
import com.error402.server.utils.Utils;
import jdk.jshell.spi.ExecutionControl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@AllArgsConstructor
@Component
public class Simulator {
    private final GameService gameService;
    private final UserService userService;
    private final SessionService sessionService;
    private final CpuService cpuService;
    private final ApplicationEventPublisher publisher;

    public void start(SimulatorDto simulatorDto) throws Exception {

        ExecutorService executor = Executors.newFixedThreadPool(10);

        if (simulatorDto.getGameType().name().equalsIgnoreCase("Human")) {
            gameWithPlayer();
        } else if (simulatorDto.getGameType().name().equalsIgnoreCase("Cpu")) {

            executor.submit(() -> {
                try {
                    gameWithCpu(simulatorDto.getUsername(), simulatorDto.getGames(), simulatorDto.getPlayerType());
                } catch (Exception e) {
                    log.info(e.getMessage());
                }
            });

        } else {
            log.info("wrong gametype");
        }
    }

    private void gameWithCpu(String username, int games, PlayerType type) throws EntityNotFoundException, InvalidJoinGameException, PlayerNotInSessionException, InvalidPhaseException, ParseException, IOException, InterruptedException, InvalidOperationException, NoPlayersInSessionException, NotEnoughPlayersException, ExecutionControl.NotImplementedException {
        for (int i = 0; i < games; i++) {
            //create main CpuPlayer
            Session session = cpuService.createCpuSession(username, type);

            int numberOfPlayers = Utils.getRandom(3, 4);
            //fill lobby with players
            for (int j = 0; j < numberOfPlayers - 1; j++) {
                List<PlayerType> list = new ArrayList<>(Arrays.asList(PlayerType.values()));

                PlayerType playerType = list.get(Utils.getRandom(1, list.size() - 2));
                sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), playerType));
            }
            session = cpuService.initializeCpuGame(username, session.getId(), "Global Conquest");

            session = cpuService.initialTurn(session);
            session = gameService.saveStartState(session.getId());
            Turn turn = gameService.getActiveTurn(session);
            int turnCount = 0;
            while (session.getStatus() != Status.FINISHED) {
                turnCount++;
                log.info("turn: " + turnCount);
                Player player = sessionService.getPlayer(turn.getPlayer().getId());
                log.info(player.toString());
                publisher.publishEvent(new CpuTurnEvent(this, session, player.getUsername()));
                turn = gameService.getActiveTurn(session);
                session = sessionService.getSession(session.getId());
            }
            log.info(session.getWinner());
        }
        log.info("games ended");
    }

    private String gameWithPlayer() throws Exception {
        // Player 1 creates new session
        userService.createUserIfNotExists("admin");
        Session session = sessionService.createSession("admin");

        // Player 1 invites 3 different players
//        Invite invite1 = sessionService.sendSessionInvite("Player 1", new SessionInviteDto(session.getId(), "Player 2"));
//        Invite invite2 = sessionService.sendSessionInvite("Player 1", new SessionInviteDto(session.getId(), "Player 3"));
//        Invite invite3 = sessionService.sendSessionInvite("Player 1", new SessionInviteDto(session.getId(), "Player 4"));

        // Players accept invite
//        sessionService.acceptSessionInvite("Player 2", session.getId());
//        sessionService.acceptSessionInvite("Player 3", session.getId());
//        sessionService.acceptSessionInvite("Player 4", session.getId());

        //Player invites 2cpu's
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));
        sessionService.joinPlayerAI(new JoinCpuDto(session.getId(), PlayerType.EASY));

        // Player 1 starts game
        session = gameService.initialize("admin", session.getId(), "regular");


        Scanner sc = new Scanner(System.in);
        Turn turn = gameService.getActiveTurn(session);
        // Keep playing a game until game is finished
        while (session.getStatus() != Status.FINISHED) {
            // This is one turn of a player
            // Allocation Phase
            Player currentPlayer = sessionService.getPlayer(turn.getPlayer().getId());
            String input = "";
            log.info("Turn for " + currentPlayer.getUsername() + " starts with " + turn.getPhases().get(turn.getPhases().size() - 1).getPhaseType() + " phase.");
            while (!input.equals("next phase")) {
                currentPlayer = sessionService.getPlayer(turn.getPlayer().getId());
                if (currentPlayer.getPlayerType() != PlayerType.HUMAN) {
                    input = cpuAllocationPhase(session, currentPlayer);
                } else {
                    input = allocationPhase(currentPlayer, input, sc, session);
                }
            }
            // End allocation phase
            // Start attacking phase
            session = gameService.nextPhase(session, currentPlayer.getUsername());
            log.info("Attack phase for " + currentPlayer.getUsername() + " starts.");
            input = "";
            while (!input.equals("next phase")) {
                currentPlayer = sessionService.getPlayer(turn.getPlayer().getId());
                if (currentPlayer.getPlayerType() != PlayerType.HUMAN) {
                    input = cpuAttackPhase(currentPlayer);
                } else {
                    input = attackPhase(currentPlayer, input, sc, session);
                }
                if (gameService.isGameFinished(session)) {
                    session.setStatus(Status.FINISHED);
                }
            }
            // End attacking phase
            // Start fortify phase
            session = gameService.nextPhase(session, currentPlayer.getUsername());
            log.info("Fortify phase for " + currentPlayer.getUsername() + " starts.");
            currentPlayer = sessionService.getPlayer(turn.getPlayer().getId());
            if (currentPlayer.getPlayerType() != PlayerType.HUMAN) {
                cpuFortifyPhase(currentPlayer);
            } else {
                fortifyPhase(currentPlayer, sc, session);
            }
            log.info(currentPlayer.getUsername() + " ends his turn.");
            // End fortify phase
            // End of turn
            // Start next turn
            session = gameService.nextTurn(session);
            turn = gameService.getActiveTurn(session);
        }
        return "Game finished";
    }

    private String fortifyPhase(Player currentPlayer, Scanner sc, Session session) throws Exception {
        List<Territory> territories = currentPlayer.getTerritories();
        log.info("These are your territories:");
        territories.forEach(t -> log.info(t.getName() + " --> " + t.getTroops() + " troops"));
        log.info("Where do you want to fortify troops from?");
        String input = sc.nextLine();
        if (input.equals("end turn")) {
            return input;
        }

        Territory territoryWhichFortifies = null;
        for (Territory territoryToFind : territories) {
            if (territoryToFind.getName().toUpperCase().equals(input.toUpperCase())) {
                territoryWhichFortifies = territoryToFind;
            }
        }
        if (territoryWhichFortifies == null) {
            log.info("Not a valid territory");
            return "";
        }

        log.info("Where do you want to fortify troops to?");
        input = sc.nextLine();
        if (input.equals("end turn")) {
            return input;
        }

        Territory territoryToFortify = null;
        for (Territory territoryToFind : territories) {
            if (territoryToFind.getName().toUpperCase().equals(input.toUpperCase())) {
                territoryToFortify = territoryToFind;
            }
        }
        if (territoryToFortify == null) {
            log.info("Not a valid territory");
            return "";
        }

        log.info("how many troops?");
        String numberTroops = sc.nextLine();
        if (numberTroops.equals("end turn")) {
            return numberTroops;
        }
        gameService.fortify(currentPlayer.getUsername(), new FortifyDto(session.getId(), territoryWhichFortifies.getId(), territoryToFortify.getId(), Integer.parseInt(numberTroops)));
        return "";
    }

    private String attackPhase(Player currentPlayer, String input, Scanner sc, Session session) throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException {
        log.info("These are your territories:");
        List<Territory> territories = currentPlayer.getTerritories();
        territories.forEach(t -> log.info(t.getName() + " --> " + t.getTroops() + " troops"));
        log.info("Choose a territory where you want to attack from");
        input = sc.nextLine();
        if (input.equals("next phase")) {
            return input;
        }
        Territory territory = null;
        for (Territory territoryToFind : territories) {
            if (territoryToFind.getName().toUpperCase().equals(input.toUpperCase())) {
                territory = territoryToFind;
            }
        }
        if (territory == null) {
            log.info("Not a valid territory");
            return "";
        }

        List<Border> borders = territory.getBorders();
        List<Territory> territoriesToAttack = new ArrayList<>();

        for (Border border : borders) {
            Territory territoryFromBorder = cpuService.getTerritoryFromBorder(border);
            if (!territoryFromBorder.getPlayer().getUsername().equals(currentPlayer.getUsername()))
                territoriesToAttack.add(territoryFromBorder);
        }
        log.info("These are all the territories you can attack from " + territory.getName());
        territoriesToAttack.forEach(t -> log.info(t.getName() + " --> " + t.getTroops() + " troops"));
        log.info("Choose a territory which you want to attack");
        input = sc.nextLine();
        if (input.equals("next phase")) {
            return input;
        }
        Territory territoryToAttack = null;
        for (Territory territoryToFind : territoriesToAttack) {
            if (territoryToFind.getName().equals(input)) {
                territoryToAttack = territoryToFind;
            }
        }
        if (territoryToAttack == null) {
            log.info("Not a valid territory");
            return "";
        }
        gameService.attack(currentPlayer.getUsername(), new AttackDto(session.getId(), territory.getId(), territoryToAttack.getId()));
        return "";
    }

    private String allocationPhase(Player currentPlayer, String input, Scanner sc, Session session) throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException {
        log.info("You have " + currentPlayer.getTroops() + " troops to allocate");
        log.info("These are your territories:");
        currentPlayer.getTerritories().forEach(t -> log.info(t.getName() + " --> " + t.getTroops() + " troops"));
        log.info("Where do you want to allocate troops?");
        input = sc.nextLine();
        if (input.equals("next phase")) {
            return input;
        }
        log.info("how many troops?");
        String numberTroops = sc.nextLine();
        if (numberTroops.equals("next phase")) {
            return numberTroops;
        }
        List<Territory> territories = currentPlayer.getTerritories();
        long territoryId = 0;
        for (Territory territory : territories) {
            if (territory.getName().equals(input)) {
                territoryId = territory.getId();
            }

        }
        gameService.allocate(currentPlayer.getUsername(), new AllocateDto(session.getId(), territoryId
                , Integer.parseInt(numberTroops)));
        return "";
    }

    private String cpuAllocationPhase(Session session, Player currentPlayer) throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, NoPlayersInSessionException {
        if (currentPlayer.getTroops() > 0) {
            AllocateDto allocateDto = cpuService.determineAllocationForMedium(session, currentPlayer);
            gameService.allocate(currentPlayer.getUsername(), allocateDto);
            return "";
        } else {
            return "next phase";
        }

    }

    private String cpuAttackPhase(Player currentPlayer) {
        return "next phase";
    }

    private String cpuFortifyPhase(Player playerAI) {
        return "end turn";
    }
}
