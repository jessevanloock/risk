package com.error402.server.exceptions;

public class InvalidInviteSessionException extends Exception {
    public InvalidInviteSessionException(String message) {
        super(message);
    }
}
