package com.error402.server.exceptions;

public class InvalidPhaseException extends Exception {
    public InvalidPhaseException(String message) {
        super(message);
    }
}
