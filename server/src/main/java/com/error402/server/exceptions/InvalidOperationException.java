package com.error402.server.exceptions;

public class InvalidOperationException extends Exception {
    public InvalidOperationException(String message) {
        super(message);
    }
}
