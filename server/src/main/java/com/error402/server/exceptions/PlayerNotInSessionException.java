package com.error402.server.exceptions;

public class PlayerNotInSessionException extends Exception {
    public PlayerNotInSessionException(String message) {
        super(message);
    }
}
