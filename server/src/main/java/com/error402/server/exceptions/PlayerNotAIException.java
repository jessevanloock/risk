package com.error402.server.exceptions;

public class PlayerNotAIException extends Exception {
    public PlayerNotAIException(String message) {
        super(message);
    }
}
