package com.error402.server.exceptions;

public class AlreadyFriendsException extends Exception {
    public AlreadyFriendsException(String message) { super(message); }
}
