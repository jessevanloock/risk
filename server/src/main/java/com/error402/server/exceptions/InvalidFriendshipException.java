package com.error402.server.exceptions;

public class InvalidFriendshipException extends Exception {
    public InvalidFriendshipException(String message) {
        super(message);
    }
}
