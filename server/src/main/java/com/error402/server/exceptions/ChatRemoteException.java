package com.error402.server.exceptions;

public class ChatRemoteException extends Exception {
    public ChatRemoteException(String message) {
        super(message);
    }
}
