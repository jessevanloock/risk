package com.error402.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Handles all exceptions and advises controller of correct response.
 *
 * @author  Error 402
 * @version 0.2
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler
    public final ResponseEntity<String> handleException(Exception ex, WebRequest request) {
        if (ex instanceof EntityNotFoundException) {
            EntityNotFoundException enfex = (EntityNotFoundException) ex;
            return new ResponseEntity<>(enfex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof PlayerNotInSessionException) {
            PlayerNotInSessionException pnisex = (PlayerNotInSessionException) ex;
            return new ResponseEntity<>(pnisex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof InvalidPhaseException) {
            InvalidPhaseException ipex = (InvalidPhaseException) ex;
            return new ResponseEntity<>(ipex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof InvalidOperationException) {
            InvalidOperationException ioex = (InvalidOperationException) ex;
            return new ResponseEntity<>(ioex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof InvalidFriendshipException) {
            InvalidFriendshipException ifex = (InvalidFriendshipException) ex;
            return new ResponseEntity<>(ifex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof AlreadyFriendsException) {
            AlreadyFriendsException afex = (AlreadyFriendsException) ex;
            return new ResponseEntity<>(afex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof ChatRemoteException) {
            ChatRemoteException crex = (ChatRemoteException) ex;
            return new ResponseEntity<>(crex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof InvalidInviteSessionException) {
            InvalidInviteSessionException iisex = (InvalidInviteSessionException) ex;
            return new ResponseEntity<>(iisex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof InvalidJoinGameException) {
            InvalidJoinGameException ijgex = (InvalidJoinGameException) ex;
            return new ResponseEntity<>(ijgex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof NoPlayersInSessionException) {
            NoPlayersInSessionException npisex = (NoPlayersInSessionException) ex;
            return new ResponseEntity<>(npisex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof NotEnoughPlayersException) {
            NotEnoughPlayersException nepex = (NotEnoughPlayersException) ex;
            return new ResponseEntity<>(nepex.getMessage(), HttpStatus.BAD_REQUEST);
        } else if (ex instanceof NotPlayerTurnException) {
            NotPlayerTurnException nptex = (NotPlayerTurnException) ex;
            return new ResponseEntity<>(nptex.getMessage(), HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("Oops, something went wrong...", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
