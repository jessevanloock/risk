package com.error402.server.exceptions;

public class NoPlayersInSessionException extends Exception {
    public NoPlayersInSessionException(String message) {
        super(message);
    }
}
