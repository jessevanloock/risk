package com.error402.server.exceptions;

public class InvalidJoinGameException extends Exception {
    public InvalidJoinGameException(String message) {
        super(message);
    }
}
