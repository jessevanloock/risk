package com.error402.server.exceptions;

public class NotPlayerTurnException extends Exception {
    public NotPlayerTurnException(String message) {
        super(message);
    }
}
