package com.error402.server.controller;

import com.error402.server.communication.chat.ChatStub;
import com.error402.server.exceptions.*;
import com.error402.server.model.*;
import com.error402.server.model.dto.*;
import com.error402.server.model.dto.gameoverview.ConquestDto;
import com.error402.server.model.dto.gameoverview.FinishedDto;
import com.error402.server.model.dto.gameoverview.InviteDto;
import com.error402.server.model.dto.gameoverview.LobbyDto;
import com.error402.server.service.NotifierService;
import com.error402.server.service.SessionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles all incoming calls concerning outside-session logic.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
@CrossOrigin({"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/session")
@MessageMapping("/lobby")
public class ApiSessionController {

    @Value("${websocket.topic.lobby}")
    private String LOBBY_STATE_TOPIC;

    //region INJECTED PROPERTIES

    private final SessionService sessionService;
    private final ChatStub chatStub;
    private final NotifierService notifierService;

    //endregion

    /**
     * Handles a request to create a new session.
     *
     * @param principal Authenticated user that tries to create a session
     * @return Response containing the id of the created session
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws ChatRemoteException     Thrown when a
     */
    @PostMapping
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Long> create(@RequestHeader("Authorization") String authorization, Principal principal)
            throws EntityNotFoundException, ChatRemoteException {

        log.info("POST: /session from " + principal.getName());

        Session session = sessionService.createSession(principal.getName());                                            // create a session
        boolean result = chatStub.createSessionChat(session.getId(), authorization);

        if (!result)
            throw new ChatRemoteException("Something went wrong while trying to create a session chat.");

        return new ResponseEntity<>(session.getId(), HttpStatus.OK);
    }

    /**
     * Handles a request to discard a session.
     *
     * @param principal Authenticated user that tries to discard a session
     * @param sessionId Id of the session that is discarded
     * @return Empty response
     * @throws PlayerNotInSessionException Thrown when player can't be found in session.
     * @throws InvalidOperationException   Thrown when the operation is invalid and can't be executed.
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> discard(Principal principal, @PathVariable("id") long sessionId)
            throws PlayerNotInSessionException, InvalidOperationException {

        log.info("DELETE: /session/{id} from " + principal.getName());

        sessionService.discard(principal.getName(), sessionId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to leave a session.
     *
     * @param principal Authenticated user that tries to leave a session
     * @param sessionId Id of the session from which the user wants to leave
     * @return Empty response
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws PlayerNotInSessionException Thrown when player can't be found in session.
     * @throws InvalidOperationException   Thrown when the operation is invalid and can't be executed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @DeleteMapping("/leave/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> leave(Principal principal, @PathVariable("id") long sessionId)
            throws NoPlayersInSessionException, PlayerNotInSessionException, InvalidOperationException, EntityNotFoundException, IOException, ParseException {

        log.info("DELETE: /session/leave/{id} from " + principal.getName());

        sessionService.leave(principal.getName(), sessionId);

        Session session = sessionService.getSession(sessionId);
        notifyPlayersOfLobbyStateUpdate(session);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to kick a player from a session.
     *
     * @param principal Authenticated user that tries to kick a player
     * @param sessionId Id of the session from which the player is tried to be kicked
     * @param username  Username of the player that is tried to be kicked
     * @return Empty response
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws PlayerNotInSessionException Thrown when player can't be found in session.
     * @throws InvalidOperationException   Thrown when the operation is invalid and can't be executed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @DeleteMapping("/kick/{sessionId}/{username}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> kick(Principal principal, @PathVariable("sessionId") long sessionId, @PathVariable("username") String username)
            throws InvalidOperationException, PlayerNotInSessionException, ParseException, NoPlayersInSessionException, IOException, EntityNotFoundException {

        log.info("DELETE: /session/kick/{sessionId}/{username} from " + principal.getName());

        sessionService.kick(principal.getName(), sessionId, username);

        Session session = sessionService.getSessionLobby(principal.getName(), sessionId);
        notifyPlayersOfLobbyStateUpdate(session);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to view the lobby of a session.
     *
     * @param principal Authenticated user that tries to view the lobby
     * @param sessionId Id of the session of which the lobby will be viewed
     * @return Response containing session lobby data
     * @throws EntityNotFoundException   Thrown when a required entity can't be found.
     * @throws InvalidOperationException Thrown when the operation can't be executed.
     */
    @GetMapping(value = "/lobby/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<SessionLobbyDto> getSessionLobby(Principal principal, @PathVariable("id") Long sessionId)
            throws EntityNotFoundException, InvalidOperationException {

        Session session = sessionService.getSessionLobby(principal.getName(), sessionId);                               // retrieve the lobby

        return new ResponseEntity<>(new SessionLobbyDto(session), HttpStatus.OK);                                       // return the lobby
    }

    /**
     * Handles a request to change the color of a player.
     *
     * @param principal Authenticated user that tries to change the color of a player
     * @param dto       Data transfer object containing player and color data
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws PlayerNotInSessionException Thrown when player can't be found in session.
     * @throws InvalidOperationException   Thrown when the operation is invalid and can't be executed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/color")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public void setColor(Principal principal, @RequestBody PlayerColorDto dto)
            throws EntityNotFoundException, PlayerNotInSessionException, InvalidOperationException, ParseException, NoPlayersInSessionException, IOException {

        log.info("MESSAGE: /lobby/color from " + principal.getName());

        sessionService.setPlayerColor(principal.getName(), dto);
        Session session = sessionService.getSessionLobby(principal.getName(), dto.getSessionId());

        notifyPlayersOfLobbyStateUpdate(session);
    }

    /**
     * Handles a request to retrieve received invites of a player.
     *
     * @param principal Authenticated user that tries to retrieve invites
     * @return Response containing a list of data transfer object that contain invite data
     */
    @GetMapping("/overview/invites")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<InviteDto>> getInvites(Principal principal) {

        log.info("GET: /session/overview/invites from " + principal.getName());

        List<Invite> invites = sessionService.getReceivedInvites(principal.getName());
        List<InviteDto> dtos = new ArrayList<>();
        if (invites != null) {
            invites.forEach(invite -> {
                InviteDto dto = new InviteDto(
                        invite.getSender().getUsername(),
                        invite.getSession().getId()
                );

                dtos.add(dto);
            });
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);                                                               // return received invites
    }

    /**
     * Handles a request to view the lobbies of a player.
     *
     * @param principal Authenticated user that tries to view lobbies
     * @return Response containing a list of data transfer objects that contain lobby data
     */
    @GetMapping("/overview/lobbies")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<LobbyDto>> getLobbies(Principal principal) {

        log.info("GET: /session/overview/lobbies from " + principal.getName());

        List<Session> lobbies = sessionService.getSessionsOfPlayerByStatus(principal.getName(), Status.LOBBY);
        List<LobbyDto> dtos = new ArrayList<>();
        if (lobbies != null) {
            lobbies.forEach(lobby -> {
                LobbyDto dto = new LobbyDto(
                        lobby.getId(),
                        lobby.getHost(),
                        lobby.getCreated().getTime(),
                        lobby.getPlayers().size()
                );

                dtos.add(dto);
            });
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Handles a request to view the active games of a player.
     *
     * @param principal Authenticated user that tries to view playing sessions
     * @return Response containing a list of data transfer objects that contain active game data
     */
    @GetMapping("/overview/playing")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<ConquestDto>> getActiveSessions(Principal principal) {

        log.info("GET: /session/overview/playing from " + principal.getName());

        List<Session> playing = sessionService.getSessionsOfPlayerByStatus(principal.getName(), Status.PLAYING);
        List<ConquestDto> dtos = new ArrayList<>();
        if (playing != null) {
            playing.forEach(session -> {
                ConquestDto dto = new ConquestDto(
                        session.getId(),
                        session.getHost(),
                        session.getMap(),
                        session.getPlayers().size(),
                        session.getTurns().get(session.getTurns().size() - 1).getStartTime().getTime(),
                        session.getTurns().get(session.getTurns().size() - 1).getPlayer().getUsername().equals(principal.getName()),
                        session.getTurns().get(session.getTurns().size() - 1).getPhases().get(session.getTurns().get(session.getTurns().size() - 1).getPhases().size() - 1).getPhaseType()
                );

                dtos.add(dto);
            });
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Handles a request to view the finished games of a player.
     *
     * @param principal Authenticated user that tries to view finished games
     * @return Response containing a list of data transfer objects that contain finished game data
     */
    @GetMapping("/overview/finished")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<FinishedDto>> getFinishedSessions(Principal principal) {

        log.info("GET: /session/overview/finished " + principal.getName());

        List<Session> finished = sessionService.getSessionsOfPlayerByStatus(principal.getName(), Status.FINISHED);
        List<FinishedDto> dtos = new ArrayList<>();
        if (finished != null) {
            finished.forEach(session -> {
                FinishedDto dto = new FinishedDto(
                        session.getMap(),
                        session.getWinner(),
                        session.getId()
                );

                dtos.add(dto);
            });
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    /**
     * Handles a request to view the game overview of a session.
     *
     * @param sessionId Id of the session of which the game overview is requested
     * @return Response containing the game overview of a session
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     * @throws JsonProcessingException Thrown when a JSON processing error occurs.
     */
    @GetMapping("/overview/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<JSONObject> getOverviewOfGame(@PathVariable("id") long sessionId)
            throws EntityNotFoundException, JsonProcessingException {

        log.info("GET: /session/overview/{id}");

        JSONObject returnJson = new JSONObject();

        try {
            JSONObject startState = new ObjectMapper().readValue(sessionService.getSession(sessionId).getStartState(), JSONObject.class);
            List<ActionDto> dtos = new ArrayList<>();

            sessionService.getSessionLog(sessionId).forEach(action -> {
                ActionDto actionDto = null;
                if (action instanceof Allocation) actionDto = new ActionDto((Allocation) action);
                else if (action instanceof Attack) actionDto = new ActionDto((Attack) action);
                else if (action instanceof Fortification) actionDto = new ActionDto((Fortification) action);
                else if (action instanceof StartPhaseAction) actionDto = new ActionDto((StartPhaseAction) action);
                if (actionDto != null) dtos.add(actionDto);
            });

            returnJson.put("startstate", startState);
            returnJson.put("log", dtos);
        } catch (Exception e) {
            log.error(e.toString() + ": " + e.getMessage());
            throw e;
        }

        return new ResponseEntity<>(returnJson, HttpStatus.OK);
    }

    /**
     * Handles a request to accept a session invite.
     *
     * @param principal Authenticated user that tries to accept a session invite
     * @param sessionId Id of the session for which the user is invited
     * @return Empty response
     * @throws InvalidJoinGameException    Thrown when a session can't be joined.
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws InvalidOperationException   Thrown when the operation is invalid and can't be executed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @PostMapping("/accept/{id}")
    public ResponseEntity<Void> accept(Principal principal, @PathVariable("id") long sessionId)
            throws InvalidJoinGameException, EntityNotFoundException, InvalidOperationException, ParseException, NoPlayersInSessionException, IOException {

        log.info("POST: /session/accept/{id} from " + principal.getName());

        sessionService.acceptSessionInvite(principal.getName(), sessionId);                                             // accept the invite
        Session session = sessionService.getSessionLobby(principal.getName(), sessionId);                               // retrieve the lobby

        notifyPlayersOfLobbyStateUpdate(session);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to invite a player to a session.
     *
     * @param principal Authenticated user that tries to invite a player
     * @param dto       Data transfer object containing invite data
     * @return Empty response
     * @throws InvalidInviteSessionException Thrown when an invite can't be sent.
     * @throws EntityNotFoundException       Thrown when a required entity can't be found.
     */
    @PostMapping("/invite")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> sendSessionInvite(Principal principal, @RequestBody SessionInviteDto dto)
            throws InvalidInviteSessionException, EntityNotFoundException {

        log.info("POST: /session/invite from " + principal.getName());

        sessionService.sendSessionInvite(principal.getName(), dto);                                                     // send the invite

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to retrieve sent invites of a player.
     *
     * @param principal Authenticated user that tries to retrieve sent invites
     * @return Response containing a list of data transfer objects that contain session invite data
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    @GetMapping("/sent-invites")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<SessionInviteDto>> getSentInvites(Principal principal)
            throws EntityNotFoundException {

        log.info("GET: /session/sent-invites from " + principal.getName());

        return new ResponseEntity<>(sessionService.getSentInvites(principal.getName()), HttpStatus.OK);                 // return sent invites
    }

    /**
     * Handles a request to let a cpu join the session.
     *
     * @param dto Data transfer object containing session and cpu data
     * @return Empty response
     * @throws InvalidJoinGameException    Thrown when a session can't be joined.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws IOException                 Thrown when an I/O error occurs.
     */
    @PostMapping("/join-session-cpu")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> cpuJoinSession(@RequestBody JoinCpuDto dto)
            throws InvalidJoinGameException, EntityNotFoundException, ParseException, NoPlayersInSessionException, IOException {

        log.info("POST: /session/join-session-cpu");

        Session session = sessionService.joinPlayerAI(dto);
        notifyPlayersOfLobbyStateUpdate(session);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Notifies all players in a session of an update.
     *
     * @param session Session of which the players need to be notified
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws NoPlayersInSessionException Thrown when no players can't be found in session.
     * @throws IOException                 Thrown when an I/O error occurs.
     */
    private void notifyPlayersOfLobbyStateUpdate(Session session)
            throws ParseException, NoPlayersInSessionException, IOException, EntityNotFoundException {

        notifierService.notify(session.getPlayers(), LOBBY_STATE_TOPIC, session);
    }
}
