package com.error402.server.controller;

import com.error402.server.exceptions.AlreadyFriendsException;
import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.exceptions.InvalidFriendshipException;
import com.error402.server.model.dto.FriendshipDto;
import com.error402.server.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Handles all incoming calls concerning a user.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
@CrossOrigin({"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class ApiUserController {

    private final UserService userService;

    /**
     * Handles a request to retrieve the friend requests of a user.
     *
     * @param principal Authenticated user that tries to retrieve friend requests
     * @return Response containing a list with data transfer objects that contain friendship data
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    @GetMapping("/friends/requests")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<List<FriendshipDto>> getFriendRequests(Principal principal)
            throws EntityNotFoundException {

        log.info("GET: /user/friends/requests from " + principal.getName());

        return new ResponseEntity<>(userService.getFriendRequests(principal.getName()), HttpStatus.OK);
    }

    /**
     * Handles a request to confirm a friend request.
     *
     * @param principal    Authenticated user that tries to confirm a friend request
     * @param friendshipId Id of the friendship that needs to be confirmed
     * @return Empty response
     * @throws InvalidFriendshipException Thrown when a friendship action can't be executed.
     * @throws EntityNotFoundException    Thrown when a required entity can't be found.
     */
    @PutMapping("/friends/{id}/confirm")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> confirmFriendRequest(Principal principal, @PathVariable("id") long friendshipId)
            throws InvalidFriendshipException, EntityNotFoundException {

        log.info("PUT: /user/friends/{id}/confirm from " + principal.getName());

        userService.confirmFriendship(principal.getName(), friendshipId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to send a friend request.
     *
     * @param principal Authenticated user that tries to send a friend request
     * @param username  Username of the receiver of the friend request
     * @return Response containing a data transfer object that contains friendship data
     * @throws AlreadyFriendsException Thrown when a friendship already exists.
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    @PostMapping("/friends/send-request")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<FriendshipDto> sendFriendRequest(Principal principal, @RequestBody String username)
            throws AlreadyFriendsException, EntityNotFoundException {

        log.info("POST: /user/friends/send-request from " + principal.getName());

        return new ResponseEntity<>(userService.sendFriendRequest(principal.getName(), username), HttpStatus.OK);
    }

    /**
     * Handles a request to create a new user.
     *
     * @param principal Authenticated user that needs to be created
     * @return Empty response
     */
    @PostMapping("/create-user")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> createUser(Principal principal) {

        log.info("POST: /user/create-user from " + principal.getName());

        if (!userService.checkIfUserExists(principal.getName())) {
            userService.createUserIfNotExists(principal.getName());
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Handles a request to delete a friendship.
     *
     * @param principal    Authenticated user that tries to delete a friendship
     * @param friendshipId Id of the friendship that needs to be deleted
     * @return Empty response
     * @throws InvalidFriendshipException Thrown when a friendship action can't be executed.
     * @throws EntityNotFoundException    Thrown when a required entity can't be found.
     */
    @DeleteMapping("/friends/{id}/delete")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> deleteFriendship(Principal principal, @PathVariable("id") long friendshipId)
            throws InvalidFriendshipException, EntityNotFoundException {

        log.info("DELETE: /user/friends/{id}/delete from " + principal.getName());

        userService.deleteFriendship(principal.getName(), friendshipId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
