package com.error402.server.controller;

import com.error402.server.exceptions.EntityNotFoundException;
import com.error402.server.model.data.GlobalData;
import com.error402.server.model.data.PersonalData;
import com.error402.server.service.DataService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Handles all incoming calls concerning personal and global statistics.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@CrossOrigin({"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/data")
public class ApiDataController {

    private final DataService dataService;

    /**
     * Handles a request to retrieve the personal data of a player.
     *
     * @param username Username of the player whose data is retrieved
     * @return Response containing personal statistics
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    @GetMapping("/personal/{username}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<PersonalData> fetchPersonalData(@PathVariable("username") String username)
            throws EntityNotFoundException {

        log.info("GET: /data/personal/{username} from " + username);

        return new ResponseEntity<>(
                dataService.getPersonalDataOfUser(username),
                HttpStatus.OK
        );
    }

    /**
     * Handles a request to retrieve the global data.
     *
     * @return Response containing global statistics
     * @throws EntityNotFoundException Thrown when a required entity can't be found.
     */
    @GetMapping("/global")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<GlobalData> fetchGlobalData()
            throws EntityNotFoundException {

        log.info("GET: /data/global");

        return new ResponseEntity<>(
                dataService.getGlobalData(),
                HttpStatus.OK
        );
    }
}
