package com.error402.server.controller;

import com.error402.server.exceptions.*;
import com.error402.server.model.Session;
import com.error402.server.model.dto.AllocateDto;
import com.error402.server.model.dto.AttackDto;
import com.error402.server.model.dto.FortifyDto;
import com.error402.server.model.dto.InitializeDto;
import com.error402.server.service.GameService;
import com.error402.server.service.NotifierService;
import com.error402.server.service.SessionService;
import com.error402.server.service.UserService;
import com.error402.server.utils.Utils;
import jdk.jshell.spi.ExecutionControl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;

/**
 * Handles all incoming calls concerning inside-session logic.
 * Only handles calls that return a state.
 *
 * @author   Error 402
 * @version  0.2
 */
@Slf4j
@CrossOrigin({"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com"})
@RequiredArgsConstructor
@RestController
@MessageMapping("/game")
public class ApiGameController {

    @Value("${websocket.topic.state}")
    private String GAME_STATE_TOPIC;

    //region INJECTED PROPERTIES

    private final GameService gameService;
    private final SessionService sessionService;
    private final UserService userService;
    private final NotifierService notifierService;

    //endregion

    /**
     * Handles a request to retrieve a session state.
     *
     * @param sessionId Id of the session of which the state needs to be retrieved
     * @return Response containing the session state
     * @throws NoPlayersInSessionException Thrown when there are no players in the session.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Throw when the state can't be parsed.
     */
    @GetMapping("/game/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<JSONObject> requestState(@PathVariable("id") long sessionId)
            throws NoPlayersInSessionException, EntityNotFoundException, IOException, ParseException {

        log.info("GET: /game/{id}");

        Session session = sessionService.getSession(sessionId);
        return new ResponseEntity<>(Utils.convertSessionToJson(session), HttpStatus.OK);
    }

    /**
     * Handles a request coming from a lobby to start the game.
     *
     * @param principal Authenticated user that tries to initialize a game
     * @param dto       Data transfer object that contains initialization data
     * @throws NotEnoughPlayersException   Thrown when there are not enough players in this session to start.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/initialize")
    public void initialize(Principal principal, @Payload InitializeDto dto)
            throws NotEnoughPlayersException, NoPlayersInSessionException, ParseException, IOException, EntityNotFoundException, InvalidOperationException, PlayerNotInSessionException, InvalidPhaseException, InterruptedException, ExecutionControl.NotImplementedException {

        log.info("MESSAGE: /game/initialize from " + principal.getName());

        Session session = gameService.initialize(principal.getName(), dto.getSessionId(), dto.getMap());                // initialize the session
        notifyPlayersOfGameStateUpdate(session);                                                                        // notify all players in that session of an update
    }

    //region ACTIONS

    /**
     * Handles a request to perform an allocation.
     *
     * @param principal Authenticated user that tries to allocate troops
     * @param dto       Data transfer object that contains allocation data
     * @throws InvalidPhaseException       Thrown when the current turn is not in the allocation phase.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/allocate")
    public void allocate(Principal principal, @RequestBody AllocateDto dto)
            throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, ParseException, NoPlayersInSessionException, IOException {

        log.info("MESSAGE: /game/allocate from " + principal.getName());

        Session session = gameService.allocate(principal.getName(), dto);                                               // perform an allocation
        notifyPlayersOfGameStateUpdate(session);                                                                        // notify all players in that session of an update
    }

    /**
     * Handles a request to perform an attack.
     *
     * @param principal Authenticated user that tries to attack
     * @param dto       Data transfer object that contains attack data
     * @throws InvalidPhaseException       Thrown when the current turn is not in the attack phase.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/attack")
    public void attack(Principal principal, @RequestBody AttackDto dto)
            throws InvalidPhaseException, EntityNotFoundException, PlayerNotInSessionException, InvalidOperationException, NoPlayersInSessionException, IOException, ParseException {

        log.info("MESSAGE: /game/attack from " + principal.getName());

        Session session = gameService.attack(principal.getName(), dto);                                                 // perform an attack
        notifyPlayersOfGameStateUpdate(session);                                                                        // notify all players in that session of an update
    }

    /**
     * Handles a request to perform a fortification.
     *
     * @param principal Authenticated user that tries to fortify
     * @param dto       Data transfer object that contains fortification data
     * @throws InvalidPhaseException       Thrown when the current turn is not in the attack phase.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws InvalidOperationException   Thrown when an operation can't be executed.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/fortify")
    public void fortify(Principal principal, @RequestBody FortifyDto dto)
            throws InvalidPhaseException, InvalidOperationException, PlayerNotInSessionException, EntityNotFoundException, ParseException, NoPlayersInSessionException, IOException {

        log.info("MESSAGE: /game/fortify from " + principal.getName());

        Session session = gameService.fortify(principal.getName(), dto);                                                // perform a fortification
        notifyPlayersOfGameStateUpdate(session);                                                                        // notify all players in that session of an update
    }

    //endregion

    /**
     * Handles a request to phase the session into the next phase.
     *
     * @param principal Authenticated user that tries to start a next phase
     * @param sessionId Id of the session in which the game is played
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     * @throws InvalidPhaseException       Thrown when the current phase is invalid.
     * @throws PlayerNotInSessionException Thrown when the authenticated sender is not a participant.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws ParseException              Thrown when the state can't be parsed.
     */
    @MessageMapping("/phase/{sessionId}")
    public void phase(Principal principal, @DestinationVariable int sessionId)
            throws EntityNotFoundException, InvalidPhaseException, PlayerNotInSessionException, NoPlayersInSessionException, IOException, ParseException, InvalidOperationException, InterruptedException, ExecutionControl.NotImplementedException, NotPlayerTurnException {

        log.info("MESSAGE: /game/phase/{sessionId} from " + principal.getName());

        gameService.nextPhase(sessionService.getSession(sessionId), principal.getName());                               // start the next phase
        notifyPlayersOfGameStateUpdate(sessionService.getSession(sessionId));                                           // notify all players in that session of an update
    }

    /**
     * Handles an exception thrown by a websocket request.
     *
     * @param exception Thrown exception
     * @return Exception message
     */
    @MessageExceptionHandler
    @SendToUser("/topic/error")
    public String handleException(Throwable exception) {

        log.warn("SEND: /topic/error with exception " + exception.toString());

        return exception.getMessage();
    }

    /**
     * Notifies players of an updated session.
     *
     * @param session Session of which players will be notified
     * @throws IOException                 Thrown when an I/O error occurs.
     * @throws NoPlayersInSessionException Thrown when there are no players in this session.
     * @throws ParseException              Thrown when the state can't be parsed.
     * @throws EntityNotFoundException     Thrown when a required entity can't be found.
     */
    private void notifyPlayersOfGameStateUpdate(Session session)
            throws IOException, NoPlayersInSessionException, ParseException, EntityNotFoundException {

        notifierService.notify(session.getPlayers(), GAME_STATE_TOPIC, session);
    }
}
