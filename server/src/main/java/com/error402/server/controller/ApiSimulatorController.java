package com.error402.server.controller;

import com.error402.server.Simulator;
import com.error402.server.model.dto.SimulatorDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Handles all incoming calls concerning the simulator.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
@CrossOrigin({"http://localhost:4200", "https://upbeat-spence-1a0ea0.netlify.com"})
@RequiredArgsConstructor
@RestController
@RequestMapping("/simulator")
public class ApiSimulatorController {

    private final Simulator simulator;

    /**
     * Handles a request to simulate data.
     *
     * @param simulatorDto Data transfer object that contains simulation data
     * @return Empty response
     * @throws Exception Thrown when an error occurs.
     */
    @GetMapping
    public ResponseEntity<Void> simulate(@RequestBody SimulatorDto simulatorDto)
            throws Exception {

        log.info("GET: /simulator");

        simulator.start(simulatorDto);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
