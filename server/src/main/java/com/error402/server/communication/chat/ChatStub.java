package com.error402.server.communication.chat;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Handles communication to chat microservice.
 *
 * @author  Error 402
 * @version 0.2
 */
@Slf4j
public class ChatStub {

    @Value("${chat_url}")
    private String CHAT_URL;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Creates a chat for a session.
     *
     * @param sessionId   Id of the session for which a chat needs to be created
     * @param accessToken Token of the authenticated user who tries to create a session chat
     * @return True if the session chat is successfully created;
     *         False otherwise
     */
    public boolean createSessionChat(long sessionId, String accessToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject createSessionChatDto = new JSONObject();
        createSessionChatDto.put("sessionId", sessionId);

        HttpEntity<String> request = new HttpEntity<>(createSessionChatDto.toJSONString(), headers);
        ResponseEntity<String> result = restTemplate.postForEntity(CHAT_URL + "/chat/create-session-chat", request, String.class);

        log.info("Result from remote chat microservice: STATUS: " + result.getStatusCode() + " - BODY: " + result.getBody());

        return result.getStatusCode().is2xxSuccessful();
    }
}
