package com.error402.chat.dao;

import com.error402.chat.model.Chat;
import com.error402.chat.model.SessionChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Integer> {

    @Query(value = "select * from CHAT where CHAT_TYPE = 'GLOBAL' limit 1", nativeQuery = true)
    Chat getGlobalChat();

    @Query(value = "select * from CHAT where SESSION_ID = :sessionId", nativeQuery = true)
    Optional<Chat> findBySessionId(@Param("sessionId") long sessionId);
}
