package com.error402.chat.dao;

import com.error402.chat.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<ChatMessage, Integer> {
    List<ChatMessage> findByChatId(int chatId);
}
