package com.error402.chat.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("GLOBAL")
@Getter
@Setter
public class GlobalChat extends Chat {
}
