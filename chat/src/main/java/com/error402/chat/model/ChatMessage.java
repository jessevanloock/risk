package com.error402.chat.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Getter
@Setter
public class ChatMessage {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(length = 50, nullable = false)
    private String username;

    @Column(length = 250, nullable = false)
    private String message;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "CHAT_ID", foreignKey = @ForeignKey(name = "FK_MESSAGE_CHAT_ID"))
    private Chat chat;
}
