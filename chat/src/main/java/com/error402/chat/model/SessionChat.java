package com.error402.chat.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SESSION")
@Getter
@Setter
public class SessionChat extends Chat {
    @Column(name = "SESSION_ID")
    private long sessionId;
}
