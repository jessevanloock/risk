package com.error402.chat.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateSessionChatDto {
    private long sessionId;
}
