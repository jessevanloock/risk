package com.error402.chat.service;

import com.error402.chat.dao.ChatRepository;
import com.error402.chat.dao.MessageRepository;
import com.error402.chat.model.Chat;
import com.error402.chat.model.GlobalChat;
import com.error402.chat.model.ChatMessage;
import com.error402.chat.model.SessionChat;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ChatService {

    private final ChatRepository chatRepository;
    private final MessageRepository messageRepository;

    public ChatService(ChatRepository chatRepository, MessageRepository messageRepository) {
        this.chatRepository = chatRepository;
        this.messageRepository = messageRepository;
        createGlobalChatIfNotExists();
    }

    public Chat createGlobalChatIfNotExists() {
        // Only supposed to be called once!
        Chat chat = new GlobalChat();
        return chatRepository.save(chat);
    }

    public void createSessionChatIfNotExists(long sessionId) throws Exception {
        Optional<Chat> chatOptional = chatRepository.findBySessionId(sessionId);

        if (chatOptional.isPresent()) {
            throw new Exception("SessionChat already exists");
        }

        SessionChat sessionChat = new SessionChat();
        sessionChat.setSessionId(sessionId);
        chatRepository.save(sessionChat);

    }

    public ChatMessage createMessage(int chatId, String text, String username) {

        Chat chat = this.getChat(chatId);
        ChatMessage createdMessage = new ChatMessage();
        createdMessage.setChat(chat);
        createdMessage.setMessage(text);
        createdMessage.setUsername(username);
        chat.getMessages().add(createdMessage);
        chatRepository.save(chat);


        //CreatedMessage id is null, something with detached...?
        return createdMessage;
    }

    public Chat getChat(int chatId) {
        return chatRepository.findById(chatId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find chat"));
    }


    public Chat getSessionChat(long sessionId) {
        return chatRepository.findBySessionId(sessionId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find chat for this session"));
    }

    public Chat getGlobalChat() {
        return chatRepository.getGlobalChat();
    }


    public List<ChatMessage> getMessages(int chatId) {
        return messageRepository.findByChatId(chatId);
    }
}
