package com.error402.chat.controller;

import com.error402.chat.model.dto.CreatedMessageDto;
import com.error402.chat.model.dto.MessageDto;
import com.error402.chat.model.ChatMessage;
import com.error402.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

import java.security.Principal;

@AllArgsConstructor
@Controller
@MessageMapping("/chat")
public class ChatWSController {
    private final ChatService chatService;
    private final SimpMessageSendingOperations simpTemplate;

    @MessageMapping("/send-message")
    public void send(@Payload MessageDto dto, Principal principal) {
        System.out.println("Message: " + dto.toString());

        ChatMessage message = chatService.createMessage(dto.getChatId(), dto.getMessage(), principal.getName());

        simpTemplate.convertAndSend("/topic/" + message.getChat().getId().toString() + "/messages",
                new CreatedMessageDto(message.getUsername(), message.getMessage()));
    }
}
