package com.error402.chat.controller;


import com.error402.chat.exception.EntityNotFoundException;
import com.error402.chat.model.Chat;
import com.error402.chat.model.dto.CreateSessionChatDto;
import com.error402.chat.service.ChatService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin(origins = {
        "http://localhost:4200",
        "https://upbeat-spence-1a0ea0.netlify.com",
        "http://localhost:9001",
        "https://game-dot-error-402-risk.appspot.com"
})
@RequestMapping("/chat")
@RestController
public class ChatRestController {
    private final ChatService chatService;

    public ChatRestController(ChatService chatService) {
        this.chatService = chatService;
    }


    /**
     * @param id the ID of the chat to be found.
     * @return The data transfer object representing the found chat object.
     * @throws EntityNotFoundException Thrown when a chat is not found.
     */
    @GetMapping("/{id}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Chat> getChat(@PathVariable int id) throws EntityNotFoundException {
        Chat found = chatService.getChat(id);

        if (found == null) {
            throw new EntityNotFoundException("No chat found for chatId: " + id);
        }
        return new ResponseEntity<>(found, HttpStatus.OK);
    }


    /**
     * @return The data transfer object representing the found global chat object.
     * @throws EntityNotFoundException Thrown when a chat is not found.
     */

    @GetMapping("/global-chat")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Chat> getGlobalChat() throws EntityNotFoundException {
        Chat found = chatService.getGlobalChat();

        if (found == null) {
            throw new EntityNotFoundException("No global chat found");
        }
        return new ResponseEntity<>(found, HttpStatus.OK);
    }


    /**
     * @param sessionId the session id of a session to get the session chat.
     * @return The data transfer object representing the found session chat object.
     * @throws EntityNotFoundException Thrown when a chat is not found with the given sessionId.
     */
    @GetMapping("/session-chat/{sessionId}")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Chat> getSessionChat(@PathVariable long sessionId) throws EntityNotFoundException {
        Chat found = chatService.getSessionChat(sessionId);

        if (found == null) {
            throw new EntityNotFoundException("No chat found for sessionId: " + sessionId);
        }
        return new ResponseEntity<>(found, HttpStatus.OK);
    }


    //TODO This needs to happen on launch.
    @PostMapping("/create-global-chat")
    public ResponseEntity<Chat> createGlobalChat(Principal principal) {
        Chat createdChat = chatService.createGlobalChatIfNotExists();
        return new ResponseEntity<>(createdChat, HttpStatus.CREATED);
    }


    /**
     *
     * @param dto The data transfer object representing the chat that needs to be created.
     * @return the created chat.
     */
    @PostMapping("/create-session-chat")
    @PreAuthorize("#oauth2.hasAnyScope('read')")
    public ResponseEntity<Void> createSessionChat(@RequestBody CreateSessionChatDto dto) {
        try {
            chatService.createSessionChatIfNotExists(dto.getSessionId());
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
