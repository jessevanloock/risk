package com.error402.chat.service;

import com.error402.chat.ChatApplication;
import com.error402.chat.dao.ChatRepository;
import com.error402.chat.dao.MessageRepository;
import com.error402.chat.model.Chat;
import com.error402.chat.model.ChatMessage;
import com.error402.chat.model.SessionChat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ChatApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ChatServiceIntegrationTest {

    @Autowired
    private ChatService chatService;

    @MockBean
    private ChatRepository chatRepository;

    @MockBean
    private MessageRepository messageRepository;

    @BeforeEach
    public void setUp() {
        Chat sessionChat = new SessionChat();
        sessionChat.setId(1);
        ChatMessage message = new ChatMessage();
        message.setMessage("message");
        sessionChat.getMessages().add(message);

        Mockito.when(messageRepository.findByChatId(1))
                .thenReturn(sessionChat.getMessages());

        Mockito.when(chatRepository.findById(1))
                .thenReturn(Optional.of(sessionChat));

    }

    // write test cases here
    @Test
    public void whenValidSessionChatId_thenMessagesShouldBeFound() {
        int sessionChatId = 1;
        List<ChatMessage> found = chatService.getMessages(sessionChatId);

        assertThat(found.get(0).getMessage())
                .isEqualTo("message");
    }

    @Test
    public void whenValidChatId_thenSessionChatShouldBeFound() {
        int chatId = 1;
        Chat found = chatService.getChat(chatId);

        assertThat(found.getId())
                .isEqualTo(1);
    }
}
