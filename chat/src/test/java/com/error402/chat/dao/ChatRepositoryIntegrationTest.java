package com.error402.chat.dao;

import com.error402.chat.model.Chat;
import com.error402.chat.model.GlobalChat;
import com.error402.chat.model.ChatMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ChatRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ChatRepository chatRepository;

    // write test cases here
    @Test
    public void whenMessageSend_thenReturnMessages() {
        // given
        Chat chat = new GlobalChat();
        ChatMessage message = new ChatMessage();
        message.setChat(chat);
        message.setMessage("Message");
        message.setUsername("bob");
        chat.getMessages().add(message);

        entityManager.persist(chat);
        entityManager.flush();


        // when
        List<ChatMessage> found = Objects.requireNonNull(chatRepository.findById(1).orElse(null)).getMessages();
        found.sort(Comparator.comparing(ChatMessage::getId));
        // then
        assertThat(found.get(found.size() - 1).getChat() == chat);
        assertThat(found.get(found.size() - 1).getMessage().equals("Message"));
        assertThat(found.get(found.size() - 1).getUsername().equals("bob"));
    }
}
