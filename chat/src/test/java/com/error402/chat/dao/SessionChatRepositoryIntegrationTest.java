package com.error402.chat.dao;

import com.error402.chat.model.Chat;
import com.error402.chat.model.ChatMessage;
import com.error402.chat.model.GlobalChat;
import com.error402.chat.model.SessionChat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class SessionChatRepositoryIntegrationTest {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MessageRepository messageRepository;

    // write test cases here
    @Test
    public void whenFindById_thenReturnSessionChat() {
        // given
        Chat savedChat = chatRepository.save(new SessionChat());
        chatRepository.flush();

        // when
        Chat found = chatRepository.findById(savedChat.getId()).orElseThrow();

        // then
        assertThat(found.getId().equals(savedChat.getId()));
        assertThat(found.getClass().equals(SessionChat.class));
    }

    @Test
    public void whenFindById_thenReturnGlobalChat() {
        // given
        Chat savedChat = chatRepository.save(new GlobalChat());
        chatRepository.flush();

        // when
        Chat found = chatRepository.findById(savedChat.getId()).orElseThrow();

        // then
        assertThat(found.getId().equals(savedChat.getId()));
        assertThat(found.getClass().equals(GlobalChat.class));
    }

    @Test
    public void whenFindBySessionId_thenReturnSessionChat() {
        // given
        SessionChat sessionChat = new SessionChat();
        sessionChat.setSessionId(5);
        chatRepository.save(sessionChat);
        chatRepository.flush();

        // when
        SessionChat found = (SessionChat) chatRepository.findBySessionId(5).orElseThrow();

        // then

        assertThat(found.getSessionId())
                .isEqualTo(sessionChat.getSessionId());

    }

    @Test
    public void whenFindById_thenReturnMessages() {
        // given
        Chat sessionChat = new SessionChat();
        for (int i = 0; i < 20; i++) {
            ChatMessage message = new ChatMessage();
            message.setChat(sessionChat);
            message.setMessage("Message: " + i);
            message.setUsername("bob");
            sessionChat.getMessages().add(message);
        }
        chatRepository.save(sessionChat);
        chatRepository.flush();


        // when
        List<ChatMessage> found = Objects.requireNonNull(messageRepository.findByChatId(sessionChat.getId()));
        found.sort(Comparator.comparing(ChatMessage::getId));
        // then
        for (int i = 0; i < 20; i++) {
            assertThat(found.get(i).getChat() == sessionChat);
            assertThat(found.get(i).getMessage().equals("Message: " + i));
            assertThat(found.get(i).getUsername().equals("bob"));
        }

    }


}
