package com.error402.chat.controller;

import com.error402.chat.model.dto.MessageDto;
import com.error402.chat.model.Chat;
import com.error402.chat.model.GlobalChat;
import com.error402.chat.model.ChatMessage;
import com.error402.chat.model.SessionChat;
import com.error402.chat.service.ChatService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Disabled
@ActiveProfiles("test")
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ApiControllerTest {

    private final String AUTH_URL = "https://auth-dot-inductive-world-267715.appspot.com";
    private final String CHAT_URL = "http://localhost:9002";
    @Value("oauth_credentials_path")
    private String credentialsFilePath;

    @Autowired
    private MockMvc mvc;


    @Value("oauth.client.id")
    private String CLIENTID;
    @Value("oauth.client.password")
    private String CLIENT_PASSWORD;
    @Value("oauth.user.username")
    private String USERNAME;
    @Value("oauth.user.password")
    private String PASSWORD;


    private String TOKEN;

    @MockBean
    private ChatService chatService;


    @Autowired
    TestRestTemplate restTemplate;


    @BeforeAll
    void setUp() throws URISyntaxException, IOException, JSONException {

        restTemplate = new TestRestTemplate();
        String authString = CLIENTID + ":" + CLIENT_PASSWORD;


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Basic " + Base64.getEncoder().encodeToString(authString.getBytes()));


        HttpEntity<String> request = new HttpEntity<>(headers);

        String response = restTemplate.postForObject(String.format("%s/oauth/token?grant_type=password&username=%s&password=%s", AUTH_URL, USERNAME, PASSWORD), request, String.class);
        JSONObject jsonObject = new JSONObject(response);
        TOKEN = jsonObject.getString("access_token");
    }

    @Test
    void createGlobalChat() {
        try {
            mvc.perform(post(CHAT_URL + "/create-global-chat").header("Authorization", "Bearer " + TOKEN)).andDo(print()).andExpect(status().isCreated());
        } catch (Exception ignored) {
            fail();
        }

    }

    @Test
    void createSessionChat() {
        try {
            mvc.perform(post(CHAT_URL + "/create-session-chat").header("Authorization", "Bearer " + TOKEN)).andDo(print()).andExpect(status().isCreated());
        } catch (Exception ignored) {
            fail();
        }
    }

    @Test
    void sendMessageToGlobalChat() {

        Chat globalChat = new GlobalChat();
        globalChat.setId(1);

        MessageDto messageToSend = new MessageDto();

        messageToSend.setMessage("Hello World");
        messageToSend.setChatId(1);


        ChatMessage messageToReceive = new ChatMessage();

        messageToReceive.setMessage("Hello World");
        messageToReceive.setUsername(USERNAME);
        messageToReceive.setChat(globalChat);


        given(chatService.getChat(globalChat.getId())).willReturn(globalChat);


        //Weird way of testing, do we need to check if the message is actually added to the list of the chat? or is that out of scope of the controller testing.
        given(chatService.createMessage(globalChat.getId(), messageToSend.getMessage(), USERNAME)).willReturn(messageToReceive);
        try {
            mvc.perform(post(CHAT_URL + "/send-message").content(asJsonString(messageToSend)).header("Authorization", "Bearer " + TOKEN).contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(jsonPath("text").value("Hello World")).andExpect(status().isCreated());
        } catch (Exception ignored) {
            fail();
        }
    }


    @Test
    void getChat() {
        Chat chat = new SessionChat();
        chat.setId(1);
        given(chatService.getChat(1)).willReturn(chat);
        try {
            mvc.perform(get(CHAT_URL + "/chat/" + chat.getId()).header("Authorization", "Bearer " + TOKEN).contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());
        } catch (Exception ignored) {
            fail();
        }
    }

    @Test
    void getGlobalChat() {
        GlobalChat chat = new GlobalChat();
        chat.setId(10);
        given(chatService.getGlobalChat()).willReturn(chat);
        try {
            mvc.perform(get(CHAT_URL + "/global-chat").header("Authorization", "Bearer " + TOKEN).contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());
        } catch (Exception ignored) {
            fail();
        }

    }

    @Test
    void getSessionChat() {
        SessionChat chat = new SessionChat();
        chat.setSessionId(100);
        given(chatService.getSessionChat(chat.getSessionId())).willReturn(chat);
        try {
            mvc.perform(get(CHAT_URL + "/session-chat/" + chat.getSessionId()).header("Authorization", "Bearer " + TOKEN).contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());
        } catch (Exception ignored) {
            fail();
        }
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
