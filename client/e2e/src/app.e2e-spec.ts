import {RegisterPage} from './app.po';

describe('workspace-project App', () => {
  let page: RegisterPage;

  beforeEach(() => {
    page = new RegisterPage();
  });

  it('should have text Register', () => {
    page.navigateTo();
    expect(page.getRegisterTitleText()).toEqual('Register');
  });
});
