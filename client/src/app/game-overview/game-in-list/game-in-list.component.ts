import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../../services/game.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {GameLog} from '../../../models/game-log';

@Component({
  selector: 'app-game-in-list',
  templateUrl: './game-in-list.component.html',
  styleUrls: ['./game-in-list.component.scss']
})
export class GameInListComponent implements OnInit {
  @Input() sessionId: number;
  gameLog: GameLog;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private gameService: GameService,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.sessionId = JSON.parse(params['sessionId']);
        this.gameService.getOverviewOfGame(this.sessionId).subscribe(
          res => {
            this.gameLog = res;
            console.log(res);
          }
        );
      });
  }

  personal() {
      this.router.navigate(['personal']);
  }

  startReplay() {
    this.router.navigate(['replay'], {queryParams: {sessionId: this.sessionId}});
  }
}
