import {Component, OnInit} from '@angular/core';
import {Player} from '../../../models/player';
import {Continent} from '../../../models/continent';
import {GameState} from '../../../models/game-state';
import {animate, AnimationEvent, style, transition, trigger} from '@angular/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from '../../../services/game.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {GameLog} from '../../../models/game-log';

@Component({
  selector: 'app-replay',
  templateUrl: './replay.component.html',
  styleUrls: ['./replay.component.scss'],
  animations: [
    trigger('phaseAnimation', [
      transition(':enter', [
        style({opacity: 0}),
        animate('1500ms', style({opacity: 1})),
      ])
    ]),
  ]
})
export class ReplayComponent implements OnInit {
  phase = 'ALLOCATION';
  currentPlayer: Player = null;
  continents: Continent[];
  players: Player[];
  extras: string[];
  allocationIndicator: string;
  attackIndicator: string;
  fortificationIndicator: string;
  stateQueue: GameState[] = [];
  stateAnimationsFinished: boolean;
  phaseAnimationFinished: boolean;
  sessionId: number;
  gameLog: GameLog;
  pause = false;
  mapName: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private gameService: GameService,
              private authService: AuthenticationService) {
  }

  /**
   * Called on Initialize of replay component
   */
  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.sessionId = JSON.parse(params['sessionId']);
        this.gameService.getOverviewOfGame(this.sessionId).subscribe(
          res => {
            this.gameLog = res;
            this.fillStateQueue();
          }
        );
      });
  }

  /**
   * Loops trough the action, creates the state for each action and ads them to the state queue
   */
  private fillStateQueue() {
    console.log(this.gameLog);
    // let prevState: GameState = JSON.parse(JSON.stringify(this.gameLog.startState));
    let prevState: GameState = this.gameLog.startstate;
    this.gameLog.log.forEach(action => {
      // let actionState: GameState = JSON.parse(JSON.stringify(prevState));
      let actionState: GameState = prevState;
      if (action.allocation != null) {
        // allocation
        actionState.turn.phase = 'ALLOCATION';
        actionState.turn.username = action.allocation.player;
        actionState.continents.forEach(continent => {
          continent.territories.forEach(territory => {
            if (territory.name === action.allocation.territory) {
              territory.troops = territory.troops + action.allocation.troops;
            }
          });
        });
      } else if (action.attack != null) {
        // attack
        actionState.turn.phase = 'ATTACK';
        actionState.turn.username = action.attack.attacker;
        actionState.continents.forEach(continent => {
          continent.territories.forEach(territory => {
            if (territory.name === action.attack.attackingTerritory) {
              if (action.attack.attackSuccessful === true) {
                territory.troops = 1;
              } else {
                territory.troops = action.attack.attackingTroopsAtEnd;
              }
            }
            if (territory.name === action.attack.defendingTerritory) {
              if (action.attack.attackSuccessful === true) {
                territory.troops = action.attack.attackingTroopsAtEnd - 1;
                territory.player = action.attack.attacker;
              } else {
                territory.troops = action.attack.defendingTroopsAtEnd;
              }
            }
          });
        });

      } else if (action.fortification != null) {
        // fortification
        actionState.turn.phase = 'FORTIFICATION';
        actionState.turn.username = action.fortification.player;
        actionState.continents.forEach(continent => {
          continent.territories.forEach(territory => {
            if (territory.name === action.fortification.fortifyingTerritory) {
              territory.troops = territory.troops - action.fortification.troops;
            }
            if (territory.name === action.fortification.fortifiedTerritory) {
              territory.troops = territory.troops + action.fortification.troops;
            }
          });
        });
      } else if (action.startPhaseAction != null) {
        actionState.turn.phase = action.startPhaseAction.phasetype;
        actionState.turn.username = action.startPhaseAction.player;
      }
      this.stateQueue.push(JSON.parse(JSON.stringify(actionState)));
      prevState = JSON.parse(JSON.stringify(actionState));
    });
    this.initializeGame(this.gameLog.startstate);
  }

  /**
   * Gets called when the state animations in the map component are started or finished
   * @param value    false: if state animations are started, true: if state animations are finished
   */
  setStateAnimationsFinished(value: boolean) {
    this.stateAnimationsFinished = value;
    if (this.stateAnimationsFinished === true && this.phaseAnimationFinished === true && this.pause === false) {
      if (this.stateQueue.length > 0) {
        this.initializeGame(this.stateQueue[0]);
        this.stateQueue.shift();
      }
    }
  }

  /**
   * Gets called when the phase animations is started or finished
   * @param $event      The animation itself
   * @param value       false: if phase animation is started, true: if phase animation is finished
   */
  setPhaseAnimationFinishedState($event: AnimationEvent, value: boolean) {
    if ($event.fromState === 'void') {
      this.phaseAnimationFinished = value;
      if (this.phaseAnimationFinished === true && this.stateAnimationsFinished === true && this.pause === false) {
        if (this.stateQueue.length > 0) {
          this.initializeGame(this.stateQueue[0]);
          this.stateQueue.shift();
        }
      }
    }
  }

  /**
   * Initialize the game for the given state
   * @param resState      The game state that has to be initialized
   */
  private initializeGame(resState: GameState) {
    this.phaseAnimationFinished = false;
    this.stateAnimationsFinished = false;
    this.players = resState.players;
    this.continents = resState.continents;
    this.extras = resState.extras;
    this.mapName = resState.map;
    this.currentPlayer = this.players.find(p => p.username === resState.turn.username);
    this.setPhase(resState.turn.phase);
  }

  /**
   * Sets the phase based on the phase of the state that is being initialized.
   * Handles related things to the state that have to be done (ex. sets the right indicator)
   * @param phase       The phase of the state that is being initialized
   */
  private setPhase(phase: string) {
    if (this.phase !== phase) {
      this.phase = phase;
    } else {
      this.phaseAnimationFinished = true;
    }
    this.allocationIndicator = null;
    this.attackIndicator = null;
    this.fortificationIndicator = null;
    switch (this.phase) {
      case 'ALLOCATION':
        this.allocationIndicator = 'current';
        break;
      case 'ATTACK':
        this.attackIndicator = 'current';
        break;
      case 'FORTIFICATION':
        this.fortificationIndicator = 'current';
    }
  }

  /**
   * Navigate to game overview
   */
  statsPersonal() {
    this.router.navigate(['stats-personal']);
  }

  /**
   * Pauze and unpauze the replay
   */
  togglePause() {
    this.pause = !this.pause;
    if (this.phaseAnimationFinished === true && this.stateAnimationsFinished === true && this.pause === false) {
      if (this.stateQueue.length > 0) {
        this.initializeGame(this.stateQueue[0]);
        this.stateQueue.shift();
      }
    }
  }
}
