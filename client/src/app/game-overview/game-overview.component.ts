import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {GameService} from '../../services/game.service';
import {NotifierService} from 'angular-notifier';
import {OverviewInvite} from '../../models/gameoverview/invite';
import {OverviewLobby} from '../../models/gameoverview/lobby';
import {OverviewSession} from '../../models/gameoverview/session';

@Component({
  selector: 'app-game-overview',
  templateUrl: './game-overview.component.html',
  styleUrls: ['./game-overview.component.scss']
})
export class GameOverviewComponent implements OnInit {
  loading = true;
  status = 'Fetching data';
  selected;
  invites: OverviewInvite[];
  lobbies: OverviewLobby[];
  conquests: OverviewSession[];

  constructor(private router: Router,
              private gameService: GameService,
              private notifier: NotifierService) {
  }

  /**
   * Called on initialize of the game-overview component
   */
  ngOnInit() {
    this.refresh();
  }

  /**
   * Gets the items from the server
   */
  refresh() {
    this.loading = true;
    this.status = 'Loading your overview';
    this.gameService.getGameOverviewInvites()
      .subscribe(
        invites => this.invites = invites,
        error => {
          this.home();
          console.log(error);
          this.notifier.notify('error', 'We were unable to load your overview');
        },
        () => this.gameService.getGameOverviewLobbies()
          .subscribe(
            lobbies => this.lobbies = lobbies,
            () => {
              this.home();
              this.notifier.notify('error', 'We were unable to load your overview');
            },
            () => this.gameService.getGameOverviewPlaying()
              .subscribe(
                conquests => this.conquests = conquests,
                () => {
                  this.home();
                  this.notifier.notify('error', 'We were unable to load your overview');
                },
                () => this.loading = false
              )
          )
      )
  }

  /**
   * Sends a request to the server for accepting an invite
   * @param sessionId         Id of the session of the invite
   */
  acceptInvite(sessionId) {
    this.loading = true;
    this.status = 'Accepting invite';
    setTimeout(() => {
      this.gameService.joinSession(sessionId).subscribe(
        res => {
          this.joinLobby(sessionId);
        }, () => this.loading = false
      );
      this.loading = false;
    }, 1000);
  }

  /**
   * Navigete to the lobby when the invite accept request is succeeded
   * @param sessionId       Id of the session
   */
  joinLobby(sessionId) {
    this.loading = true;
    this.status = 'Joining lobby';
    setTimeout(() => {
      this.router.navigate(['lobby'], {queryParams: {id: sessionId}});
      this.loading = false;
    }, 500);
  }

  /**
   * Navigate to the game of an active game
   * @param sessionId       Id of the session
   */
  resumeGame(sessionId) {
    this.loading = true;
    this.status = 'Rejoining game';
    setTimeout(() => {
      this.router.navigate(['game'], {queryParams: {sessionId: sessionId}});
    }, 500);
  }

  /**
   * Start a new lobby and navigate to the session
   */
  startGame() {
    this.loading = true;
    this.status = 'Creating a new Lobby';
    setTimeout(() => {
      this.gameService.startNewSession()
        .subscribe(
          res_session => {
            this.router.navigate(['lobby'], {queryParams: {id: res_session}});
            this.loading = false;
          }, () => this.loading = false
        );
    }, 500);
  }

  /**
   * Navigate to the home page
   */
  home() {
    this.router.navigate(['home']);
  }
}
