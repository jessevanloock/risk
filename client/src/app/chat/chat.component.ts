import {AfterViewChecked, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {ChatService} from '../../services/chat.service';
import {ChatMessage} from '../../models/chat-message';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy, AfterViewChecked {
  @Input() sessionId: number = null;
  display = 'hidden';
  chats: any;
  globalChat: ChatMessage[] = [];
  sessionChat: ChatMessage[] = [];
  tab = 'global';
  message: string;
  characters = 250;
  private sessionChatId: number;
  private globalChatId: number;

  constructor(private authService: AuthenticationService, private chatService: ChatService, private router: Router) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.connectToChat(this.sessionId);
    } else {
      this.router.navigate(['home']);
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  toggleDisplay() {
    if (this.display === 'visible') {
      this.display = 'hidden';
    } else {
      this.display = 'visible';
    }
  }

  switchTab(tab: string) {
    if (tab === 'global' || tab === 'session') {
      this.tab = tab;
    }
  }

  sendMessage() {
    if (this.tab === 'global' && this.message != null) {
      this.chatService.sendChatMessage(this.globalChatId, this.message);
    } else if (this.tab === 'session' && this.message != null) {
      this.chatService.sendChatMessage(this.sessionChatId, this.message);
    }

    this.message = null;
    this.characters = 250;
  }

  scrollToBottom() {
    this.chats = document.getElementsByClassName('chat-content');
    for (let chat of this.chats) {
      chat.scrollTop = chat.scrollHeight;
    }
  }

  messageChanged() {
    this.characters = 250 - this.message.length;
  }


  private connectToChat(sessionId) {
    this.chatService.connectToChatChange();

    this.chatService.getGlobalChat().subscribe(
      res => {
        this.globalChat = res.messages;

        this.chatService.onWebsocketError()
          .subscribe(
            () => {
            });

        this.globalChatId = res.id;

        this.chatService.onChatChange(this.globalChatId).subscribe(
          resChat => {
            console.log(resChat);

            console.log(resChat.body);

            this.globalChat.push(new ChatMessage(
              JSON.parse(resChat.body).username,
              JSON.parse(resChat.body).message
            ));
          }
        );
      }
    );

    if (sessionId !== null) {
      this.chatService.getSessionChat(sessionId).subscribe(
        res => {
          this.sessionChat = res.messages;

          this.chatService.onWebsocketError()
            .subscribe(
              () => {
              });

          this.sessionChatId = res.id;

          this.chatService.onChatChange(this.sessionChatId).subscribe(
            resChat => {
              this.sessionChat.push(new ChatMessage(
                JSON.parse(resChat.body).username,
                JSON.parse(resChat.body).message
              ));
            }
          );
        }
      );
    }
  }

  ngOnDestroy(): void {
    this.chatService.disconnectFromChatChange();
  }
}
