import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  UserData = {
    password: undefined,
    username: undefined
  };

  constructor(private router: Router, private authService: AuthenticationService, private notifier: NotifierService) {
  }

  ngOnInit() {
  }

  home() {
    this.router.navigate(['home']);
  }

  loginUser() {
    this.authService.login(this.UserData.username, this.UserData.password).subscribe(
      resToken => {
        this.authService.currentUser = this.UserData.username;
        this.notifier.notify('success', 'Welcome back, ' + this.UserData.username + '!');
        console.log('token success');
        console.log(resToken);
        localStorage.setItem('RiskRefreshToken', resToken.refresh_token);
        sessionStorage.setItem('RiskAccessToken', resToken.access_token);
        this.home();
      }
    );
  }

  register() {
    this.router.navigate(['register']);
  }

  signInWithGoogle() {
    this.authService.signInWithGoogle();
  }

  signInWithFacebook() {
    this.notifier.notify('info', 'This feature was postponed to a later version')
  }
}
