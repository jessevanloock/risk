import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {GameService} from '../../../services/game.service';
import {NotifierService} from 'angular-notifier';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  confirmPassword: any;
  UserData = {
    username: undefined,
    password: undefined,
  };

  constructor(private router: Router,
              private authService: AuthenticationService,
              private gameService: GameService,
              private notifier: NotifierService,
              private googleService: AuthService) {
  }

  ngOnInit() {
  }

  home() {
    this.router.navigate(['home']);
  }

  registerUser() {
    this.authService.createUser(this.UserData.username, this.UserData.password)
      .subscribe(
        res => {
          console.log('register success');
          console.log(res);
          this.authService.login(this.UserData.username, this.UserData.password)
            .subscribe(
              resToken => {
                this.authService.currentUser = this.UserData.username;
                localStorage.setItem('RiskRefreshToken', resToken.refresh_token);
                sessionStorage.setItem('RiskAccessToken', resToken.access_token);
                this.gameService.createUser().subscribe(
                  resUser => {
                    console.log('success user');
                    console.log(resUser);
                    this.notifier.notify('success', 'Your account has been created!');
                  }
                );
                this.home();
              }
            );
        });
  }

  signInWithGoogle() {
    this.authService.signInWithGoogle();
  }

  signInWithFacebook() {
    this.notifier.notify('info', 'This feature was postponed to a later version')
  }
}
