import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {GameService} from '../../services/game.service';
import {Friendship} from '../../models/friendship';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  loading: boolean;
  status: string;
  friends: Friendship[] = [];
  friendRequests: Friendship[] = [];
  newFriendName: string;

  constructor(private router: Router,
              private authService: AuthenticationService,
              private gameService: GameService,
              private notifier: NotifierService) {
  }

  ngOnInit() {
    this.refresh();
  }

  private refresh() {
    this.loading = true;
    this.status = "Looking for your Companions";
    setTimeout(() => {
      this.gameService.getFriendRequests().subscribe(
        res => {
          res.forEach(f => {
            if (f.confirmed) this.friends.push(f);
            else this.friendRequests.push(f);
          });
          this.loading = false;
        }, () => this.loading = false
      );
    }, 1000);
  }

  addFriend() {
    this.gameService.sendFriendRequest(this.newFriendName).subscribe(
      res => {
        this.friendRequests.push(res);
      }
    );
  }

  removeFriendship(friendship: Friendship) {
    this.gameService.deleteFriendship(friendship.friendshipId).subscribe(
      () => {
        if (this.friendRequests.includes(friendship)) {
          let index = this.friendRequests.findIndex(f => f === friendship);
          this.friendRequests.splice(index, 1);
        } else {
          let index = this.friends.findIndex(f => f === friendship);
          this.friends.splice(index, 1);
        }
      }
    );
  }

  confirmFriendship(friendship: Friendship) {
    this.gameService.confirmFriendship(friendship.friendshipId).subscribe(
      () => {
        friendship.confirmed = true;
        let index = this.friendRequests.findIndex(f => f === friendship);
        this.friendRequests.splice(index, 1);
        this.friends.push(friendship);
      }
    );
  }

  home() {
    this.router.navigate(['home']);
  }
}
