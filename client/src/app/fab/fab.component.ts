import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
})
export class FabComponent implements OnInit {

  toggle = 'inactive';
  muted = false;

  constructor(public authService: AuthenticationService, private router: Router, private notifier: NotifierService) {
  }

  ngOnInit() {
  }

  toggleFab() {
    if (this.toggle === '') {
      this.toggle = 'inactive';
    } else {
      this.toggle = '';
    }
  }

  toggleMute() {
    this.muted = !this.muted;
  }

  logout() {
    this.toggleFab();
    this.authService.logout();
  }
}
