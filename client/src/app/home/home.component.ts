import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {GameService} from '../../services/game.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loading = true;
  status = 'Returning home';

  constructor(private router: Router,
              public authService: AuthenticationService,
              private gameService: GameService) {
  }

  ngOnInit() {

    setTimeout(() => this.loading = false, 500);
  }

  startGame() {
    this.loading = true;
    this.status = 'Creating lobby';
    setTimeout(() => {
      this.gameService.startNewSession()
        .subscribe(
          res_session => {
            this.router.navigate(['lobby'], {queryParams: {id: res_session}});
          }, error => {
            this.loading = false;
          }
        );
    }, 500);

  }

  gameOverview() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['overview']);
    } else {
      this.router.navigate(['login']);
    }
  }

  profile() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['profile']);
    } else {
      this.router.navigate(['login']);
    }
  }

  friends() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['friends']);
    } else {
      this.router.navigate(['login']);
    }
  }

  logIn() {
    this.router.navigate(['login']);
  }

  register() {
    this.router.navigate(['register']);
  }

  statsPersonal() {
    this.router.navigate(['stats-personal']);
  }

  statsGlobal() {
    this.router.navigate(['stats-global']);
  }
}
