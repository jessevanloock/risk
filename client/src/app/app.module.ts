import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MapComponent} from './map/map.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {GameOverviewComponent} from './game-overview/game-overview.component';
import {LobbyComponent} from './session/lobby/lobby.component';
import {LoginComponent} from './authentication/login/login.component';
import {RegisterComponent} from './authentication/register/register.component';
import {FriendsComponent} from './friends/friends.component';
import {FormsModule} from '@angular/forms';
import {GameInListComponent} from './game-overview/game-in-list/game-in-list.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {GameComponent} from './session/game/game.component';
import {NotifierModule} from 'angular-notifier';
import {SidebarModule} from 'ng-sidebar';
import {FabComponent} from './fab/fab.component';
import {ChatComponent} from './chat/chat.component';
import {PersonalComponent} from './stats/personal/personal.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReplayComponent } from './game-overview/replay/replay.component';
import { TimeagoModule } from 'ngx-timeago';
import {AuthServiceConfig, GoogleLoginProvider,SocialLoginModule} from 'angularx-social-login';
import {HttpErrorInterceptor} from '../error/http-error-interceptor';
import { GlobalComponent } from './stats/global/global.component';

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'overview', component: GameOverviewComponent},
  {path: 'overview/game', component: GameInListComponent},
  {path: 'lobby', component: LobbyComponent},
  {path: 'game', component: GameComponent},
  {path: 'friends', component: FriendsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'stats-personal', component: PersonalComponent},
  {path: 'stats-global', component: GlobalComponent},
  {path: 'replay', component: ReplayComponent},
  {path: '', component: HomeComponent},
  {path: '**', component: HomeComponent}
];

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('423413817828-b62ohv5b8ch6vsjhce62qcon0g6a5c28')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    HomeComponent,
    GameOverviewComponent,
    LobbyComponent,
    LoginComponent,
    RegisterComponent,
    FriendsComponent,
    GameInListComponent,
    GameComponent,
    FabComponent,
    ChatComponent,
    PersonalComponent,
    ReplayComponent,
    GlobalComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes, {enableTracing: false}
    ),
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    NotifierModule.withConfig({
      position: {
        horizontal: {
          position: 'middle',
          distance: 0
        },
        vertical: {
          position: 'top',
          distance: 12,
          gap: 10
        }
      },
      behaviour: {
        autoHide: 2500,
        onClick: 'hide',
        showDismissButton: false,
        onMouseover: 'pauseAutoHide'
      }
    }),
    SidebarModule.forRoot(),
    TimeagoModule.forRoot()
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory:provideConfig
  },{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpErrorInterceptor,
    multi:true
  }],
  bootstrap: [AppComponent]
})

export class AppModule {
}
