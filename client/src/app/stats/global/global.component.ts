import { Component, OnInit } from '@angular/core';
import {OverviewFinished} from '../../../models/gameoverview/finished';
import {AuthenticationService} from '../../../services/authentication.service';
import {Router} from '@angular/router';
import {GameService} from '../../../services/game.service';
import {GlobalData} from '../../../models/data/global-data';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.scss']
})
export class GlobalComponent implements OnInit {
  loading: boolean;
  status: string;
  data: GlobalData;

  constructor(public authService: AuthenticationService, private router: Router, private gameService: GameService) {
  }

  ngOnInit() {
    this.loading = true;
    this.status = "Loading Global Statistics";
    setTimeout(() => {
      this.gameService.getGlobalData().subscribe(
        res => {
          this.data = res;
          this.loading = false;
        }, () => this.loading = false
      );
    }, 1000);
  }

  home() {
    this.router.navigate(['home']);
  }

}
