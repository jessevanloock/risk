import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {Router} from '@angular/router';
import {GameService} from '../../../services/game.service';
import {OverviewFinished} from '../../../models/gameoverview/finished';
import {PersonalData} from '../../../models/data/personal-data';
import {NotifierService} from "angular-notifier";

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {
  loading: boolean;
  status: string;
  data: PersonalData;
  searchname: string;
  currentUser: string;

  constructor(public authService: AuthenticationService, private notifier: NotifierService, private router:Router, private gameService: GameService) {
  }

  ngOnInit() {
    this.loading = true;
    this.status = "Loading your Personal Statistics";
      this.gameService.getPersonalData(this.authService.currentUser).subscribe(
        res => {
          this.data = res;
          console.log(res);
          this.calculateRatios();
          this.loading = false;
        }, () => this.loading = false
      );
  }
  startReplay(sessionId) {
    this.router.navigate(['overview/game'], {queryParams: {sessionId: sessionId}});
  }
  home() {
    this.router.navigate(['home']);
  }

  getPersonalStats(searchname: string) {
    this.gameService.getPersonalData(searchname).subscribe(
      res => {
        this.data = res;
        this.calculateRatios();
      },
      () => this.notifier.notify('error', "We were unable to find any personal data of this player"),
      () => {
        this.gameService.getGameOverviewFinished().subscribe(
          finished => {
            this.data.history
          }
        )
      }
    );
  }

  calculateRatios() {
    console.log(this.data);
    if (this.data.conquestsTotal > 0)
      this.data.conquestsRatio = Math.round(this.data.conquestsWon / this.data.conquestsTotal * 100);
    if (this.data.attacksTotal > 0)
      this.data.attacksRatio = Math.round(this.data.attacksSuccessful / this.data.attacksTotal * 100);
    if (this.data.defendsTotal > 0)
      this.data.defendsRatio = Math.round(this.data.defendsSuccessful / this.data.defendsTotal * 100);
    if (this.data.troopsLost > 0)
      this.data.troopsRatio = this.data.troopsDefeated / this.data.troopsLost;
  }
}
