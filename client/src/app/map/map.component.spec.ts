import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapComponent} from './map.component';
import {Continent} from '../../models/continent';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have continent values', () => {
    expect(component.continents != null);
    expect(component.continents.length !== 0);
  });

  it('should have path values', () => {
    expect(component.map != null);
  });

  it('should parse json correctly', () => {
    const continentName = 'OCEANIA';
    const continentPath = 'M test1 z';
    const countryId = 1;
    const countryName = 'INDONESIA';
    const countryPath = 'M test2 z';
    const countryNeighbor1 = 8;
    const countryNeighbor2 = 9;

    const continent: Continent = JSON.parse(`{"name": "${continentName}", ` +
      `"path": "${continentPath}", ` +
      '"countries": [ ' +
      `{"id": ${countryId}, "name": "${countryName}", "path": "${countryPath}", ` +
      `"neighbors": [ ${countryNeighbor1}, ${countryNeighbor2} ]}` +
      ' ]}');

    expect(continent.name === continentName);
    expect(continent.path === continentPath);
    expect(continent.territories.length === 1);
    expect(continent.territories[0].id === countryId);
    expect(continent.territories[0].name === countryName);
    expect(continent.territories[0].path === countryPath);
    expect(continent.territories[0].neighbors.length === 2);
    expect(continent.territories[0].neighbors[0] === countryNeighbor1);
    expect(continent.territories[0].neighbors[1] === countryNeighbor2);
  });
});
