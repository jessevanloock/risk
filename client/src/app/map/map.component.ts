import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import Raphael from 'raphael/raphael.js';
import {Continent} from '../../models/continent';
import {Territory} from '../../models/territory';
import {MapElement} from '../../models/map-element';
import {Player} from '../../models/player';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnInit, OnChanges {
  map: any;
  @Input() continents: Continent[];
  territories: Territory[];
  playerList: Player[];
  @Input() extras;
  @Input() mapName;
  @Input() phase;
  @Input() players;
  @Input() currentPlayer: Player;
  @Input() showContinentMap = false;

  @Output() selectedTerritory: EventEmitter<Territory> = new EventEmitter<Territory>();
  @Output() selectedNeighborTerritory: EventEmitter<Territory> = new EventEmitter<Territory>();
  @Output() selectedFortifyTerritory: EventEmitter<Territory> = new EventEmitter<Territory>();
  @Output() stateAnimationsAreFinished: EventEmitter<boolean> = new EventEmitter<boolean>();

  attackFrom: Territory;
  fortifyFrom: Territory;
  // list of elements that are drawn on top of the map for visualisation to the user (ex. selected territories)
  onTopDrawnElements: MapElement[];
  // map of animations for state changes with a boolean value if they are finished or not
  stateChangeAnimationsFinished: Map<string, boolean>;
  raphaelColors = [
    {'color': 'red', 'value': '#c0392b'},
    {'color': 'purple', 'value': '#9b59b6'},
    {'color': 'orange', 'value': '#e67e22'},
    {'color': 'blue', 'value': '#2980b9'},
    {'color': 'yellow', 'value': '#ebc83d'},
    {'color': 'green', 'value': '#27ae60'}];
  MAP_WIDTH;
  MAP_HEIGHT;

  constructor(private authService: AuthenticationService) {
  }

  /**
   * Receives the changes of the @Input() variables from the parent component and handles them.
   * @param changes   All the changes from the parent class
   */
  ngOnChanges(changes: SimpleChanges) {
    this.stateAnimationsAreFinished.emit(false);
    if (changes.continents && changes.continents.currentValue !== undefined) {
      if (changes.continents.previousValue !== undefined) {
        if (this.onTopDrawnElements !== undefined) {
          this.removeOnTopDrawnElements();
        }
        this.stateChangeAnimationsFinished = new Map();
        this.handleStateChanges(changes.continents.previousValue, changes.continents.currentValue);
      }
    }
    if (changes.players && changes.players.currentValue !== undefined) {
      this.playerList = changes.players.currentValue;
    }
    if (changes.phase && changes.phase.currentValue !== undefined) {
      this.phase = changes.phase.currentValue;
    }
    if (changes.currentPlayer && changes.currentPlayer.currentValue !== undefined) {
      this.currentPlayer = changes.currentPlayer.currentValue;
    }
    if (changes.extras && changes.extras.currentValue !== undefined) {
      this.extras = changes.extras.currentValue;
    }
    if (changes.showContinentMap && changes.showContinentMap.currentValue !== undefined) {
      this.showContinentMap = changes.showContinentMap.currentValue;
    }
    if (changes.mapName && changes.mapName.currentValue !== undefined) {
      this.mapName = changes.mapName.currentValue;
    }
    this.checkAnimationsFinished();
  }

  /**
   * Initializes the map component. called after ngOnChanges
   */
  ngOnInit(): void {
    if (this.onTopDrawnElements !== [] && this.onTopDrawnElements !== undefined) {
      this.removeOnTopDrawnElements();
    }
    this.territories = [];
    this.onTopDrawnElements = [];
    if (this.continents != null && this.playerList != null) {
      this.drawMap();
    }
    this.MAP_WIDTH = window.innerWidth;
    this.MAP_HEIGHT = window.innerHeight - 25;
  }

  /**
   * Handles selected territory on map in the allocation phase
   * @param territory     The selected territory
   */
  private allocate(territory: Territory) {
    if (territory.player === this.currentPlayer.username) {
      this.removeOnTopDrawnElements();
      this.selectedTerritory.emit(territory);
      this.drawOnTopTerritory(territory, 'selected');
      this.drawTroopsForOnTopTerritory(territory, 'selected');
    }
  }

  /**
   * Handles selected territory on map in the attack phase
   * @param territory     The selected territory
   */
  private attack(territory: Territory) {
    if (territory.player === this.currentPlayer.username && territory.troops > 1) {
      this.removeOnTopDrawnElements();
      this.selectedTerritory.emit(territory);
      this.attackFrom = territory;

      this.drawOnTopTerritory(territory, 'selected');
      territory.neighbors.forEach(neighbor => {
        let neighborTerritory: Territory = this.territories.find(c => c.id === neighbor);
        if (neighborTerritory.player !== territory.player) {
          this.drawOnTopTerritory(neighborTerritory, 'neighbor');
        }
      });
      territory.neighbors.forEach(neighbor => {
        let neighborTerritory: Territory = this.territories.find(c => c.id === neighbor);
        if (neighborTerritory.player !== territory.player) {
          this.drawAnimationForPath(this.attackFrom, neighborTerritory, 'neighbor');
        }
      });
      territory.neighbors.forEach(neighbor => {
        let neighborTerritory: Territory = this.territories.find(c => c.id === neighbor);
        if (neighborTerritory.player !== territory.player) {
          this.drawTroopsForOnTopTerritory(neighborTerritory, 'neighbor');
        }
      });
      this.drawTroopsForOnTopTerritory(territory, 'selected');

    }
  }

  /**
   * Handles clicked territory on map in the fortification phase
   * @param territory     The clicked territory
   */
  private fortify(territory: Territory) {
    if (territory.player === this.currentPlayer.username) {
      if (this.onTopDrawnElements.find(e => e.key === 'selectedTerritory')
        && !(this.onTopDrawnElements.find(e => e.key === 'fortifyTerritory'))) {
        this.selectedFortifyTerritory.emit(territory);
        this.drawOnTopTerritory(territory, 'fortify');
        this.drawAnimationForPath(this.fortifyFrom, territory, 'fortify');
        this.drawTroopsForOnTopTerritory(this.fortifyFrom, 'selected');
        this.drawTroopsForOnTopTerritory(territory, 'fortify');
      } else {
        this.removeOnTopDrawnElements();
        if (territory.troops > 1) {
          this.selectedTerritory.emit(territory);
          this.fortifyFrom = territory;
          this.drawOnTopTerritory(territory, 'selected');
          this.drawTroopsForOnTopTerritory(this.fortifyFrom, 'selected');
        }
      }
    }
  }

  //#region STATE ANIMATIONS METHODS
  /**
   * Checks that all state change animations are finished and calls ngOnInit if they are finished
   */
  private checkAnimationsFinished() {
    let checkAnimationsFinished = true;
    if (this.stateChangeAnimationsFinished !== undefined) {
      this.stateChangeAnimationsFinished.forEach((value: boolean, key: string) => {
        if (value === false) {
          checkAnimationsFinished = false;
        }
      });
    }
    if (checkAnimationsFinished === true) {
      this.stateAnimationsAreFinished.emit(true);
      this.ngOnInit();
    }
  }

  /**
   * Is called when there are changes in the continents and handles them
   * @param oldContinents     The continents in the previous state
   * @param newContinents     The continents in the new or current state
   */
  private handleStateChanges(oldContinents: Continent[], newContinents: Continent[]) {
    this.onTopDrawnElements.push(
      new MapElement(
        'rect',
        this.map.rect(0,0,this.MAP_WIDTH,this.MAP_HEIGHT)
          .attr('fill', 'black')
          .attr('opacity','0')
      )
    );
    let playerchange = false;
    oldContinents.forEach(continent => {
      let newContinent: Continent = newContinents.find(n => n.id === continent.id);
      continent.territories.forEach(territory => {
        let newTerritory: Territory = newContinent.territories.find(n => n.id === territory.id);
        if (newTerritory.player !== territory.player) {
          this.playerChanged(territory, newTerritory);
          playerchange = true;
        }
      });
    });
    oldContinents.forEach(continent => {
      let newContinent: Continent = newContinents.find(n => n.id === continent.id);
      continent.territories.forEach(territory => {
        let newTerritory: Territory = newContinent.territories.find(n => n.id === territory.id);
        if (newTerritory.troops !== territory.troops && playerchange === false) {
          this.troopsChanged(territory, newTerritory);
        }
      });
    });
  }

  /**
   * Is called when there is a change in troops on a territory and handles the animations for it
   * @param oldTerritory      The territory in previous state
   * @param newTerritory      The territory in new or current state
   */
  private troopsChanged(oldTerritory: Territory, newTerritory: Territory) {
    this.stateChangeAnimationsFinished.set('troopsChangedCircle' + oldTerritory.id, false);
    this.stateChangeAnimationsFinished.set('troopsChangedText' + oldTerritory.id, false);
    let animCircle = Raphael.animation(
      {'transform': 's1', 'stroke-width': '1'}
      , 700, 'linear', this.stateChangeAnimationFinished.bind(this, 'troopsChangedCircle' + oldTerritory.id));
    let animText = Raphael.animation(
      {'transform': 's1'}
      , 700, 'linear', this.stateChangeAnimationFinished.bind(this, 'troopsChangedText' + oldTerritory.id));
    this.onTopDrawnElements.push(
      new MapElement(
        'troopsChangedCircle' + oldTerritory.id,
        this.map.circle(
          oldTerritory.pathElement.getBBox().x + oldTerritory.pathElement.getBBox().width / 2,
          oldTerritory.pathElement.getBBox().y + oldTerritory.pathElement.getBBox().height / 2,
          12)
          .attr('fill', oldTerritory.troops > newTerritory.troops ? 'red' : 'green')
          .attr('stroke', 'white')
          .attr('stroke-width', '2')
          .scale(2, 2)
          .animate(animCircle)
      )
    );
    this.onTopDrawnElements.push(
      new MapElement(
        'troopsChangedText' + oldTerritory.id,
        this.map.text(
          oldTerritory.pathElement.getBBox().x + oldTerritory.pathElement.getBBox().width / 2,
          oldTerritory.pathElement.getBBox().y + oldTerritory.pathElement.getBBox().height / 2,
          oldTerritory.troops > newTerritory.troops ?
            '-' + (oldTerritory.troops - newTerritory.troops) :
            '+' + (newTerritory.troops - oldTerritory.troops))
          .attr('fill', 'white')
          .scale(2, 2,)
          .animate(animText)
      )
    );
  }

  /**
   * Is called when there is a change in player on a territory and handles the animations for it
   * @param oldTerritory      The territory in previous state
   * @param newTerritory      The territory in new or current state
   */
  private playerChanged(oldTerritory: Territory, newTerritory: Territory) {
    const style = {
      'fill': this.playerList.find(p => p.username === oldTerritory.player).color,
      'stroke': 'white',
      'stroke-width': 2,
      'stroke-linejoin': 'round',
    };
    this.stateChangeAnimationsFinished.set('playerChanged' + oldTerritory.id, false);
    let animPlayer = Raphael.animation(
      {
        'fill': this.raphaelColors.find(c =>
          c.color === this.playerList.find(p => p.username === newTerritory.player).color).value
      }, 1500, 'linear', this.stateChangeAnimationFinished.bind(this, 'playerChanged' + oldTerritory.id));
    this.onTopDrawnElements.push(
      new MapElement(
        'playerChanged' + oldTerritory.id,
        this.map.path(oldTerritory.path)
          .attr(style)
          .animate(animPlayer)
      )
    );
  }

  /**
   * Is the callback function for the animations for the state change
   * @param name      Name of the animation that is finished
   */
  stateChangeAnimationFinished(name: string) {
    this.stateChangeAnimationsFinished.set(name, true);
    this.checkAnimationsFinished();
  }

//#endregion

  //#region MAP DRAW METHODS
  /**
   * Draws the game or continent map
   */
  private drawMap() {
    const style = {
      stroke: '#34495e',
      'stroke-width': 1,
      'stroke-linejoin': 'round',
    };
    if (this.map !== undefined) {
      this.map.remove();
    }
    const MAP_CONTAINER = document.getElementById('map');
    this.map = new Raphael(MAP_CONTAINER);

    console.log(this.mapName);
    // voordien
    this.map.setViewBox(100, 100, 950, 550, true);

    // nieuw
    if (this.mapName === 'Global Conquest') {
      this.map.setViewBox(100, 100, 950, 550, true);
    } else if (this.mapName === 'belgium') {
      this.map.setViewBox(100, 100, 700, 900, true);
    }



    this.map.setSize(this.MAP_WIDTH, this.MAP_HEIGHT);

    if (this.showContinentMap === false) {
      this.continents.forEach(continent => {
        continent.territories.forEach(territory => {
          territory.pathElement = this.map.path(territory.path)
            .click((event: { target: SVGPathElement }) => this.territoryClick(territory))
            .attr(style)
            .attr('cursor', this.authService.currentUser === this.currentPlayer.username &&
            this.authService.currentUser === territory.player ? 'pointer' : 'default')
            .attr('fill', this.raphaelColors.find(
              c => c.color === this.playerList.find(p => p.username === territory.player).color).value);
          this.territories.push(territory);
        });
      });
      this.extras.forEach(extra => {
        this.map.path(extra);
      });
      this.drawTroops();
    } else {
      let colors = ['#3498db', '#9c59b6', '#bd261e', '#e67e22', '#ebc83d', '#0fa84f'];
      this.continents.forEach(continent => {
        let color = colors.pop();
        let minXBox;
        let minYBox;
        let maxXBox;
        let maxYBox;
        continent.territories.forEach(territory => {
          territory.pathElement = this.map.path(territory.path)
            .attr(style)
            .attr('fill', color);
          if (minXBox > territory.pathElement.getBBox().x || minXBox === undefined) {
            minXBox = territory.pathElement.getBBox().x;
          }
          if (minYBox > territory.pathElement.getBBox().y || minYBox === undefined) {
            minYBox = territory.pathElement.getBBox().y;
          }
          if (maxXBox < territory.pathElement.getBBox().x + territory.pathElement.getBBox().width || maxXBox === undefined) {
            maxXBox = territory.pathElement.getBBox().x + territory.pathElement.getBBox().width;
          }
          if (maxYBox < territory.pathElement.getBBox().y + territory.pathElement.getBBox().height || maxYBox === undefined) {
            maxYBox = territory.pathElement.getBBox().y + territory.pathElement.getBBox().height;
          }
        });
        this.map.circle((minXBox + maxXBox) / 2, (minYBox + maxYBox) / 2, 10)
          .attr('fill', color)
          .attr('stroke', 'white')
          .attr('stroke-width', '2');
        this.map.text((minXBox + maxXBox) / 2, (minYBox + maxYBox) / 2, '+' + continent.bonus)
          .attr('fill', 'white');
      });
      this.extras.forEach(extra => {
        this.map.path(extra);
      });
    }
  }

  /**
   * Draw the troop on the game or continent map
   */
  private drawTroops() {
    this.continents.forEach(continent => {
      continent.territories.forEach(territory => {
        this.map.circle(
          territory.pathElement.getBBox().x + territory.pathElement.getBBox().width / 2,
          territory.pathElement.getBBox().y + territory.pathElement.getBBox().height / 2,
          9)
          .click(() => this.territoryClick(territory))
          .attr('fill', this.raphaelColors.find(c => c.color === this.playerList.find(p => p.username === territory.player).color).value)
          .attr('cursor', this.authService.currentUser === this.currentPlayer.username &&
          this.authService.currentUser === territory.player ? 'pointer' : 'default')
          .glow({width: 1, offsetx: -1, offsety: 1, fill: true, opacity: 0.4})
          .attr('cursor', this.authService.currentUser === this.currentPlayer.username &&
          this.authService.currentUser === territory.player ? 'pointer' : 'default') // this goes for the glow
          .attr('stroke', 'black');
        this.map.text(
          territory.pathElement.getBBox().x + territory.pathElement.getBBox().width / 2,
          territory.pathElement.getBBox().y + territory.pathElement.getBBox().height / 2,
          territory.troops)
          .click(() => this.territoryClick(territory))
          .attr('cursor', this.authService.currentUser === this.currentPlayer.username &&
          this.authService.currentUser === territory.player ? 'pointer' : 'default')
          .attr('fill', 'white');
      });
    });
  }

  /**
   * Handles click event on territory on a map
   * @param territory       Clicked territory
   */
  private territoryClick(territory: Territory) {
    if (this.phase === 'ALLOCATION') {
      this.allocate(territory);
    } else if (this.phase === 'ATTACK') {
      this.attack(territory);
    } else if (this.phase === 'FORTIFICATION') {
      this.fortify(territory);
    }
  }

//#endregion

  //#region ON TOP DRAWN ELEMENTS
  /**
   * Remove all the on top drawn elements (selected, fortify, neighbor territorys and attributes, state animation objects, ...)
   */
  public removeOnTopDrawnElements() {
    this.onTopDrawnElements.forEach(e => {
      e.element.remove();
    });
    this.onTopDrawnElements = [];
  }

  /**
   * Draw on top drawn territory (for selected, neighbor or fortify)
   * @param territory     The selected, neighbor or to fortify territory
   * @param type          The type of the territory (selected, neighbor or to fortify)
   */
  private drawOnTopTerritory(territory: Territory, type: string) {
    const style = {
      'fill': this.raphaelColors.find(c => c.color === this.playerList.find(p => p.username === territory.player).color).value,
      'stroke': '#ecf0f1',
      'stroke-width': 2,
      'stroke-linejoin': 'round',
      'opacity': 0,
    };
    let name = type + 'Territory';
    if (type === 'neighbor') {
      name = name + territory.id;
      this.onTopDrawnElements.push(
        new MapElement(
          name,
          this.map.path(territory.path)
            .click((event: { target: SVGPathElement }) => this.onTopDrawnTerritoryClick(territory, type))
            .attr(style)
            .attr('stroke-width', '2')
            .attr('cursor', 'pointer')
            .animate(
              {'opacity': 1}
              , 800)
        )
      );
    } else {
      this.onTopDrawnElements.push(
        new MapElement(
          name,
          this.map.path(territory.path)
            .click(() => this.onTopDrawnTerritoryClick(territory, type))
            .attr(style)
            .attr('cursor', this.authService.currentUser === this.currentPlayer.username &&
            this.authService.currentUser === territory.player ? 'pointer' : 'default')
            .animate(
              {'opacity': 1}
              , 300)
        )
      );
    }
  }

  /**
   * Draw troops for the on top drawn territoy (for selected, neighbor or fortify)
   * @param territory     The selected, neighbor or to fortify territory
   * @param type          The type of the territory (selected, neigbor or to fortify)
   */
  private drawTroopsForOnTopTerritory(territory: Territory, type: string) {
    let troopscirclename = type + 'TerritoryTroopsCircle';
    let troopsname = type + 'TerritoryTroops';
    if (type === 'neighbor') {
      troopscirclename = troopscirclename + territory.id;
      troopsname = troopsname + territory.id;
    }
    this.onTopDrawnElements.push(
      new MapElement(
        troopscirclename,
        this.map.circle(
          territory.pathElement.getBBox().x + territory.pathElement.getBBox().width / 2,
          territory.pathElement.getBBox().y + territory.pathElement.getBBox().height / 2,
          9)
          .click(() => this.onTopDrawnTerritoryClick(territory, type))
          .attr('fill', this.raphaelColors.find(
            c => c.color === this.playerList.find(p => p.username === territory.player).color).value)
          .attr('stroke', 'white')
          .attr('stroke-width', '1')
          .attr('cursor', 'pointer')
          .animate(
            {'stroke-width': 2}
            , 300)
      )
    );
    this.onTopDrawnElements.push(
      new MapElement(
        troopsname,
        this.map.text(
          territory.pathElement.getBBox().x + territory.pathElement.getBBox().width / 2,
          territory.pathElement.getBBox().y + territory.pathElement.getBBox().height / 2,
          territory.troops)
          .click(() => this.onTopDrawnTerritoryClick(territory, type))
          .attr('fill', 'white')
          .attr('cursor', 'pointer')
      )
    );
  }

  /**
   * Handles click event on on top drawn territory on a map
   * @param territory       Clicked on top drawn territory
   * @param type            type of clicked territory (selected, neighbor or fortify)
   */
  private onTopDrawnTerritoryClick(territory: Territory, type: string) {
    if (type === 'fortify') {
      this.selectedFortifyTerritory.emit(territory);
    }
    if (type === 'neighbor') {
      this.selectedNeighborTerritory.emit(territory);
    } else {
      this.selectedTerritory.emit(territory);
    }
  }

  /**
   * Draws the animations between two territories. Between selected and neighbor territory or between selected and to fortify territory
   * @param territoryFrom       The selected territory
   * @param territoryTo         The neigbor territory or the to fortify territory
   * @param type                The type of the territory to go to (neighbor or fortify)
   */
  private drawAnimationForPath(territoryFrom: Territory, territoryTo: Territory, type: string) {
    let anim = Raphael.animation({
      'cx': territoryTo.pathElement.getBBox().x + territoryTo.pathElement.getBBox().width / 2,
      'cy': territoryTo.pathElement.getBBox().y + territoryTo.pathElement.getBBox().height / 2
    }, 1000, 'linear').repeat(Infinity);
    let name = type + 'CirclePath';
    if (type === 'neighbor') {
      name = name + territoryTo.id;
    }
    this.onTopDrawnElements.push(
      new MapElement(
        name,
        this.map.circle(
          territoryFrom.pathElement.getBBox().x + territoryFrom.pathElement.getBBox().width / 2,
          territoryFrom.pathElement.getBBox().y + territoryFrom.pathElement.getBBox().height / 2,
          5)
          .attr('stroke', 'white')
          .attr('fill', 'white')
          .attr('stroke-width', '2')
          .animate(anim)
      )
    );
    /*this.onTopDrawnElements.push(
      new MapElement(
        name,
        this.map.path(
          'M' + Math.round(territoryFrom.pathElement.getBBox().x + territoryFrom.pathElement.getBBox().width / 2) + ',' +
          Math.round(territoryFrom.pathElement.getBBox().y + territoryFrom.pathElement.getBBox().height / 2) + ' ' +
          'L' + Math.round(territoryTo.pathElement.getBBox().x + territoryTo.pathElement.getBBox().width / 2) + ',' +
          Math.round(territoryTo.pathElement.getBBox().y + territoryTo.pathElement.getBBox().height / 2)
        )
          .attr('stroke-dasharray', '-')
          .attr('stroke', 'white')
          .attr('stroke-width', '2')
          .attr('fill', 'white')
      )
    );*/

  }

//#endregion
}
