import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {GameService} from '../../../services/game.service';
import {Lobby} from '../../../models/lobby';
import {NotifierService} from 'angular-notifier';
import {ChatService} from '../../../services/chat.service';
import {Friendship} from '../../../models/friendship';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {
  sessionId: number;
  lobby: Lobby;
  loading = true;
  status = '';
  isHost = false;
  private username;
  showPlayers = true;
  showInvite = false;
  showPreferences = false;
  maps = ['Global Conquest','belgium'];
  map= this.maps[0];
  showAI = true;
  showPlayer = false;
  friends: Friendship[] = [];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authService: AuthenticationService,
              private gameService: GameService,
              private chatService: ChatService,
              private notifier: NotifierService) {
  }

  /**
   * Called on Initialize of lobby component
   */
  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.sessionId = JSON.parse(params['id']);

        this.connectToWebsocket();

        this.gameService.getSessionLobby(this.sessionId).subscribe(
          res => {
            console.log(res);
            this.initializeLobby(res);
          }
        );
      }
    );
    this.gameService.getFriendRequests().subscribe(
      res => {
        res.forEach(f => {
          if (f.confirmed) {
            this.friends.push(f);
          }
        });
      }
    );
  }

  /**
   * Initializes the lobby with the given state
   * @param lobbyState      The state of the lobby
   */
  initializeLobby(lobbyState: Lobby) {
    this.lobby = lobbyState;
    this.lobby.rows = [
      [this.lobby.players[0], this.lobby.players[1]],
      [this.lobby.players[2], this.lobby.players[3]],
      [this.lobby.players[4], this.lobby.players[5]],
    ];
    console.log(this.lobby.host);
    this.isHost = this.authService.currentUser === this.lobby.host;
    this.loading = false;
  }

  /**
   * Called on Destory of lobby component
   */
  ngOnDestroy(): void {
    this.gameService.disconnectFromStateChange();
  }

  /**
   * Navigates to home component
   */
  home() {
    this.router.navigate(['home']);
  }

  /**
   * Shows the right tab
   * @param tab     number of the tab
   */
  show(tab: number) {
    this.showPlayers = false;                 // close all tabs first
    this.showInvite = false;
    this.showPreferences = false;
    switch (tab) {
      case 1:
        this.showPlayers = true;
        break;
      case 2:
        this.showInvite = true;
        break;
      case 3:
        this.showPreferences = true;
        break;
    }
  }

  /**
   * Shows the right tab inside invite tab
   * @param tab     number of the tab
   */
  inviteTabShow(tab: number) {
    this.showAI = false;                 // close all tabs first
    this.showPlayer = false;
    switch (tab) {
      case 1:
        this.showAI = true;
        break;
      case 2:
        this.showPlayer = true;
        break;
    }
  }

  /**
   * Sends start game request to the server
   */
  start() {
    if (this.showPlayers) {
      if (this.authService.currentUser === this.lobby.host) {
        if (this.lobby.players.length >= 3) {
          this.loading = true;
          this.status = 'Preparing armies';
          this.gameService.startGame(this.sessionId, this.map);
        } else {
          this.notifier.notify('error', 'Whoops, you need at least ' + (3 - this.lobby.players.length) + ' more players to dominate!');
        }
      } else {
        this.notifier.notify('error', 'Wait for the host to start');
      }
    } else {
      this.showInvite = false;
      this.showPreferences = false;
      this.showPlayers = true;
    }
  }

  /**
   * Sends discard session request to the server
   */
  discard() {
    if (this.authService.isLoggedIn()) {
      this.gameService.discardSession(this.sessionId)
        .subscribe(
          () => {
            this.home();
            this.notifier.notify('success', 'Lobby closed');
          }
        );
    }
  }

  /**
   * Sends leave session request to the server
   */
  leave() {
    if (this.authService.isLoggedIn()) {
      this.gameService.leaveSession(this.sessionId)
        .subscribe(
          () => {
            this.home();
            this.notifier.notify('success', 'Left the lobby');
          }
        );
    }
  }

  /**
   * Sends kick player request to the server
   * @param username      username of player to kick
   */
  kick(username: string) {
    if (this.authService.isLoggedIn()) {
      this.gameService.kickPlayer(this.sessionId, username)
        .subscribe(
          () => {
            this.notifier.notify('success', 'Player was kicked');
          }
        );
    }
  }

  /**
   * Send invite to a player
   * @param username      name of player
   */
  sendInvite(username: string) {
    if (this.authService.isLoggedIn()) {
      this.gameService.sendSessionInvite(username, this.lobby.sessionId)
        .subscribe(
          () => {
            this.notifier.notify('success', 'Invite sent to ' + username);
          }
        );
    }
  }

  /**
   * Send request to the server for changing the color of a player
   * @param username       Username of the player
   * @param color          Color for the player
   */
  setColor(username: string, color: string) {
    if (this.authService.isLoggedIn()) {
      this.gameService.setColor(this.sessionId, username, color);
    }
  }

  /**
   * Send a request to add an AI player to the game
   * @param type        Type of the AI player
   */
  addAIPlayer(type: string) {
    this.gameService.addAIPlayer(type, this.sessionId)
      .subscribe(
        () => {
          this.notifier.notify('success', 'AI player added!');
        }
      );
  }

  private connectToWebsocket() {
    this.gameService.connectToStateChange();

    this.gameService.onWebsocketError()
      .subscribe(
        () => {
        },
        err => {
          console.error(err);
          console.error('error websocket lobby');
          this.notifier.notify('error', err);
          this.router.navigate(['home']);
        });

    this.gameService.onLobbyStateChange().subscribe(res => {
      console.log(res.body);
      this.initializeLobby(JSON.parse(res.body));
    });

    this.gameService.onGameStateChange().subscribe(res => {
      this.router.navigate(['game'], {queryParams: {sessionId: this.sessionId}});
    });
  }

  prevMap() {
    let i = this.maps.findIndex(m => m === this.map);
    if (i===0) {
      this.map = this.maps[this.maps.length-1];
    } else {
      this.map = this.maps[i-1];
    }
  }

  nextMap() {
    let i = this.maps.findIndex(m => m === this.map);
    if (i=== this.maps.length-1) {
      this.map = this.maps[0];
    } else {
      this.map = this.maps[i+1];
    }
  }


}
