import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Territory} from '../../../models/territory';
import {GameService} from '../../../services/game.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {Continent} from '../../../models/continent';
import {Player} from '../../../models/player';
import {NotifierService} from 'angular-notifier';
import {MapComponent} from '../../map/map.component';
import {GameState} from '../../../models/game-state';
import {animate, AnimationEvent, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
  animations: [
    trigger('phaseAnimation', [
      transition(':enter', [
        style({opacity: 0}),
        animate('1500ms', style({opacity: 1})),
      ])
    ]),
    trigger('winnerAnimation', [
      transition(':enter', [
        style({
          transform: 'translateX(-100%)'
        }),
        animate('500ms ease-in',style({transform: 'translateX(0)' }))
      ])
    ])
  ]
})
export class GameComponent implements OnInit, OnDestroy {

  @ViewChild(MapComponent) map: MapComponent;

  sessionId: number = null;
  phase = 'ALLOCATE';
  selectedTerritory: Territory;
  selectedNeighborTerritory: Territory;
  selectedFortifyTerritory: Territory;
  stateQueue: GameState[] = [];
  placeableTroops = 0;
  addTroops = 1;
  moveTroops = 1;
  continents: Continent[];
  players: Player[];

  currentPlayer: Player = null;
  mapName;
  extras: string[];
  continentMap = false;
  phaseCompleted = false;

  allocationIndicator: string;
  attackIndicator: string;
  fortificationIndicator: string;
  private stateAnimationsFinished = false;
  private phaseAnimationFinished = false;
  sliderOptions;
  finished = false;
  winner: Player;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private gameService: GameService,
              private authService: AuthenticationService,
              private notifier: NotifierService) {
    this.players = [];
    route.queryParams.subscribe(
      params => {
        this.sessionId = params['sessionId'];
      }
    );
  }

  /**
   * Called on Initialize of game component
   */
  ngOnInit() {
    this.getState();
  }

  /**
   * Called on Destroy of game component
   */
  ngOnDestroy(): void {
    this.gameService.disconnectFromStateChange();
  }

  /**
   * Sets slider for allocation
   */
  private setAllocationSliderOptions() {
    this.sliderOptions = {
      animate: false,
      vertical: true,
      floor: 1,
      ceil: this.placeableTroops,
      hideLimitLabels: true
    };
  }

  /**
   * Gets the current state of the game from the server
   */
  private getState() {
    this.gameService.getSessionState(this.sessionId)
      .subscribe(
        resState => {
          console.log(resState);
          this.initializeGame(resState);
        }
      );
    this.connectToWebsocket();
  }

  /**
   * Gets called when the state animations in the map component are started or finished
   * @param value    false: if state animations are started, true: if state animations are finished
   */
  setStateAnimationsFinished(value: boolean) {
    this.stateAnimationsFinished = value;
    if (this.stateAnimationsFinished === true && this.phaseAnimationFinished === true) {
      if (this.stateQueue.length > 0) {
        this.initializeGame(this.stateQueue[0]);
        this.stateQueue.shift();
      }
    }
  }

  /**
   * Gets called when the phase animations is started or finished
   * @param $event      The animation itself
   * @param value       false: if phase animation is started, true: if phase animation is finished
   */
  setPhaseAnimationFinishedState($event: AnimationEvent, value: boolean) {
    if ($event.fromState === 'void') {
      this.phaseAnimationFinished = value;
      if (this.phaseAnimationFinished === true && this.stateAnimationsFinished === true) {
        if (this.stateQueue.length > 0) {
          this.initializeGame(this.stateQueue[0]);
          this.stateQueue.shift();
        }
      }
    }
  }

  /**
   * Initialize the game for the given state
   * @param resState      The game state that has to be initialized
   */
  private initializeGame(resState: GameState) {
    this.phaseAnimationFinished = false;
    this.stateAnimationsFinished = false;
    this.selectedTerritory = null;
    this.selectedFortifyTerritory = null;
    this.selectedNeighborTerritory = null;
    this.addTroops = 1;
    this.moveTroops = 1;
    this.players = resState.players;
    this.continents = resState.continents;
    this.extras = resState.extras;
    this.mapName = resState.map;
    console.log(resState);
    this.currentPlayer = this.players.find(p => p.username === resState.turn.username);
    this.placeableTroops = resState.turn.placeableTroops;
    if (resState.finished !== undefined) {
      this.gameIsFinished(resState.finished);
    }
    this.setPhase(resState.turn.phase);
  }

  /**
   * Navigates to the game overview
   */
  gameOverview() {
    this.router.navigate(['gameOverview']);
  }

  /**
   * Called when a territory in the map component is selected
   * @param territory     The selected Territory
   */
  onTerritorySelected(territory: Territory) {
    // If the user clicked the same territory (to deselect)
    if (this.selectedTerritory === territory) {
      this.deselectTerritories();
    } else {
      this.selectedTerritory = territory;
      this.selectedFortifyTerritory = null;
    }
  }

  /**
   * Called when a to fortify territory in the map component is selected
   * @param territory       The selected fortify territory
   */
  onFortifySelected(territory: Territory) {
    if (this.selectedFortifyTerritory === territory) {
      this.deselectTerritories();
    } else {
      this.selectedFortifyTerritory = territory;
    }
  }

  /**
   * Deselects selected territories
   */
  deselectTerritories() {
    this.map.removeOnTopDrawnElements();
    this.selectedTerritory = null;
    this.selectedFortifyTerritory = null;
    this.selectedNeighborTerritory = null;
  }

  /**
   * Sends an allocate action to the server
   */
  allocate() {
    this.gameService.allocate(this.sessionId, this.selectedTerritory.id, this.addTroops);
  }

  /**
   * Sends an attack action to the server.
   * Called by selecting a neighbor territory to attack in the map component.
   * @param territory         The neighbor territory to attack
   */
  attack(territory: Territory) {
    if (this.selectedTerritory !== territory) {
      this.selectedNeighborTerritory = territory;
      this.gameService.attack(this.sessionId, this.selectedTerritory.id, this.selectedNeighborTerritory.id);
    } else {
      this.onTerritorySelected(territory);
    }
  }

  /**
   * Sends a fortify action to the server
   */
  fortify() {
    this.gameService.fortify(this.sessionId, this.selectedTerritory.id, this.selectedFortifyTerritory.id, this.moveTroops);
  }

  /**
   * Sends a next phase action to the server
   */
  nextPhase() {
    this.gameService.phase(this.sessionId);
  }

  /**
   * Sets the phase based on the phase of the state that is being initialized.
   * Handles related things to the state that have to be done (ex. sets the right indicator)
   * @param phase       The phase of the state that is being initialized
   */
  private setPhase(phase: string) {
    if (this.phase !== phase) {
      this.phase = phase;
      if (this.phase === 'ALLOCATION') {
        this.setAllocationSliderOptions();
        if (this.placeableTroops === 0) {
          this.phaseCompleted = true;
        }
      }
    } else {
      this.phaseAnimationFinished = true;
    }
    this.allocationIndicator = null;
    this.attackIndicator = null;
    this.fortificationIndicator = null;
    switch (this.phase) {
      case 'ALLOCATION':
        this.allocationIndicator = 'current';
        break;
      case 'ATTACK':
        this.attackIndicator = 'current';
        break;
      case 'FORTIFICATION':
        this.fortificationIndicator = 'current';
    }
  }

  /**
   * Called when game is finished
   * @param winner      winner of the game
   */
  private gameIsFinished(winner: string) {
    console.log(winner);
    this.finished = true;
    this.winner = this.players.find(p => p.username === winner);
  }

  /**
   * Handles + click on the troops slider in the allocation phase
   */
  private addTroopsAdd() {
    if (this.addTroops < this.placeableTroops) {
      this.addTroops++;
    } else if (this.addTroops > this.placeableTroops) {
      this.addTroops = this.placeableTroops;
    } else {
      this.addTroops = 1;
    }
  }

  /**
   * handles - click on the troops slider in the allocation phase
   */
  private addTroopsMin() {
    if (this.addTroops > 1) {
      this.addTroops--;
    } else if (this.addTroops < 1) {
      this.addTroops = 1;
    } else {
      this.addTroops = this.placeableTroops;
    }
  }

  /**
   * handles + click on the troops slider in the fortification phase
   */
  private moveTroopsAdd() {
    if (this.moveTroops < this.selectedTerritory.troops - 1) {
      this.moveTroops++;
    } else if (this.moveTroops >= this.selectedTerritory.troops - 1) {
      this.moveTroops = this.selectedTerritory.troops - 1;
    } else {
      this.moveTroops = 1;
    }
  }

  /**
   * handles - click on the troops slider in the fortification phase
   */
  private moveTroopsMin() {
    if (this.moveTroops > 1) {
      this.moveTroops--;
    } else if (this.moveTroops < 1) {
      this.moveTroops = 1;
    } else {
      this.moveTroops = this.selectedTerritory.troops - 1;
    }
  }

  // TODO: refactor of websocket to GameService
  private connectToWebsocket() {
    this.gameService.connectToStateChange();

    this.gameService.onWebsocketError()
      .subscribe(
        () => {
        });

    this.gameService.onGameStateChange()
      .subscribe(
        resState => {
          if (this.stateAnimationsFinished === false || this.phaseAnimationFinished === false || this.stateQueue.length > 0) {
            this.stateQueue.push(JSON.parse(resState.body));
          } else {
            this.initializeGame(JSON.parse(resState.body));
          }
        });

    this.gameService.onErrorMessage()
      .subscribe(
        err => {
          console.error(err);
          console.error('error websocket game');
          this.notifier.notify('error', err.body);
        });
  }

  toggleContinentMap() {
    this.continentMap = !this.continentMap;
  }
}
