import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './notifier-core.scss']
})
export class AppComponent {

  _opened = false;
  landscape: boolean;

  constructor() {
    this.landscape = (screen.orientation.type === 'landscape-primary' || screen.orientation.type === 'landscape-secondary');
  }

  private _toggleSidebar() {
    this._opened = !this._opened;
  }
}
