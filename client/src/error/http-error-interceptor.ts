import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthenticationService} from '../services/authentication.service';
import {Injectable} from '@angular/core';
import {NotifierService} from 'angular-notifier';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthenticationService, private notifier: NotifierService) {
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
            if (error.status === 401) {
              this.authService.logout();
            }
            console.error(error);
            if (error.error.error_description !== undefined) {
              this.notifier.notify('error',error.error.error_description);
            } else {
              this.notifier.notify('error',error.error);
            }
          return throwError(error);
        })
      );
  }
}
