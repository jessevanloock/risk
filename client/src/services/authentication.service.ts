import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {AuthService, GoogleLoginProvider} from 'angularx-social-login';
import {GameService} from './game.service';
import {NotifierService} from 'angular-notifier';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authUser = 'error402';
  authPassword = 'admin1234';
  currentUser = '';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + btoa(this.authUser + ':' + this.authPassword)
    })
  };

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private router: Router,
              private googleService: AuthService,
              private gameService: GameService,
              private notifier: NotifierService) {
    console.log('sessionStorage: ' + sessionStorage.getItem('RiskAccessToken'));

    if (this.isLoggedIn()) {
      this.http.post<any>(environment.addressAuth + 'oauth/check_token?token=' +
        sessionStorage.getItem('RiskAccessToken'), null).subscribe(
        res => {
          this.currentUser = res.user_name;
        }
      );
    }
  }

  createUser(username, password): Observable<any> {
    console.log('create user method');
    return this.http.post<any>(environment.addressAuth + environment.createUserUrl, {username, password});
  }

  login(username, password): Observable<any> {
    const url = environment.addressAuth + environment.getTokenUrl +
      'grant_type=password' + '&' + 'username=' + username + '&' + 'password=' + password;
    return this.http.post<any>(url, null, this.httpOptions);
  }

  isLoggedIn() {
    return sessionStorage.getItem('RiskAccessToken') != null;
  }

  logout() {
    const url = environment.addressAuth + environment.revokeTokenUrl;
    this.http.post<any>(url, null, {
      headers: new HttpHeaders({'Authorization': 'Bearer ' + sessionStorage.getItem('RiskAccessToken')})
    }).subscribe(
      res => {
        console.log(res);
        this.currentUser = '';
        localStorage.removeItem('RiskRefreshToken');
        sessionStorage.removeItem('RiskAccessToken');
      }, err => {
        console.error(err);
      }
    );
    this.signOutWithGoogle();
    this.router.navigate(['home']);
  }

  signInWithGoogle() {
    let httpOptionsGoogle = {headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})};
    this.googleService.signIn(GoogleLoginProvider.PROVIDER_ID);
    let google = this.googleService.authState.subscribe((user) => {
      console.log(user);
      if (user != null) {
        this.http.post<any>(environment.addressAuth + environment.googleUrl + user.idToken, null, httpOptionsGoogle)
          .subscribe(
            res => {
              console.log('google success');
              console.log(res);
              localStorage.setItem('RiskRefreshToken', res.refresh_token);
              sessionStorage.setItem('RiskAccessToken', res.access_token);
              this.gameService.createUser().subscribe(
                resUser => {
                  console.log('success user');
                  console.log(resUser);
                  this.currentUser = user.firstName;
                  google.unsubscribe();
                }, error => {
                  google.unsubscribe();
                }
              );
              this.router.navigate(['home']);
            }
          );
      }
    });
  }

  signOutWithGoogle() {
    this.googleService.signOut().catch();
  }

  /*errorHandler(err: HttpErrorResponse) {
    this.logout();
    console.log(err);
    return throwError(err);
  }*/
}
