import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {WebsocketService} from './websocket.service';
import {IMessage} from '@stomp/stompjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private readonly CHAT_WEBSOCKET_KEY = 'CHAT';
  private httpOptions = {
    headers: new HttpHeaders()
  };

  constructor(private http: HttpClient, private websocket: WebsocketService) {
  }

  getSessionChat(sessionId): Observable<any> {
    this.setAuthHeader();
    return this.http.get<any>(environment.addressChat + environment.getSessionChatUrl + sessionId.toString(), this.httpOptions);
  }

  getGlobalChat() {
    this.setAuthHeader();
    return this.http.get<any>(environment.addressChat + environment.getGlobalChatUrl, this.httpOptions);
  }

  connectToChatChange() {
    this.websocket.connect(environment.addressChatWebsocket, this.CHAT_WEBSOCKET_KEY);
  }

  disconnectFromChatChange() {
    this.websocket.disconnect(this.CHAT_WEBSOCKET_KEY);
  }

  onWebsocketError(): Observable<never> {
    return this.websocket.onError(this.CHAT_WEBSOCKET_KEY);
  }

  onChatChange(chatId): Observable<IMessage> {
    return this.websocket.onMessage(`/topic/${chatId}/messages`, this.CHAT_WEBSOCKET_KEY);
  }

  sendChatMessage(chatId, message: string) {
    this.websocket.send('/chat/send-message', {chatId: chatId, message: message}, this.CHAT_WEBSOCKET_KEY);
  }

  private setAuthHeader() {
    this.httpOptions = {
      headers: new HttpHeaders({'Authorization': 'Bearer ' + sessionStorage.getItem('RiskAccessToken')})
    };
  }
}
