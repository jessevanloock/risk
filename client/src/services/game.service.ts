import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WebsocketService} from './websocket.service';
import {environment} from '../environments/environment';
import {IMessage} from '@stomp/stompjs';
import {Lobby} from '../models/lobby';
import {Friendship} from '../models/friendship';
import {GameState} from '../models/game-state';
import {GameLog} from '../models/game-log';
import {OverviewInvite} from 'src/models/gameoverview/invite';
import {OverviewLobby} from 'src/models/gameoverview/lobby';
import {OverviewSession} from 'src/models/gameoverview/session';
import {OverviewFinished} from '../models/gameoverview/finished';
import {PersonalData} from '../models/data/personal-data';
import {GlobalData} from '../models/data/global-data';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private readonly GAME_WEBSOCKET_KEY = 'GAME';
  private httpOptions = {
    headers: new HttpHeaders()
  };

  constructor(private http: HttpClient, private websocket: WebsocketService) {
  }

  // region REST

  createUser(): Observable<void> {
    this.setAuthHeader();
    return this.http.post<void>(environment.addressGame + environment.userCreateUrl, null, this.httpOptions);
  }

  startNewSession(): Observable<number> {
    this.setAuthHeader();
    return this.http.post<number>(environment.addressGame + environment.sessionCreateUrl, null, this.httpOptions);
  }

  discardSession(id): Observable<void> {
    this.setAuthHeader();
    return this.http.delete<void>(environment.addressGame + environment.sessionDiscardUrl + id, this.httpOptions);
  }

  leaveSession(id): Observable<void> {
    this.setAuthHeader();
    return this.http.delete<void>(environment.addressGame + environment.sessionLeaveUrl + id, this.httpOptions);
  }

  kickPlayer(sessionId, username): Observable<void> {
    this.setAuthHeader();
    return this.http.delete<void>(environment.addressGame + environment.sessionKickUrl + sessionId + '/' + username, this.httpOptions);
  }

  getSessionLobby(id): Observable<Lobby> {
    this.setAuthHeader();
    return this.http.get<Lobby>(environment.addressGame + environment.sessionGetLobbyUrl + id, this.httpOptions);
  }

  sendSessionInvite(username, sessionId): Observable<void> {
    this.setAuthHeader();
    return this.http.post<void>(environment.addressGame + environment.sessionSendInviteUrl, {
      sessionId,
      username
    }, this.httpOptions);
  }

  getSessionState(id): Observable<GameState> {
    this.setAuthHeader();
    return this.http.get<GameState>(environment.addressGame + environment.gameGetStateUrl + id, this.httpOptions);
  }

  joinSession(sessionId): Observable<void> {
    this.setAuthHeader();
    return this.http.post<void>(environment.addressGame + environment.sessionAcceptInviteUrl + sessionId, null, this.httpOptions);
  }

  addAIPlayer(type, sessionId): Observable<void> {
    this.setAuthHeader();
    return this.http.post<void>(environment.addressGame + environment.sessionAddAIPlayer,
      {sessionId: sessionId, playerType: type}, this.httpOptions);
  }

  getGameOverviewInvites(): Observable<OverviewInvite[]> {
    this.setAuthHeader();
    return this.http.get<OverviewInvite[]>(environment.addressGame + environment.sessionGetOverviewInvitesUrl, this.httpOptions);
  }

  getGameOverviewLobbies(): Observable<OverviewLobby[]> {
    this.setAuthHeader();
    return this.http.get<OverviewLobby[]>(environment.addressGame + environment.sessionGetOverviewLobbiesUrl, this.httpOptions);
  }

  getGameOverviewPlaying(): Observable<OverviewSession[]> {
    this.setAuthHeader();
    return this.http.get<OverviewSession[]>(environment.addressGame + environment.sessionGetOverviewPlayingUrl, this.httpOptions);
  }
  getGameOverviewFinished(): Observable<OverviewFinished[]> {
    this.setAuthHeader();
    return this.http.get<OverviewFinished[]>(environment.addressGame + environment.sessionGetOverviewFinishedUrl,this.httpOptions);
  }

  getOverviewOfGame(sessionId): Observable<GameLog> {
    this.setAuthHeader();
    return this.http.get<GameLog>(environment.addressGame + environment.sessionGetOverviewGameUrl + sessionId,this.httpOptions);
  }

  getFriendRequests(): Observable<Friendship[]> {
    this.setAuthHeader();
    return this.http.get<Friendship[]>(environment.addressGame + environment.getFriendRequestsUrl, this.httpOptions);
  }

  confirmFriendship(friendshipId): Observable<void> {
    this.setAuthHeader();
    return this.http.put<void>(environment.addressGame + environment.friendshipsUrl +
      friendshipId + environment.confirmFriendshipUrl, null, this.httpOptions);
  }

  deleteFriendship(friendshipId): Observable<void> {
    this.setAuthHeader();
    return this.http.delete<void>(environment.addressGame + environment.friendshipsUrl +
      friendshipId + environment.deleteFriendshipUrl, this.httpOptions);
  }

  sendFriendRequest(friendName): Observable<Friendship> {
    this.setAuthHeader();
    return this.http.post<Friendship>(environment.addressGame + environment.sendFriendRequestUrl, friendName, this.httpOptions);
  }

  getPersonalData(username): Observable<PersonalData> {
    this.setAuthHeader();
    return this.http.get<PersonalData>(environment.addressGame + environment.getPersonalDataUrl + username, this.httpOptions);
  }

  getGlobalData(): Observable<GlobalData> {
    this.setAuthHeader();
    return this.http.get<GlobalData>(environment.addressGame + environment.getGlobalDataUrl , this.httpOptions);
  }

  // endregion

  // region WEBSOCKET

  connectToStateChange() {
    this.websocket.connect(environment.addressGameWebsocket, this.GAME_WEBSOCKET_KEY);
  }

  disconnectFromStateChange() {
    this.websocket.disconnect(this.GAME_WEBSOCKET_KEY);
  }

  onWebsocketError(): Observable<never> {
    return this.websocket.onError(this.GAME_WEBSOCKET_KEY);
  }

  onLobbyStateChange(): Observable<IMessage> {
    return this.websocket.onMessage('/user/topic/lobby', this.GAME_WEBSOCKET_KEY);
  }

  onGameStateChange(): Observable<IMessage> {
    return this.websocket.onMessage('/user/topic/state', this.GAME_WEBSOCKET_KEY);
  }

  onErrorMessage(): Observable<IMessage> {
    return this.websocket.onMessage('/user/topic/error', this.GAME_WEBSOCKET_KEY);
  }

  startGame(sessionId, map) {
    this.websocket.send('/game/initialize',
      {sessionId: sessionId, map: map},
      this.GAME_WEBSOCKET_KEY);
  }

  allocate(sessionId, territoryId, troops) {
    this.websocket.send('/game/allocate',
      {sessionId: sessionId, territoryId: territoryId, troops: troops},
      this.GAME_WEBSOCKET_KEY);
  }

  attack(sessionId, attackingTerritoryId, defendingTerritoryId) {
    this.websocket.send('/game/attack',
      {sessionId: sessionId, attackingTerritoryId: attackingTerritoryId, defendingTerritoryId: defendingTerritoryId},
      this.GAME_WEBSOCKET_KEY);
  }

  fortify(sessionId, fromTerritoryId, toTerritoryId, troops) {
    this.websocket.send('/game/fortify',
      {sessionId: sessionId, fromTerritoryId: fromTerritoryId, toTerritoryId: toTerritoryId, troops: troops},
      this.GAME_WEBSOCKET_KEY);
  }

  phase(sessionId) {
    this.websocket.send('/game/phase/' + sessionId, {}, this.GAME_WEBSOCKET_KEY);
  }

  setColor(sessionId, username, color) {
    this.websocket.send('/lobby/color',
      {sessionId: sessionId, username: username, color: color},
      this.GAME_WEBSOCKET_KEY);
  }

  // endregion

  private setAuthHeader() {
    this.httpOptions = {
      headers: new HttpHeaders({'Authorization': 'Bearer ' + sessionStorage.getItem('RiskAccessToken')})
    };
  }


}
