import {Injectable} from '@angular/core';
import {StompConfig, StompService} from '@stomp/ng2-stompjs';
import {from, Observable} from 'rxjs';
import {IMessage} from '@stomp/stompjs';
import {Websocket} from '../models/websocket';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private static readonly MAX_RECONNECT = 2;
  private websockets: Map<string, Websocket> = new Map<string, Websocket>();

  connect(url, key) {
    let stompConfig: StompConfig = {
      url: url + sessionStorage.getItem('RiskAccessToken'),
      headers: {
        login: '',
        passcode: ''
      },
      heartbeat_in: 0,
      heartbeat_out: 20000,
      reconnect_delay: 2000,
      debug: true
    };

    this.websockets.set(key, new Websocket());
    this.websockets.get(key).stompService = new StompService(stompConfig);
    this.websockets.get(key).stompService.stompClient.beforeConnect = () => {
      this.websockets.get(key).connCounter++;

      if (this.websockets.get(key).connCounter === WebsocketService.MAX_RECONNECT) {
        this.disconnect(key);
        this.websockets.get(key).error.error(new Error('Whoops, connection lost!'));
      }
    };
  }

  onMessage(topic, key): Observable<IMessage> {
    return this.websockets.get(key).stompService.watch(topic);
  }

  disconnect(key) {
    if (this.websockets.get(key).stompService) {
      this.websockets.get(key).stompService.disconnect();
      this.websockets.get(key).connCounter = 0;
      this.websockets.delete(key);
    }
  }

  send(topic: string, payload: any, key): void {
    this.websockets.get(key).stompService.publish(topic, JSON.stringify(payload));
  }

  onError(key): Observable<never> {
    return from(this.websockets.get(key).error);
  }
}
