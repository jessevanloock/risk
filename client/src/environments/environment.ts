// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  addressAuth: 'http://localhost:9000/',
  addressGame: 'http://localhost:9001/',
  addressChat: 'http://localhost:9002/',
  addressGameWebsocket: 'ws://localhost:9001/game-websocket?access_token=',
  addressChatWebsocket: 'ws://localhost:9002/chat-websocket?access_token=',
  // auth urls
  createUserUrl: 'user/create-new-user',
  logoutUrl: 'logout',
  getTokenUrl: 'oauth/token?',
  checkTokenUrl: 'oauth/check_token?token=',
  revokeTokenUrl: 'user/logout',
  googleUrl: 'user/google?idtoken=',
  // game urls
  gameGetStateUrl: 'game/',
  sessionCreateUrl: 'session',
  sessionDiscardUrl: 'session/',
  sessionLeaveUrl: 'session/leave/',
  sessionKickUrl: 'session/kick/',
  sessionGetLobbyUrl: 'session/lobby/',
  sessionGetOverviewInvitesUrl: 'session/overview/invites',
  sessionGetOverviewLobbiesUrl: 'session/overview/lobbies',
  sessionGetOverviewPlayingUrl: 'session/overview/playing',
  sessionGetOverviewFinishedUrl: 'session/overview/finished',
  sessionGetOverviewGameUrl: 'session/overview/',
  sessionAcceptInviteUrl: 'session/accept/',
  sessionAddAIPlayer: 'session/join-session-cpu',
  sessionSendInviteUrl: 'session/invite',
  userCreateUrl: 'user/create-user',
  friendshipsUrl: 'user/friends/',
  getFriendRequestsUrl: 'user/friends/requests',
  confirmFriendshipUrl: '/confirm',
  deleteFriendshipUrl: '/delete',
  sendFriendRequestUrl: 'user/friends/send-request',
  getPersonalDataUrl: 'data/personal/',
  getGlobalDataUrl: 'data/global/',
  // chat urls
  getChatUrl: 'chat/',
  getSessionChatUrl: 'chat/session-chat/',
  getGlobalChatUrl: 'chat/global-chat',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
