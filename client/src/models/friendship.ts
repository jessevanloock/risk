export class Friendship {
  private readonly _friendshipId: number;
  private readonly _friendName: string;
  private _confirmed: boolean;
  private readonly _requestType: string;

  constructor(friendshipId: number, friendName: string, confirmed: boolean, requestType: string) {
    this._friendshipId = friendshipId;
    this._friendName = friendName;
    this._confirmed = confirmed;
    this._requestType = requestType;
  }

  get friendshipId(): number {
    return this._friendshipId;
  }

  get friendName(): string {
    return this._friendName;
  }

  get confirmed(): boolean {
    return this._confirmed;
  }

  set confirmed(value: boolean) {
    this._confirmed = value;
  }

  get requestType(): string {
    return this._requestType;
  }
}
