import {GameState} from './game-state';

export class GameLog {
  private readonly _startstate: GameState;
  private readonly _log: any[];

  constructor(startstate: GameState, log: any[]) {
    this._startstate = startstate;
    this._log = log;
  }

  get startstate(): GameState {
    return this._startstate;
  }

  get log(): any[] {
    return this._log;
  }
}
