export class Highscore {
  private _username: string;
  private _conquestsWon: number;
  private _conquestsPlayed: number;


  get username(): string {
    return this._username;
  }

  get conquestsWon(): number {
    return this._conquestsWon;
  }

  get conquestsPlayed(): number {
    return this._conquestsPlayed;
  }
}
