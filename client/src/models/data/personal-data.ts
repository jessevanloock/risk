import {HistoryItem} from "./historyItem";

export class PersonalData {
  private _username: string;
  // Conquests
  private _conquestsTotal: number;
  private _conquestsWon: number;
  private _conquestsLost: number;
  private _conquestsRatio: number;
  // Attacks
  private _attacksTotal: number;
  private _attacksSuccessful: number;
  private _attacksFailed: number;
  private _attacksRatio: number;
  // Defends
  private _defendsTotal: number;
  private _defendsSuccessful: number;
  private _defendsFailed: number;
  private _defendsRatio: number;
  // Troops
  private _troopsDeployed: number;
  private _troopsDefeated: number;
  private _troopsLost: number;
  private _troopsRatio: number;
  // History
  private _history: HistoryItem[];


  get username(): string {
    return this._username;
  }

  get conquestsTotal(): number {
    return this._conquestsTotal;
  }

  get conquestsWon(): number {
    return this._conquestsWon;
  }

  get conquestsLost(): number {
    return this._conquestsLost;
  }

  get conquestsRatio(): number {
    return this._conquestsRatio;
  }

  get attacksTotal(): number {
    return this._attacksTotal;
  }

  get attacksSuccessful(): number {
    return this._attacksSuccessful;
  }

  get attacksFailed(): number {
    return this._attacksFailed;
  }

  get attacksRatio(): number {
    return this._attacksRatio;
  }

  get defendsTotal(): number {
    return this._defendsTotal;
  }

  get defendsSuccessful(): number {
    return this._defendsSuccessful;
  }

  get defendsFailed(): number {
    return this._defendsFailed;
  }

  get defendsRatio(): number {
    return this._defendsRatio;
  }

  get troopsDeployed(): number {
    return this._troopsDeployed;
  }

  get troopsDefeated(): number {
    return this._troopsDefeated;
  }

  get troopsLost(): number {
    return this._troopsLost;
  }

  get troopsRatio(): number {
    return this._troopsRatio;
  }

  get history(): HistoryItem[] {
    return this._history;
  }

  set conquestsRatio(conquestsRatio: number) {
    this._conquestsRatio = conquestsRatio;
  }

  set attacksRatio(attacksRatio: number) {
    this._attacksRatio = attacksRatio;
  }

  set defendsRatio(defendsRatio: number) {
    this._defendsRatio = defendsRatio;
  }

  set troopsRatio(troopsRatio: number) {
    this._troopsRatio = troopsRatio;
  }
}
