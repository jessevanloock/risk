import {Highscore} from './highscore';

export class GlobalData {
  private _conquestsFinished: number;
  private _conquestsPlaying: number;
  private _conquestsLobby: number;
  private _usersInConquestPlaying: number;
  private _highscores: Highscore[];


  get conquestsFinished(): number {
    return this._conquestsFinished;
  }

  get conquestsPlaying(): number {
    return this._conquestsPlaying;
  }

  get conquestsLobby(): number {
    return this._conquestsLobby;
  }

  get usersInConquestPlaying(): number {
    return this._usersInConquestPlaying;
  }


  get highscores(): Highscore[] {
    return this._highscores;
  }
}
