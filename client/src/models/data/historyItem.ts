export class HistoryItem {
  private _sessionId: number;
  private _map: string;
  private _won: boolean;


  get sessionId(): number {
    return this._sessionId;
  }

  get map(): string {
    return this._map;
  }

  get won(): boolean {
    return this._won;
  }
}
