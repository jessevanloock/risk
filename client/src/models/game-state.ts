import {Continent} from './continent';
import {Player} from './player';
import {Turn} from './turn';

export class GameState {
  private _players: Player[];
  private _continents: Continent[];
  private _extras: string[];
  private _turn: Turn;
  private _map: string;
  private _finished: string;

  constructor(players: Player[], continents: Continent[], extras: string[], turn: Turn, map: string, finished: string) {
    this._players = players;
    this._continents = continents;
    this._extras = extras;
    this._turn = turn;
    this._map = map;
    this._finished = finished;
  }

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }

  get continents(): Continent[] {
    return this._continents;
  }

  set continents(value: Continent[]) {
    this._continents = value;
  }

  get extras(): string[] {
    return this._extras;
  }

  set extras(value: string[]) {
    this._extras = value;
  }

  get turn(): Turn {
    return this._turn;
  }

  set turn(value: Turn) {
    this._turn = value;
  }


  get map(): string {
    return this._map;
  }

  set map(value: string) {
    this._map = value;
  }

  get finished(): string {
    return this._finished;
  }

  set finished(value: string) {
    this._finished = value;
  }
}
