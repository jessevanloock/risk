export class Player {
  private readonly _username: string;
  private _color: string;
  private readonly _playerType: string;

  constructor(username: string, color: string, playerType: string) {
    this._username = username;
    this._color = color;
    this._playerType = playerType;
  }

  get username(): string {
    return this._username;
  }

  get color(): string {
    return this._color;
  }

  set color(value: string) {
    this._color = value;
  }

  get playerType(): string {
    return this._playerType;
  }
}
