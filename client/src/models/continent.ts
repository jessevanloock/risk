import {Territory} from './territory';

export class Continent {
  private readonly _id: number;
  private readonly _bonus: number;
  private readonly _name: string;
  private readonly _territories: Territory[];

  constructor(id: number, bonus: number, name: string, territories: Territory[]) {
    this._id = id;
    this._bonus = bonus;
    this._name = name;
    this._territories = territories;
  }

  get id(): number {
    return this._id;
  }

  get bonus(): number {
    return this._bonus;
  }

  get name(): string {
    return this._name;
  }

  get territories(): Territory[] {
    return this._territories;
  }
}
