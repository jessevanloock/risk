export class Territory {
  private readonly _id: number;
  private readonly _name: string;
  private readonly _path: string;
  private readonly _neighbors: number[];
  private _troops: number;
  private _player: string;
  private _pathElement: SVGPathElement;

  constructor(id: number, name: string, path: string, neighbors: number[], troops: number, player: string, pathElement: SVGPathElement) {
    this._id = id;
    this._name = name;
    this._path = path;
    this._neighbors = neighbors;
    this._troops = troops;
    this._player = player;
    this._pathElement = pathElement;
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get path(): string {
    return this._path;
  }

  get neighbors(): number[] {
    return this._neighbors;
  }

  get troops(): number {
    return this._troops;
  }

  set troops(value: number) {
    this._troops = value;
  }

  get player(): string {
    return this._player;
  }

  set player(value: string) {
    this._player = value;
  }

  get pathElement(): SVGPathElement {
    return this._pathElement;
  }

  set pathElement(value: SVGPathElement) {
    this._pathElement = value;
  }
}

