export class MapElement {
  private _key: string;
  private _element: SVGPathElement;

  constructor(key, element) {
    this._key = key;
    this._element = element;
  }

  get key(): string {
    return this._key;
  }

  set key(value: string) {
    this._key = value;
  }

  get element(): SVGPathElement {
    return this._element;
  }

  set element(value: SVGPathElement) {
    this._element = value;
  }
}
