export class ChatMessage {
  private _username: string;
  private _message: string;

  constructor(username: string, message: string) {
    this._username = username;
    this._message = message;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get message(): string {
    return this._message;
  }

  set message(value: string) {
    this._message = value;
  }
}
