import {Player} from './player';

export class Lobby {
  private readonly _sessionId: number;
  private _host: string;
  private _players: Player[];
  private _rows: any[];

  constructor(id: number, host: string, players: Player[]) {
    this._sessionId = id;
    this._host = host;
    this._players = players;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  get host(): string {
    return this._host;
  }

  set host(value: string) {
    this._host = value;
  }

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }

  get rows(): any[] {
    return this._rows;
  }

  set rows(value: any[]) {
    this._rows = value;
  }
}
