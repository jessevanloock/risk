export class Turn {
  private _username: string;
  private _phase: string;
  private _placeableTroops: number;

  constructor(username: string, phase: string, placeableTroops: number) {
    this._username = username;
    this._phase = phase;
    this._placeableTroops = placeableTroops;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get phase(): string {
    return this._phase;
  }

  set phase(value: string) {
    this._phase = value;
  }

  get placeableTroops(): number {
    return this._placeableTroops;
  }

  set placeableTroops(value: number) {
    this._placeableTroops = value;
  }
}
