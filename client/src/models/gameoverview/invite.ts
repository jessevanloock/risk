export class OverviewInvite {
  private readonly _sessionId: number;
  private readonly _username: string;

  constructor(sessionId: number, username: string) {
    this._sessionId = sessionId;
    this._username = username;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  get username(): string {
    return this._username;
  }
}
