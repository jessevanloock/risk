export class OverviewFinished {
  private readonly _sessionId: number;
  private readonly _map: string;
  private readonly _winner: string;

  constructor(sessionId: number, map: string, winner: string) {
    this._sessionId = sessionId;
    this._map = map;
    this._winner = winner;
  }

  get sessionId(): number {
    return this._sessionId;
  }


  get map(): string {
    return this._map;
  }

  get winner(): string {
    return this._winner;
  }
}
