export class OverviewSession {
  private readonly _sessionId: number;
  private readonly _host: string;
  private readonly _map: string;
  private readonly _lastPlayed: string;
  private readonly _players: number;
  private readonly _yourTurn: boolean;
  private readonly _currentPhase: string;

  constructor(sessionId: number, host: string, map: string, lastPlayed: string, players: number, yourTurn: boolean, currentPhase: string) {
    this._sessionId = sessionId;
    this._host = host;
    this._map = map;
    this._lastPlayed = lastPlayed;
    this._players = players;
    this._yourTurn = yourTurn;
    this._currentPhase = currentPhase;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  get host(): string {
    return this._host;
  }

  get map(): string {
    return this._map;
  }

  get lastPlayed(): string {
    return this._lastPlayed;
  }

  get players(): number {
    return this._players;
  }

  get yourTurn(): boolean {
    return this._yourTurn;
  }

  get currentPhase(): string {
    return this._currentPhase;
  }
}
