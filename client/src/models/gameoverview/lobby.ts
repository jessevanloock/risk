export class OverviewLobby {
  private readonly _sessionId: number;
  private readonly _host: string;
  private readonly _players: number;
  private readonly _created: string;

  constructor(sessionId: number, host: string, players: number, created: string) {
    this._sessionId = sessionId;
    this._host = host;
    this._players = players;
    this._created = created;
  }

  get sessionId(): number {
    return this._sessionId;
  }

  get host(): string {
    return this._host;
  }

  get players(): number {
    return this._players;
  }

  get created(): string {
    return this._created;
  }
}
