import {StompService} from '@stomp/ng2-stompjs';
import {Subject} from 'rxjs';

export class Websocket {
  private _stompService: StompService;
  private _connCounter = 0;
  private _error = new Subject<never>();

  get stompService(): StompService {
    return this._stompService;
  }

  set stompService(value: StompService) {
    this._stompService = value;
  }

  get connCounter(): number {
    return this._connCounter;
  }

  set connCounter(value: number) {
    this._connCounter = value;
  }

  get error(): Subject<never> {
    return this._error;
  }
}
